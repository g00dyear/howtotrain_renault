from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'howtotrain.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include('users.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^tests/', include('etest.urls')),
    url(r'^checklist/', include('checklist.urls')),
    url(r'^trainings/', include('trainings.urls')),
    url(r'^elearning/', include('elearning.urls')),
    url(r'^files/', include('files.urls')),
    url('^', include('django.contrib.auth.urls')),    

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

