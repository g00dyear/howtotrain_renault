from etest.models import *
from elearning.models import *
from users.models import *
from datetime import datetime
from trainings.models import *


def get_current_tests():
    try:
        tests = PlannedTest.objects.filter(date_start__lt=datetime.now(), date_end__gt=datetime.now()).order_by('date_start').all()
        return tests
    except:
        return None


def get_current_courses():
    try:
        courses = PlannedCourse.objects.filter(date_start__lt=datetime.now(), date_end__gt=datetime.now()).order_by('date_start').all()
        return courses
    except:
        return None
        
        
def users_sync(path):
    from sync.tools import correct_symbols, correct_mail
    csv_file = open(path)
    row = next(csv_file)
    errors = 0
    new = 0
    old = 0
    inactive = 0
    no_changes = 0
    while errors < 100:
        try:
            row = next(csv_file)
            row = row.split(';')
            # Проверка наличия даты окончания контракта
            first_name = row[2] # Имя пользователя
            last_name = row[1] # Фамилия пользователя
            parent = User.objects.filter(short_title=row[22]).first()  # Dealer
            # Проверка наличия почты и автоматическое ее создание при отсутствии
            if row[10] != '':
                email = row[10].lower()
            else:
                email = row[0].lower() + '@example.com'
            #Должность
            if UserJob.objects.filter(name = row[11]).last():
                job = UserJob.objects.filter(name = row[11]).last()
            else:
                job = UserJob(name=row[11])
                job.save()
            userid = row[0]
            # Создание пользователя или обновление информации о нем
            if not User.objects.filter(userid=userid).first():
                u = User(username='employee', parentId=parent, first_name=first_name, last_name=last_name, email=email, job=job, userid=userid)
                u.save()
                u.groups.add(3)
                u.set_password('renault1898')
                u.save()
                new += 1
            elif User.objects.filter(userid=userid, email=email, job=job, parentId=parent).last():
                no_changes += 1
            else:
                u = User.objects.filter(userid=userid).last()
                u.job = job
                u.email = email
                u.groups.add(3)
                u.parentId = parent
                u.save()
                old += 1
            # Проверка наличия даты окончания контракта
            if row[24] != '':
                u.is_active = False
                u.save()
                inactive += 1
        except:
            errors+=1
            continue
    return (new, old, inactive, no_changes)


def get_events(user_id):
    from datetime import datetime
    import time
    user = User.objects.get(id=user_id)
    if user.is_user():
        tests = PlannedTest.objects.filter(allowed_users=user, date_end__gt=datetime.now()).all()
        courses = PlannedCourse.objects.filter(allowed_users=user, date_end__gt=datetime.now()).all()
        trainings = set(PlannedTraining.objects.filter(allowed_users=user, dates__date__gt=datetime.now()).all())
    else:
        tests = PlannedTest.objects.filter(date_end__gt=datetime.now()).all()
        courses = PlannedCourse.objects.filter(date_end__gt=datetime.now()).all()
        trainings = set(PlannedTraining.objects.filter(dates__date__gt=datetime.now()).all())
    events = []
    for test in tests:
        if user.is_user():
            link = '/tests/info/' + str(test.id) + '/'
        else:
            link = '/tests/full-testinfo/' + str(test.id) + '/'
        events.append([test, 'Тестирование', test.date_start, test.date_end, link])
    for course in courses:
        if user.is_user():
            link = '/elearning/course/' + str(course.id) + '/'
        else:
            link = '/elearning/userlist/' + str(course.id) + '/'
        events.append([course, 'Дистанционное обучение', course.date_start, course.date_end, link])
    for tr in trainings:
        link = '/trainings/traininginfo/' + str(tr.id) + '/'
        events.append([tr, 'Очное обучение', tr.dates.order_by('date').first().date, tr.dates.order_by('date').last().date, link])
    events = sorted(events, key=lambda x: x[2])
    return events
