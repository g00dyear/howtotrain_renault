from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
# Create your models here.

class Region(models.Model):
    name = models.CharField(verbose_name='Регион', max_length=30)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = "Регионы"
    
    
class City(models.Model):
    name = models.CharField(verbose_name='Город', max_length=30)
    region = models.ForeignKey(Region, verbose_name='Регион')
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = "Города"


class Departament(models.Model):
    name = models.CharField(max_length=300)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Отделы'
        verbose_name = 'Отдел'


class UserJob(models.Model):
    name = models.CharField(verbose_name='Должность', max_length=255)
    departament = models.ForeignKey(Departament, null=True, blank=True, related_name='jobs')
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Должность'
        verbose_name_plural = "Должности"


AbstractUser._meta.get_field('email')._unique = True
AbstractUser._meta.get_field('email').blank = False
AbstractUser._meta.get_field('email').db_index = True
AbstractUser._meta.get_field('username')._unique = False
AbstractUser._meta.get_field('username').default = 'noname'
        
        
        
class User(AbstractUser):
    class Meta:
        permissions = (
            ('UPerm', "can join user's urls"),
            ('DLPerm', "can join dealer's urls"),
            ('DSPerm', "can join distributor's urls"),
        )
        verbose_name = 'Пользователь'
        verbose_name_plural = "Пользователи"
        ordering = ('last_name',)
        
    fathers_name = models.CharField(blank=True, max_length=30, verbose_name='Отчество')
    city = models.OneToOneField(City, blank=True, null=True, verbose_name='Город')
    phone = models.CharField(verbose_name='Телефон', max_length=30, blank=True)
    job = models.ForeignKey(UserJob, blank=True, null=True, verbose_name='Должность', related_name='users')
    parentId = models.ForeignKey('self', blank=True, null=True, related_name='childs', verbose_name='Родитель')
    title = models.CharField(verbose_name='Название(дилер, дистрибьютор)', max_length=255, blank=True, null=True)
    avatar = models.ImageField(verbose_name='Аватар', upload_to='media/avatars/', default='media/avatars/renault_logo.jpg')
    short_title = models.CharField(verbose_name='Короткое название', max_length=20, blank=True, null=True)
    userid = models.CharField(verbose_name='id пользователя', max_length=50, blank=True)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']
        
    def get_full_name(self):
        if self.title:
            return self.title
        else:
            return '%s %s %s' % (self.last_name, self.first_name, self.fathers_name,)
           
    def is_dealer(self):
        if Group.objects.get(id = 1) in self.groups.all():
            return True
        else:
            return False
    
    def is_dist(self):
        if Group.objects.get(id = 2) in self.groups.all():
            return True
        else:
            return False
            
    def is_user(self):
        if Group.objects.get(id = 3) in self.groups.all() or Group.objects.get(id = 5) in self.groups.all():
            return True
        else:
            return False
        
    def is_coach(self):
        if Group.objects.get(id = 7) in self.groups.all():
            return True
        else:
            return False
    
    def is_tester(self):
        if Group.objects.get(id = 5) in self.groups.all():
            return True
        else:
            return False
    
    def is_regmanager(self):
        if Group.objects.get(id = 6) in self.groups.all():
            return True
        else:
            return False
            
    def get_study_level(self):
        from tools import get_study_level as gsl
        return gsl(self)

    def __str__(self):
        if self.is_dealer():
            return self.short_title
        try:
            return self.get_full_name() + " | " + self.job.name
        except:
            return self.get_full_name()

    def get_rating(self):
        from newcourse.models import CourseResult
        results = CourseResult.objects.filter(for_raiting=True, user=self).all()
        total = 0
        for r in results:
            total += r.mark
        try:
            raiting = round(total/results.count(), 2)
            return raiting
        except:
            return None
    
    
class Faq(models.Model):
    question = models.TextField(verbose_name='Вопрос')
    answer = models.TextField(verbose_name='Ответ')
    
    
class CustomGroup(models.Model):
    name = models.CharField(verbose_name='Наименование', max_length=64)
    dealer = models.ManyToManyField(User, limit_choices_to=Q(groups=1) | Q(groups=6), verbose_name='Дилер')
    label = models.CharField(max_length=128, verbose_name='Метка', blank=True, null=True)
    
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Дополнительная группа'
        verbose_name_plural = 'Дополнительные группы'
        
        
class Sync(models.Model):
    date = models.DateTimeField(auto_now=True)
    file = models.FileField(upload_to='sync')
    
    class Meta:
        verbose_name = 'Файл синхронизации'
        verbose_name_plural = 'Файлы синхронизации'
