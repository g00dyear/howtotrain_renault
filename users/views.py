from django.shortcuts import render, render_to_response, redirect, HttpResponse
from django.contrib import auth
from django.contrib.auth.models import Group
from trainings.models import PlannedTraining, UserlistByDate, Training, TrainingResult, StudyLevel
from .models import User, UserJob, Faq, CustomGroup, Departament
from checklist.models import *
from etest.models import *
from elearning.models import PlannedCourse
from django.contrib.auth.decorators import login_required, permission_required
from etest.tools import time, get_study_level
from django.contrib.auth import update_session_auth_hash
from django.core.mail import send_mail, EmailMultiAlternatives
from checklist.tools import *
from datetime import datetime
from django.db.models import Q
# Create your views here.


def login(request):
    if request.method == 'POST':
        e = request.POST['email'].lower()
        if not User.objects.filter(email=e).first():
            message = 'Не правильный email!'
            return render(request, 'users/login.html', {'message': message})
        u = User.objects.filter(email=e).first()
        email = u.email
        password = request.POST['password']
        user = auth.authenticate(email=email, password=password)   
        if user:
            auth.login(request, user)
            return redirect('/')
        else:
            message = 'Не правильно введен пароль'
            return render(request, 'users/login.html', {'message': message})
    else:
        return render(request, 'users/login.html')


def recover(request):
    if request.method == 'POST':
        email = request.POST['email'].lower()
        try:
            from mailing.mailgun import send_simple_message3
            user = User.objects.get(email=email)
            password = User.objects.make_random_password()
            user.set_password(password)
            user.save()
            subject = 'Восстановление пароля.'
            text = 'Добрый день! Вы запросили восстановление пароля. Ваш новый пароль:' + password
            text_html = '<p>Добрый день!</p><p>Вы запросили восстановление пароля.</p><p>Ваш новый пароль: ' + password + '</p>'
            send_simple_message3(text_html, subject, email)
            message = 'Ваш новый пароль выслан на почту'
            return render(request, 'users/login.html', {'message': message})
        except:
            message = 'Пользователя с таким e-mail не существует'
            return render(request, 'users/login.html', {'message': message})
        
        
        
@login_required
def index(request):
    from newcourse.reiting import raiting_count
    user = request.user
    if user.is_user():
        study_mark = raiting_count(user.id)
        return render(request, 'users/user_mainpage.html', {'current_user': user, 'study_mark': study_mark})
        
    """ Старая версия главной страницы дистра  
    if user.is_dist():
        trainings = Training.objects.all()
        done = [] #пройденные пользователями тренинги
        undone = [] #не пройденные
        smart_done = [] # с пользой ( обязательный для них )
        target_users = [] # целевая аудитория ( количество пользователей для которых он обязателен )
        for training in trainings:
            # Количество пользователей которые должны пройти тренинг
            maxusers = 0
            for a in training.auditory.all():
                maxusers += a.users.count()
            target_users.append(maxusers)
            #Количество людей прошедших этот тренинг
            try:
                results = TrainingResult.objects.filter(training=training).all()
                count_u = set()
                count_s_u = set()
                for result in results:
                    count_u.add(result.user)
                    if result.user.job in training.auditory.all():
                        count_s_u.add(result.user)
                done.append(len(count_u))
                smart_done.append(len(count_s_u))
            except:
                done.append(0)
                smart_done.append(0)
            #Количество людей которым нужно пройти этот тренинг
            undone.append(maxusers-len(count_s_u))
        results = zip(trainings, done, undone, smart_done, target_users)
        
        # информация о пользователях, профессиях
        dealers = user.childs.all()
        dealer_childs = Group.objects.get(pk=3).user_set.all()
        jobs = UserJob.objects.all()
        count_users_job = []
        for job in jobs:
            if User.objects.filter(job=job).first():
                count_users_job.append(User.objects.filter(job=job).count())
            else:
                count_users_job.append("0")
        users_jobs = zip(count_users_job, jobs)
        return render(request, 'users/dist_mainpage.html', {'user':user, 'results': results, 'users_jobs': users_jobs, 'dealers': dealers,
                                                            'dealer_childs': dealer_childs})   
    """
    # new version of dist main-page
    if user.is_dist() or user.is_superuser:
        from users.tools import get_current_courses, get_current_tests
        courses = get_current_courses()
        tests = get_current_tests()
        return render(request, 'users/dist_mainpage.html', {'user':user, 'courses': courses, 'tests': tests})
        
        
   # Тренинги для дилера (инфо)     
    if user.is_dealer():
        from newcourse.reiting import raiting_dealer
        raiting, deps_raitings = raiting_dealer(user.id)
        return render(request, 'users/dealer_mainpage.html', {'user': user, 'raiting': raiting, 'deps_raitings': deps_raitings})
    if user.is_coach():
        trainings = PlannedTraining.objects.filter(finished=False, coach=user)
        return render(request, 'coach/coach.html', {'user': user, 'trainings': trainings})
        
        
    if user.is_regmanager():
        if request.method == 'POST':
            post = request.POST.copy()
            subcategory_id = int(post['subcategory.id'])
            subcategory = SubCategory.objects.get(id=subcategory_id)
            dealer_id = int(post['dealer.id'])
            dealer = User.objects.get(id=dealer_id)
            
            performer_id = int(post['user.id'])
            performer = User.objects.get(id=performer_id)
            points = subcategory.points.all()
            
            
            report = Report(subcategory=subcategory, dealer=dealer, performer=performer)
            report.save()
            
            for point in points:
                if request.POST[str(point.id)] == 'true':
                    mark = Mark(point=point, report=report, checked=True, mark=point.weight)
                    mark.save()
                else:
                    if request.POST[point.name+str(point.id)] != '0':
                        user_id = request.POST[point.name+str(point.id)]
                        responsible = User.objects.get(id=user_id)
                        mark = Mark(point=point, report=report, checked=False, mark=0, comment=request.POST[str(point.name)], responsible=responsible)
                        mark.save()
                    else:
                        mark = Mark(point=point, report=report, checked=False, mark=0, comment=request.POST[str(point.name)])
                        mark.save()
            date = datetime.now()
            c_mark = get_category_mark2(subcategory.category.id, dealer.id)
            current_result2 = CategoryResult(dealer=dealer, mark=c_mark, category=subcategory.category, date=date)
            current_result2.save()
            
            r_mark = get_report_mark(dealer.id)
            current_result = ReportResult(dealer=dealer, mark=r_mark, date=date)
            current_result.save()
            
            
            
            return redirect('/')
                    
        region = CustomGroup.objects.filter(dealer=user, label='Регион').first()
        group = CustomGroup.objects.filter(dealer=user, label='Регион').first().name
        dealers = CustomGroup.objects.filter(name=group).first().dealer.filter(groups__id=1).all()
        categories = Category.objects.all()
        
        report_results = []
        dealer_cat_results = []
        
        for dealer in dealers:
            cat_results = []
            for cat in categories:
                cat_results.append(get_category_mark(cat.id, dealer.id))
            dealer_cat_results.append(cat_results)
        
        for dealer in dealers:
            cr = []
            for cat in categories:
                cr.append(get_category_mark2(cat.id, dealer.id))
            report_results.append(get_report_mark(dealer.id))
        marks, monthes = regions_chart(region.id)
        chart = False
        cat_marks = []
        for category in categories:
            cat_marks.append(category_reg_chart(region.id, category.id))
        cat_charts = zip(categories, cat_marks)
        for x in marks:
            if x != '':
                chart = True
        dealers_results = zip(dealers, dealer_cat_results, report_results)
        return render(request, 'users/regmanager_mainpage.html', {'user': user, 'dealers_results': dealers_results, 'categories': categories, 'dealers': dealers, 'marks': marks,
                                                                  'monthes': monthes, 'chart': chart, 'cat_charts': cat_charts})
    else:
        return redirect('/')


@login_required
def training_info(request, training_id):
    training = Training.objects.get(id=training_id)
    user = request.user
    target_users = User.objects.filter(job=training.auditory.all()).all()
    count_u = set()
    smart_users = set()
    smart = []
    pass_date = []
    mark = []
    childs = []
    try:
        results = TrainingResult.objects.filter(training=training).all()
        for result in results:
            count_u.add(result.user)
            if result.user.job in training.auditory.all():
                smart_users.add(result.user)
        for user in count_u:
            if user in smart_users:
                smart.append(1)
            else:
                smart.append(0)
            pass_date.append(TrainingResult.objects.filter(training=training, user=user).last().planned_training)
            mark.append(TrainingResult.objects.filter(training=training, user=user).last().result)
            childs.append(user)
    except:
        pass
    training_info = zip(childs, smart, pass_date, mark)
    without_result = set(target_users) - count_u
    return render(request, 'users/training_info.html', {'user': user, 'training_info': training_info, 'training': training, 
                                                        'without_result': without_result}) 
        
    
@login_required
def logout(request):
    auth.logout(request)
    return redirect('/login/')
    

# Вывод списка пользователей для дилера и дистрибьютора
@login_required
def list_of_users(request):
    user = request.user
    if user.is_dealer():
        return render(request, 'users/dealer_userlist.html', {'user':user})
    if user.is_dist() or user.is_superuser:
        childs = Group.objects.get(id=3).user_set.all()
        return render(request, 'users/dist_userlist.html', {'user':user, 'childs':childs})
        

# Вывод информации о конкретном пользователе для дилера или дистрибьютора
@login_required
def userinfo(request, user_id):
    from newcourse.reiting import raiting_count
    user = User.objects.get(id=user_id)
    current_user = request.user
    if user in current_user.childs.all() or user.is_regmanager() or current_user.is_superuser or current_user.is_dist():
        study_mark = raiting_count(user.id)
        print(study_mark)
        return render(request, 'users/user_mainpage.html', {'user': user, 'current_user': current_user,
                                                            'study_mark': study_mark})
    else:
        return HttpResponse('Нет доступа')


# Список дилеров для дистрибьютора
@login_required
def dealers_info(request):
    user = request.user
    if user.is_dist() or user.is_superuser:
        childs = User.objects.filter(groups__id=1).all()
        results = []
        
        for child in childs:
            subchilds = child.childs.all()
            if subchilds:
                result = 0
                for subchild in subchilds:
                    if StudyLevel.objects.filter(user=subchild):
                        mark = StudyLevel.objects.filter(user=subchild).last()
                        result += mark.mark
                    else:
                        result += 0
                results.append(int(result/subchilds.count()))
            else:
                results.append(0)
        dealers_info = zip(childs, results)
        
        return render(request, 'users/dist_dealers_info.html', {'user': user,'dealers_info': dealers_info})
    else:
        return redirect('/logout/')
    
# Информация о конкретном дилере для дистрибьютора
@login_required
def dealerinfo(request, user_id):
    user = User.objects.get(id=user_id)
    dist = request.user
    marks = []
    try:
        childs = User.objects.filter(groups__id=3, parentId=user).all()
    except:
        childs = []
    if user.is_dealer():
        result = 0
        if user.childs.last():
            for child in user.childs.all():
                if StudyLevel.objects.filter(user=child):
                    mark = StudyLevel.objects.filter(user=child).last()
                    marks.append(mark.mark)
                    result += mark.mark
                else:
                    marks.append(0)
                    result += 0
        study_mark = int(result/user.childs.count())
        childs_marks = zip(childs, marks)
        return render(request, 'users/dist_dealerinfo.html', {'user': user, 'study_mark': study_mark, 'childs_marks': childs_marks, 'dist': dist})
    else:
        return redirect('/logout')
        

@login_required
def change_password(request):
    current_user = request.user
    if request.method == 'POST':
        oldpassword = request.POST['oldpassword']
        newpassword = request.POST['newpassword']
        confirmedpassword = request.POST['confirmedpassword']
        if auth.authenticate(email=current_user.email, password=oldpassword) and newpassword == confirmedpassword:
            current_user.set_password(newpassword)
            current_user.save()
            update_session_auth_hash(request, request.user)
            return redirect('/')
        elif newpassword != confirmedpassword:
            message = 'Введенные пароли не совпадают'
            return render(request, 'users/password_change.html', {'message': message})
        elif not auth.authenticate(email=current_user.email, password=oldpassword):
            message = 'Не правильно введен пароль. Повторите ввод'
            return render(request, 'users/password_change.html', {'message': message})
        return redirect('/')
    else:
        return render(request, 'users/password_change.html', {'current_user':current_user})
        
        
@login_required
def coach_info(request, training_id):
    user = request.user
    if user.is_coach():
        ptraining = PlannedTraining.objects.get(id=training_id)
        dates = ptraining.dates.all()
        users = ptraining.allowed_users.all()

        if request.method == 'POST':
            post = request.POST.copy()
            for u in users:
                try:
                    mark = int(post[str(u.id)])
                    tr = TrainingResult(result=mark, user=u, planned_training=ptraining, training=ptraining.training)
                    tr.save()
                except:
                    pass
        return render(request, 'coach/coach_info.html', {'user': user, 'ptraining': ptraining,
                                                         'dates': dates})
    else:
        return redirect('/')


@login_required
def date_info(request, date_id):
    user = request.user
    if user.is_coach():
        date = TrainingDate.objects.get(id=date_id)
        users = []
        marks = []
        for u in date.users.all():
            users.append(u.user)
        if users:
            for u in users:
                if TrainingResult.objects.filter(user=u, planned_training=date.date_choice).first():
                    marks.append(TrainingResult.objects.filter(user=u, planned_training=date.date_choice).first().result)
                else:
                    marks.append(-1)
        users_marks = zip(users, marks)
        if request.method == 'POST':
            if users:
                for user in users:
                    if request.POST[str(user.id)]:
                        try:
                            result = request.POST[str(user.id)]
                            if TrainingResult.objects.filter(user=user, planned_training=date.date_choice).last():
                                tr_result = TrainingResult.objects.filter(user=user, planned_training=date.date_choice).last()
                                tr_result.result = result
                                tr_result.save()
                            else:
                                tr_result = TrainingResult(user=user, planned_training=date.date_choice, training = date.date_choice.training, result=result)
                                tr_result.save()
                        except:
                            pass
                return redirect('/training/date/{0}/'.format(date.id))
        return render(request, 'coach/date_info.html', {'users_marks': users_marks, 'user': user,
                                                        'date': date})
    else:
        return redirect('/')


@login_required
def date_marks(request, date_id):
    user = request.user
    if user.is_coach():
        date = TrainingDate.objects.get(id=date_id)
        users = []
        marks = []
        for u in date.users.all():
            users.append(u.user)
        if users:
            for u in users:
                if TrainingResult.objects.filter(user=u, planned_training=date.date_choice).first():
                    marks.append(TrainingResult.objects.filter(user=u, planned_training=date.date_choice).first().result)
                else:
                    marks.append(-1)
        users_marks = zip(users, marks)
        return render(request, 'coach/date_marks.html', {'users_marks': users_marks, 'user': user,
                                                         'date': date})
    else:
        return redirect('/')


@login_required
def save_dates(request):
    user = request.user
    if user.is_coach():
        if request.method == 'POST':
            path = request.POST['path']
            training_id = int(request.POST['training'])
            training = PlannedTraining.objects.get(id=training_id)
            users = training.allowed_users.all()
            if users:
                for u in users:
                    post = '*' + str(u.id)
                    if post in request.POST:
                        if not UserlistByDate.objects.filter(user=u, training=training).first():
                            date = TrainingDate.objects.get(id=int(request.POST[post]))
                            user_date = UserlistByDate(user=u, date=date, training=training)
                            user_date.save()
                        else:
                            user_date = UserlistByDate.objects.filter(user=u, training=training).first()
                            date = TrainingDate.objects.get(id=int(request.POST[post]))
                            user_date.date = date
                            user_date.save()
            return redirect(path)
        else:
            return redirect('/')
    else:
        return redirect('/')


@login_required
def faq(request):
    user = request.user
    faqs = Faq.objects.all()
    return render(request, 'users/faq.html', {'user': user, 'faqs': faqs})


def support(request):
    if request.method == 'POST':
        post = request.POST.copy()
        message = 'Спасибо за обращение. Мы обязательно ответим вам.'
        buttons = []
        text = ''
        for x in post:
            text+=x+': ' + post[x]+ '\n'
        from mailing.mailgun import send_simple_message
        subject = request.GET.get('subject', 'Нет темы') + ' ' +request.POST['Email - так, как он записан в DMD']
        mail = request.POST['Email - так, как он записан в DMD']
        send_simple_message(text, subject, mail)
        return render(request, 'support/support.html', {'message':message, 'buttons':buttons})
    #Стартовая страница
    message = 'Выберите проблему, которая у вас возникла'
    buttons = [['Я не помню или не знаю свой пароль','password'],
                ['Я не знаю свои данные для входа','logopass'],
                ['Пишет, что мой email не существует','no_email'],
                ['У меня нет курса/теста, который у меня должен быть','no_course'],
                ['Я не получил приглашение','no_invite'],
                ['Другой вопрос','other']]
    problem = request.GET.get('problem', '')
    #Проблемы с паролем
    if problem == 'password':
        message = 'Если вы забыли ваш пароль, воспользуйтесь формой восстановления на странице входа'
        buttons = [['Все ок', 'ok'], ['Мне не пришло письмо', 'no_password_letter'], ['Пишет, что такого email не существует', 'no_email']]
    if problem == 'no_password_letter':
        message = 'Напишите письмо с любым содержанием на адрес renault@kpi-ua.com, спустя 20 минут попробуйте восстановить пароль еще раз'
        buttons = [['Готово', 'go_login'],['Я отправил письмо, подождал 20 минут, а письма все равно не приходят', 'pass_form'] ]
    if problem == 'no_email':
        message = 'Проверьте правильность указания почты в системе DMD. Если вы внесли изменения сегодня, то они попадут к нам в систему только завтра. Если вы внесли изменения вчера или раньше - нажмите на кнопку "Нужна синхронизация" и заполните форму.'
        buttons = [['Все ок', 'go_login'], ['Нужна синхронизация', 'sync_form']]
    if problem == 'no_invite':
        message = 'Проверьте правильность данных внесенных в DMD. Если все правильно, напишите письмо с любым содержанием на адрес renault@kpi-ua.com'
        buttons = [['Я отправил письмо', 'invite']]
    if problem == 'invite' or problem == 'logopass':
        message = 'Для входа в систему используйте вашу почту, указанную в DMD. Если вы забыли или не знаете ваш пароль, используйте кнопку восстановления на странице входа. Обучения находятся в разделе "Обучение->Дистанционное", а тестирования в разделе "Тестирование"'
        buttons = [['Я понял', 'go_login']]
    if problem == 'no_course':
        message = 'Если вы уверены, что должны проходить сейчас курс, получили приглашение или уверены, что не получили его по ошибке и в вашем аккаунте нет доступа к курсу - заполните форму ниже'
        subject = 'Нет курса' 
        fields = ['Email - так, как он записан в DMD', 'Фамилия и имя так, как они записаны в DMD', 'Курс/тест, который вы сейчас должны проходить', 'Дата внесения последних изменений в DMD']
        return render(request, 'support/support_form.html', {'message': message, 'fields':fields, 'subject': subject})
    if problem == 'go_login':
        return redirect('/login') 
    if problem == 'sync_form':
        subject = 'Нужна синхронизация' 
        message = 'Заполните форму с данными и мы вам ответим на вашу почту.'
        fields = ['Email - так, как он записан в DMD', 'Фамилия и имя так, как они записаны в DMD', 'Курс/тест, который вы сейчас должны проходить', 'Дата внесения последних изменений в DMD']
        return render(request, 'support/support_form.html', {'message':message, 'fields':fields, 'subject': subject})
    if problem == 'pass_form':
        subject = 'Проблемы с восстановлением пароля' 
        message = 'Заполните форму с данными и мы вам ответим на вашу почту.'
        fields = ['Email - так, как он записан в DMD', 'Фамилия и имя так, как они записаны в DMD', 'Дата внесения последних изменений в DMD']
        return render(request, 'support/support_form.html', {'message':message, 'fields':fields, 'subject': subject})
    if problem == 'other':    
        subject = 'Другой вопрос' 
        message = 'Заполните форму с данными и мы вам ответим на вашу почту.'
        fields = ['Email - так, как он записан в DMD', 'Фамилия и имя так, как они записаны в DMD']
        return render(request, 'support/support_form.html', {'message':message, 'fields':fields, 'subject': subject})
    
    return render(request, 'support/support.html', {'message':message, 'buttons':buttons})
    
    
    
def regroup(request):
    users = User.objects.all()
    g1 = Group.objects.get(id=3)
    g2 = Group.objects.get(id=5)
    for user in users:
        if user.username == 'noname':
            g1.user_set.remove(user)
            g2.user_set.add(user)
    return HttpResponse('Done')
    

def clear(user_id, ptest_id):
    user = User.objects.get(id=user_id)
    ptest = PlannedTest.objects.get(id=ptest_id)
    
    testresults = TestResults.objects.filter(user=user, ptest=ptest).all()
    for t in testresults:
        t.delete()
    results  = Result.objects.filter(user=user, ptest=ptest).all()
    for r in results:
        r.delete()
    finish_result = MainResult.objects.filter(user=user, ptest=ptest).last()
    finish_result.delete()
    return HttpResponse('Done!')
    
    
def sync(request):
    from users.models import Sync
    from users.tools import users_sync
    message = None
    if request.method == 'POST':
        if request.FILES['file'].name.split('.')[-1] == "csv":
            file = request.FILES['file']
            new_sync = Sync(file=file)
            new_sync.save()
            new, old, inactive, no_changes = users_sync(new_sync.file.path)
            message = 'Синхронизация прошла успешно. Добавлено новых пользователей: %s, изменено существующих пользователей: %s, неактивных пользователей: %s' %(new, old, inactive)
            message += ', пользователей без изменений: %s' % no_changes
        else:
            message = 'Не правильное расширение файла. Попробуйте загрузить снова'
    return render(request, 'sync/sync.html', {'message': message})


def calendar(request):
    from users.tools import get_events
    user = request.user
    if user.is_user() or user.is_dist() or user.is_dealer():
        events = get_events(user.id)
        return render(request, 'users/calendar.html', {'events': events})
    else:
        return redirect('/')


def course_choice(request):
    from newcourse.models import StudyCourse
    courses = StudyCourse.objects.all()
    if request.method == 'POST':
        courses = request.POST.getlist('course')
        course_id = int(courses[0])
        return redirect('/item_choice/{0}/'.format(course_id))
    return render(request, 'raiting/course_choice.html', {'courses': courses})


def item_choice(request, course_id):
    from newcourse.models import StudyCourse
    course = StudyCourse.objects.get(id=course_id)
    message = request.GET.get('message', '')
    return render(request, 'raiting/item_choice.html', {'course': course, 'message': message})



def test_to_raiting(request, course_id):
    from newcourse.models import StudyCourse, CourseResult, Level1, Level1Part
    from etest.tools import time
    all_tests = PlannedTest.objects.filter(date_end__lt=time())
    tests = []
    for test in all_tests:
        if not Level1Part.objects.filter(test=test).first():
            tests.append(test)
    course = StudyCourse.objects.get(id=course_id)
    if request.method == 'POST':
        tests = request.POST.getlist('tests')
        test = PlannedTest.objects.get(id=int(tests[0]))
        for user in test.allowed_users.all():
            if MainResult.objects.filter(ptest=test, user=user).last():
                mark = MainResult.objects.filter(ptest=test, user=user).last().mark
            else:
                mark = 0
            course_result = CourseResult(user=user, mark=float(mark), course=course, temporary=False, for_raiting=True)
            course_result.save()
            course.allowed_users.add(user)
            course.save()
        level1 = Level1(study_course=course, finish_mark='>=80', next_step_mark='<80')
        level1.save()
        level1part = Level1Part(course_part=level1, test=test)
        level1part.save()
        return redirect('/item_choice/{0}/?message=Тестирование переведено в рейтинговую систему успешно'.format(course_id))
    return render(request, 'raiting/test_to_raiting.html', {'course': course, 'tests': tests})



def elearining_to_raiting(request, course_id):
    from newcourse.models import StudyCourse, CourseResult, Level2, Level2Part
    from etest.tools import time
    from elearning.models import Elearning_mark
    all_ecourses = PlannedCourse.objects.filter(date_end__lt=time(), for_test=False).all()
    ecourses = []
    for ecourse in all_ecourses:
        if not Level2Part.objects.filter(ecourse=ecourse).first():
            ecourses.append(ecourse)
    course = StudyCourse.objects.get(id=course_id)
    if request.method == 'POST':
        ecourses = request.POST.getlist('ecourses')
        ecourse = PlannedCourse.objects.get(id=int(ecourses[0]))
        for user in ecourse.allowed_users.all():
            if Elearning_mark.objects.filter(pcourse=ecourse, user=user).last():
                mark = Elearning_mark.objects.filter(pcourse=ecourse, user=user).last().mark
            else:
                mark = 0
            course_result = CourseResult(user=user, mark=float(mark), course=course, temporary=False, for_raiting=True)
            course_result.save()
            course.allowed_users.add(user)
            course.save()
        level2 = Level2(study_course=course, finish_mark='>=80', next_step_mark='<80', previous_step_mark='null')
        level2.save()
        level2part = Level2Part(course_part=level2, ecourse=ecourse)
        level2part.save()
        return redirect('/item_choice/{0}/?message=ДО переведено в рейтинговую систему успешно'.format(course_id))
    return render(request, 'raiting/elearning_to_raiting.html', {'course': course, 'ecourses': ecourses})


def training_to_raiting(request, course_id):
    from newcourse.models import StudyCourse, CourseResult, Level3, Level3Part
    from etest.tools import time
    from trainings.models import TrainingResult
    all_trainings = set(PlannedTraining.objects.all())
    trainings = []
    for tr in all_trainings:
        if not Level3Part.objects.filter(training=tr).first():
            trainings.append(tr)
    course = StudyCourse.objects.get(id=course_id)
    if request.method == 'POST':
        trainings = request.POST.getlist('trainings')
        training = PlannedTraining.objects.get(id=int(trainings[0]))
        for user in training.allowed_users.all():
            if TrainingResult.objects.filter(planned_training=training, user=user).last():
                mark = TrainingResult.objects.filter(planned_training=training, user=user).last().result
            else:
                mark = 0
            course_result = CourseResult(user=user, mark=float(mark), course=course, temporary=False, for_raiting=True)
            course_result.save()
            course.allowed_users.add(user)
            course.save()
        level3 = Level3(study_course=course, finish_mark='>=80', next_step_mark='<80', previous_step_mark='null')
        level3.save()
        level3part = Level3Part(course_part=level3, training=training)
        level3part.save()
        return redirect('/item_choice/{0}/?message=Очное обучение переведено в рейтинговую систему успешно'.format(course_id))
    return render(request, 'raiting/training_to_raiting.html', {'course': course, 'trainings': trainings})


def dist_rating(request):
    from newcourse.reiting import overall_rating, deps_rating, raiting_dealer
    user = request.user
    if user.is_dist():
        overall_rating = overall_rating()
        deps_rating = deps_rating()
        dealers = []
        ratings = []
        dealer_deps = []
        deps = Departament.objects.all()
        for dealer in User.objects.filter(groups__id=1).all():
            rating, dealer_deps_rating = raiting_dealer(dealer.id)
            ratings.append(rating)
            dealers.append(dealer)
            dealer_deps.append(dealer_deps_rating)
        total_dealers_rating = zip(dealers, dealer_deps, ratings)
        return render(request, 'users/rating.html', {'user': user,
                                                     'overall_rating': overall_rating,
                                                     'deps': deps,
                                                     'deps_rating': deps_rating,
                                                     'total_dealers_rating': total_dealers_rating})


def dist_dealer_dep(request, dealer_id, dep_id):
    from newcourse.reiting import raiting_count
    user = request.user
    if user.is_dist():
        user = request.user
        dealer = User.objects.get(id=dealer_id)
        dep = Departament.objects.get(id=dep_id)
        jobs = UserJob.objects.filter(departament=dep).all()
        users = User.objects.filter(job=jobs, parentId=dealer).all()
        ratings = []
        for u in users:
            ratings.append(raiting_count(u.id))
        users_ratings = zip(users, ratings)

        return render(request, 'users/dist_dealer_dep.html', {'user': user, 'dealer': dealer,
                                                              'users_ratings': users_ratings})
    else:
        return redirect('/')


def dealer_dep_info(request, dep_id):
    from newcourse.reiting import raiting_count
    user = request.user
    if user.is_dealer():
        dep = Departament.objects.get(id=dep_id)
        jobs = UserJob.objects.filter(departament=dep)
        users = User.objects.filter(parentId=user, job=jobs).all()
        ratings = []
        for u in users:
            ratings.append(raiting_count(u.id))
        users_ratings = zip(users, ratings)
        return render(request, 'users/dist_dealer_dep.html', {'user': user,
                                                              'users_ratings': users_ratings})
    else:
        return redirect('/')


def dep_course(request, dep_name):
    from newcourse.reiting import dealer_dep_rating
    dep = Departament.objects.get(name=dep_name)
    dealers = User.objects.filter(groups__id=1).all()
    data = []
    for dealer in dealers:
        c_res, m_res = dealer_dep_rating(dealer.id, dep.id)
        if c_res[1]:
            data.append(c_res)
    courses = []
    for d in data:
        if not d[1][0][0] in courses:
            courses.append(d[1][0][0])
    data2 = []
    for d in data:
        my_data = []
        my_data.append(d[0])
        marks = []
        for course in courses:
            if course in [j[0] for j in d[1]]:
                mark = [j[1] for j in d[1] if j[0] == course]
                marks.append(mark[0])
            else:
                marks.append('-')
        my_data.append(marks)
        my_data.append(d[2])
        data2.append(my_data)
    return render(request, 'raiting/dep_course.html', {'data': data, 'courses': courses, 'data2': data2})


def dist_courses_view(request):
    from newcourse.models import StudyCourse
    user = request.user
    if user.is_dist():
        courses = list(StudyCourse.objects.all())
        for course in courses:
            print(time())
            print(course.get_date_end())
            if course.get_date_end() < time():
                courses.remove(course)
        return render(request, 'users/dist_courses_view.html', {'user': user, 'courses': courses})
    else:
        return redirect('/')


def dist_course_info(request, course_id):
    from newcourse.models import StudyCourse
    from elearning.models import Elearning_mark
    user = request.user
    if user.is_dist():
        course = StudyCourse.objects.get(id=course_id)
        course_tests = course.get_tests()
        test_users = []
        if course_tests:
            for test in course_tests:
                users_wo_mark = []
                for u in test.allowed_users.all():
                    if not MainResult.objects.filter(user=u, ptest=test):
                        users_wo_mark.append(u)
                test_users.append(users_wo_mark)
            tests = zip(course_tests, test_users)
        else:
            tests = None
        course_ecourses = course.get_ecourses()
        ecourse_users = []
        if course_ecourses:
            for ecourse in course_ecourses:
                users_wo_mark = []
                for u in ecourse.allowed_users.all():
                    if not Elearning_mark.objects.filter(user=u, pcourse=ecourse):
                        users_wo_mark.append(u)
                ecourse_users.append(users_wo_mark)
            ecourses = zip(course_ecourses, ecourse_users)
        else:
            ecourses = None
        course_trainings = course.get_trainings()
        training_users = []
        if course_trainings:
            for tr in course_trainings:
                users_wo_mark = []
                for u in tr.allowed_users.all():
                    if not TrainingResult.objects.filter(user=u, planned_training=tr):
                        users_wo_mark.append(u)
                training_users.append(users_wo_mark)
            trainings = zip(course_trainings, training_users)
        else:
            trainings = None
        return render(request, 'users/dist_course_info.html', {'user': user, 'course': course,
                                                               'tests': tests, 'ecourses': ecourses,
                                                               'trainings': trainings})
    else:
        return redirect('/')


@login_required
def user_rating(request, user_id):
    from newcourse.models import CourseResult
    from newcourse.reiting import raiting_count
    current_user = request.user
    if current_user.is_dist():
        try:
            user = User.objects.get(id=user_id)
            courses = CourseResult.objects.filter(user=user, for_raiting=True).all()
            rating = raiting_count(user.id)
            return render(request, 'users/user_rating.html', {'current_user': current_user,
                                                              'user': user,
                                                              'courses': courses,
                                                              'rating': rating})
        except:
            return HttpResponse('Такого пользователя не существует')
    else:
        return redirect('/')


@login_required
def user_course_info(request, user_id, course_id):
    from newcourse.models import StudyCourse
    from elearning.models import Elearning_mark
    current_user = request.user
    if current_user.is_dist():
        try:
            user = User.objects.get(id=user_id)
            course = StudyCourse.objects.get(id=course_id)

            results = []
            if course.level1.first():
                levels1 = course.level1.all()
                for i in levels1:
                    results.append(['Тестирование',
                                    i.test_parts.first().test,
                                    MainResult.objects.filter(ptest=i.test_parts.first().test,
                                                              user=user).first().mark])

            if course.level2.first():
                levels2 = course.level2.all()
                for i in levels2:
                    results.append(['Дистанционное обучение',
                                    i.test_parts.first().test,
                                    Elearning_mark.objects.filter(pcourse=i.eparts.first().ecourse,
                                                                  user=user).first().mark])

            if course.level3.first():
                levels3 = course.level3.all()

                for i in levels3:
                    training = i.training_parts.first().training
                    if TrainingResult.objects.filter(user=user,
                                                     planned_training=training).first():
                        results.append(['Очное обучение',
                                        training,
                                        TrainingResult.objects.filter(planned_training=training,
                                                                      user=user).first().result])
            return render(request, 'users/user_course_info.html', {'current_user': current_user,
                                                                   'user': user,
                                                                   'results': results})
        except:
            return HttpResponse('Данные указаны не верно!')


        pass
    else:
        return redirect('/')
