from .models import User
from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm


class AdminUserAddForm(UserCreationForm):
    username = forms.CharField(widget=forms.HiddenInput, initial='noname')
    class Meta:
        model = User
        fields = ("username",) 
    def clean_username(self):
        username = self.cleaned_data["username"]
        return username
    

class AdminUserChangeForm(UserChangeForm):
    username = forms.CharField(widget=forms.HiddenInput)
    class Meta:
        model = User
        fields = "__all__"
        
