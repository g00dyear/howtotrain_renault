from django.shortcuts import render, redirect
from trainings.models import *
from users.models import User
from django.contrib.auth.models import Group


# Страница со списком тренингов
def main_training(request):
    user = request.user
    
    if user.is_user():
        trainings = user.trainings.all()
        dates = []
        for training in trainings:
            if UserlistByDate.objects.filter(training=training, user=user).last():
                x = UserlistByDate.objects.filter(training=training, user=user).last()
                dates.append(x)
            else:
                dates.append(0)
        tabledata = zip(trainings, dates)
        return render(request, 'training/main_training.html', {'user': user, 'tabledata': tabledata})
        
    elif user.is_dist():
        tlist = []
        subchilds = Group.objects.get(pk=3).user_set.all()
        for subchild in subchilds:
            tlist += PlannedTraining.objects.filter(allowed_users=subchild)
        trainings = list(set(tlist))
        return render(request, 'training/trainings_for_stuff.html', {'trainings':trainings})
        
    elif user.is_dealer():
        tlist = []
        for child in user.childs.all():
            tlist += PlannedTraining.objects.filter(allowed_users=child)
        trainings = list(set(tlist))
        return render(request, 'training/trainings_for_stuff.html', {'trainings':trainings})
        
            
# Страница тренинга ( подробно о тренинге + запись на конкретную дату )
def training_info(request, training_id):
    user = request.user
    training = PlannedTraining.objects.get(id=training_id)
    dates = training.dates.all().order_by('date')
    free_seats = []
    for date in dates:
        if (date.max_users - date.users.count()) > 0:
            free_seats.append(date.max_users - date.users.count())
        else:
            free_seats.append(0)
    dates2 = zip(dates, free_seats)
    if user.is_user():
        return render(request, 'training/training_info.html', {'user': user, 'training': training, 'dates': dates2})

    elif user.is_dist():
        tr = []
        for date in dates:
            results = []
            users = [d.user for d in date.users.all()]
            print(users)
            if users:
                for user in users:
                    if TrainingResult.objects.filter(planned_training=training, user=user).first():
                        results.append([
                            user,
                            user.parentId,
                            TrainingResult.objects.filter(planned_training=training, user=user).first().result,
                        ])
                    else:
                        results.append([
                            user,
                            user.parentId,
                            '-'
                        ])

            tr.append(results)

        date_results = zip(dates, tr)
        return render(request, 'training/training_info_for_stuff.html', {'user': user, 'training': training,
                                                                         'date_results': date_results})

    elif user.is_dealer():
        users_results = []
        for date in dates:
            marks = []
            users = []
            for u in date.users.all():
                if u.user in user.childs.all():
                    print('success')
                    users.append(u.user)
                    if TrainingResult.objects.filter(user=u.user, planned_training=training).last():
                        marks.append(TrainingResult.objects.filter(user=u.user, planned_training=training).last().result)
                    else:
                        marks.append('-')
            print(users, marks)
            results = zip(users, marks)
            users_results.append(results)
        dates_marks = zip(dates, users_results)
        return render(request, 'training/training_info_for_stuff.html', {'user': user, 'training': training,
                                                                         'dates_marks': dates_marks})
    
    
# Создание пары юзер-дата для тренинга
def user_date_add(request, date_id, training_id):
    user = request.user
    date = TrainingDate.objects.get(id = date_id)
    training = PlannedTraining.objects.get(id = training_id)
    if not UserlistByDate.objects.filter(user=user, training=training).first():
        user_date = UserlistByDate(user=user, date=date, confirmed=True, training=training)
        user_date.save()
    else:
        user_date = UserlistByDate.objects.filter(user=user, training=training).last()
        user_date.date = date
        user_date.save()
    return redirect('/trainings/')