from django.contrib import admin
from trainings.models import Training, PlannedTraining, TrainingDate, TrainingResult, UserlistByDate, StudyLevel

# Register your models here.
class TrainingDateInline(admin.TabularInline):
    model = TrainingDate
    extra = 3
    
    
class PlannedTrainingAdmin(admin.ModelAdmin):
    filter_horizontal = ("allowed_users",)
    inlines = [TrainingDateInline]
    
    
class TrainingAdmin(admin.ModelAdmin):
    filter_horizontal = ('auditory',)
    
    
admin.site.register(StudyLevel)
admin.site.register(Training, TrainingAdmin)
admin.site.register(TrainingDate)
admin.site.register(PlannedTraining, PlannedTrainingAdmin)
admin.site.register(TrainingResult)
admin.site.register(UserlistByDate)