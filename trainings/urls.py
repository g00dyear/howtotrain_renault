from django.conf.urls import patterns, include, url
from trainings import views

urlpatterns = patterns('',
        url(r'^$', views.main_training),
        url(r'^traininginfo/(?P<training_id>\d+)/$', views.training_info),
        url(r'^datechoice/(?P<date_id>\d+)/(?P<training_id>\d+)/$', views.user_date_add),
        

)
