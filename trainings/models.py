from django.db import models
from users.models import User, UserJob
import pytz
from django.utils import timezone
from ckeditor.fields import RichTextField
from django.db.models import Q
import pytz
from django.utils import timezone
from .utils import make_mail, create_training_events
# Create your models here.


class Training(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название")
    description = RichTextField(blank=True, null=True, verbose_name="Описание")
    short_description = models.CharField(max_length=255, blank=True, null=True, verbose_name="Краткое описание")
    auditory = models.ManyToManyField(UserJob, verbose_name="Целевая аудитория", blank=True, null=True)
    required = models.BooleanField(default=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Очное обучение'
        verbose_name_plural = "Очные обучения"
                           

class TrainingManager(models.Manager):
    kiev = pytz.timezone('Europe/Kiev')
    time = timezone.localtime(timezone.now(), timezone=kiev)

    def past(self):
        return set(self.filter(dates__date__lt=self.time).all())


class PlannedTraining(models.Model):
    training_name = models.CharField(max_length=150, blank=True, null=True, verbose_name='Отображаемое имя')
    training = models.ForeignKey(Training, verbose_name="Тренинг")
    rules = RichTextField(blank=True, null=True, verbose_name="Правила")
    allowed_users = models.ManyToManyField(User, related_name='trainings', verbose_name="Пользователи", db_index=True, blank=True, null=True)
    test_before = models.ForeignKey('etest.PlannedTest', blank=True, null=True, related_name='tests_before', verbose_name="Тест до тренинга", limit_choices_to={'for_training': True})
    test_after = models.ForeignKey('etest.PlannedTest', blank=True, null=True, related_name='tests_after', verbose_name="Тест после тренинга", limit_choices_to={'for_training': True})
    place = models.CharField(max_length=512, blank=True, null=True, verbose_name="Место проведения")
    coach = models.ForeignKey('users.User', blank=True, null=True, verbose_name="Тренер", limit_choices_to=Q(groups=7))
    org = models.CharField(max_length=128, blank=True, null=True, verbose_name="Организатор")
    coach_phone = models.CharField(max_length=128, blank=True, null=True, verbose_name="Телефон тренера")
    org_phone = models.CharField(max_length=128, blank=True, null=True, verbose_name="Телефон организатора")
    finished = models.BooleanField(default=False, verbose_name='Завершен')
    mailing = models.BooleanField(default=False)
    objects = TrainingManager()
        
    def __str__(self):
        if self.training_name:
            return self.training_name
        else:
            return self.training.name

    class Meta:
        verbose_name = 'Запланированное очное обучение'
        verbose_name_plural = "Запланированные очные обучения"

    def get_users(self):
        users = list(self.allowed_users.all())
        jobs = self.training.auditory.all()
        auditory = list(User.objects.filter(job=jobs).all())
        for user in auditory:
            if user not in users:
                users.append(user)
        return users

    @property
    def auditory_list(self):
        jobs = self.training.auditory.all()
        string = ''
        for job in jobs:
            string += str(job.name) + ', '
        return string
        

class TrainingDate(models.Model):
    date = models.DateTimeField(verbose_name="Дата проведения")
    max_users = models.IntegerField(default=15, verbose_name="Максимальное количество участников")
    date_choice = models.ForeignKey(PlannedTraining, related_name='dates', verbose_name="Тренинг")

    class Meta:
        verbose_name = 'Дата тренинга'
        verbose_name_plural = "Даты тренингов"

    def save(self, *args, **kwargs):
        super(TrainingDate, self).save(*args, **kwargs) # Call the "real" save() method.

        if self.date_choice.mailing:
            create_training_events(self)
    
    
class TrainingResult(models.Model):
    user = models.ForeignKey(User)
    training = models.ForeignKey(Training, null=True, blank=True)
    result = models.FloatField()
    planned_training = models.ForeignKey(PlannedTraining, blank=True, null=True)
    date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Результат тренинга'
        verbose_name_plural = "Результаты тренингов"

    def save(self, *args, **kwargs):
        super(TrainingResult, self).save(*args, **kwargs) # Call the "real" save() method.
        # Проверяем наличие принадлежности к определенному курсу.
        from newcourse.steps import level3
        level3(self.planned_training, self.result, self.user)

        if self.planned_training.mailing:
            make_mail(self.result)
        

class UserlistByDate(models.Model):
    user = models.ForeignKey(User, db_index=True)
    date = models.ForeignKey(TrainingDate, null=True, blank=True, related_name='users')
    training = models.ForeignKey(PlannedTraining, related_name='chosen_date', db_index=True)
    confirmed = models.NullBooleanField()

    class Meta:
        verbose_name = 'Пользователь по дате'
        verbose_name_plural = "Пользователи по датам"
        
        
class StudyLevel(models.Model):
    user = models.ForeignKey(User, db_index=True, related_name='study_level')
    mark = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now=True)
