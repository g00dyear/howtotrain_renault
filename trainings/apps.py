from django.apps import AppConfig


class TrainingConfig(AppConfig):
    name = 'trainings'
    verbose_name = 'Очные обучения'