from mailing.models import MessageEvent
from datetime import datetime, timedelta
from mailing.message_templates import *
from mailing.mailgun import send_message


def make_mail(result):
    from newcourse.utils import translate_training_messages

    if result.mark >= 80:
        message, subject = translate_training_messages(training_success_text,
                                                       training_success_subject,
                                                       result.user,
                                                       result.planned_training,
                                                       result.result)
    else:
        message, subject = translate_training_messages(training_unsuccess_text,
                                                       training_unsuccess_subject,
                                                       result.user,
                                                       result.planned_course,
                                                       result.result)
    send_message(message, subject, 'olegusov90@gmail.com')


def create_training_events(date):
    if not MessageEvent.objects.filter(training=date).first():
        # Create start event
        start_event = MessageEvent(date=date.date,
                                   training=date,
                                   event_type='start')
        start_event.save()

        # create invitation event
        inv_event = MessageEvent(date=datetime.now(),
                                 training=date,
                                 event_type='invite')
        inv_event.save()

        # create reminder events
        # reminder before 1 day to end
        reminder = MessageEvent(date=(date.date - timedelta(days=1)),
                                training=date,
                                event_type='reminder1')
        reminder.save()

    else:
        events = MessageEvent.objects.filter(training=date,
                                             sent=False).all()
        for event in events:
            if event.event_type == 'start':
                if event.date != date.date:
                    event.date = date.date

            if event.event_type == 'invite':
                if event.date.day != datetime.now().day:
                    event.date = datetime.now()

            if event.event_type == 'reminder1':
                if event.date != (date.date - timedelta(days=1)):
                    event.date = (date.date - timedelta(days=1))

            event.save()
