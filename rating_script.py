from newcourse.models import CourseResult, StudyCourse
from elearning.models import Elearning_mark, PlannedCourse
from etest.models import PlannedTest, MainResult


def generate_marks(ecourse_id, course_id):
    string = ''
    # Получаем элементы для перевода: курс и ДО
    course = StudyCourse.objects.get(id=course_id)
    ecourse = PlannedCourse.objects.get(id=ecourse_id)

    # Выполняем проверку двух условий:
    # 1. Если оценка больше либо равна 80
    # 2. Есть ли у человека с оценкой больше либо равной 80 оценкой, результат по прохождению ОО
    # Достанем пользователей, результаты которых подходят под первый пункт
    results = Elearning_mark.objects.filter(pcourse=ecourse).all()
    users = [[r.user, r.mark] for r in results if r.mark >= 80]

    # Если у человека в списке нету оценки по курсу, то он получает ее. Если есть, то оценка остается прежней.
    # Тут же проверим и второй курс по той же методике, если он задан.
    for user in users:
        if not CourseResult.objects.filter(user=user[0], course=course).first():
            new_result = CourseResult(user=user[0], mark=user[1], course=course, temporary=False)
            new_result.save()
            string += 'Пользователь:' + user[0].first_name + ' ' + user[0].last_name + ';\n'
            string += 'Оценка пользователя по дистанционному обучению: ' + str(user[1]) + ';\n'
            string += 'Оценка, перенесенная в курс "{0}": {1}; \n'.format(course.name, new_result.mark)
    if not string:
        string = 'Результаты не добавлены!'
    return string


def generate_marks_with_attestation(test_id, ecourse_id, course_id):
    new_marks = []
    # Получаем элементы
    course = StudyCourse.objects.get(id=course_id)
    ecourse = PlannedCourse.objects.get(id=ecourse_id)
    test = PlannedTest.objects.get(id=test_id)

    # Условия
    # 1. Проходимся по всем тестировавшимся людям
    # 2. Проверяем условия: а) Оценка по аттестации и по ДО больше 80, б) Оценка по аттестации и по ДО < 80,
    # в) Оценка по аттестации меньше 80, оценка по ДО больше 80.
    # 3. В случае выполнения ряда условий создаем недостающую оценку по курсу.

    test_results = MainResult.objects.filter(ptest=test).all()
    for r in test_results:
        # Получаем оценку по ДО. Она должна быть у каждого, кто прошел аттестацию.
        if not Elearning_mark.objects.filter(user=r.user, pcourse=ecourse).first():
            continue
        else:
            ecourse_mark = Elearning_mark.objects.filter(user=r.user, pcourse=ecourse).first().mark
        # Условие, когда оценка по аттестации больше 80 и по ДО больше 80. При отсутствии результата курса -
        # загоняем оценку по ДО в оценку по курсу. Если оценка по курсу есть, то она приоритетна.
        if r.mark >= 80 and ecourse_mark >= 80:
            if not CourseResult.objects.filter(user=r.user, course=course).first():
                course_mark = CourseResult(user=r.user,
                                           course=course,
                                           for_raiting=True,
                                           temporary=False,
                                           mark=ecourse_mark)
                course_mark.save()
                new_marks.append([r.user, r.mark, ecourse_mark, course_mark])
        # Условие, когда аттестация меньше 80, а ДО больше 80. Если оценки по курсу нету, то загоняем оценку из ДО.
        """
        if r.mark < 80 and ecourse_mark >= 80:
            if not CourseResult.objects.filter(user=r.user, course=course).first():
                course_mark = CourseResult(user=r.user,
                                           course=course,
                                           for_raiting=True,
                                           temporary=False,
                                           mark=ecourse_mark)
                course_mark.save()
                new_marks.append([r.user, r.mark, ecourse_mark, course_mark])
        """
        # Условие, когда обе оценки меньше 80. Курс не сдан. Если каким то образом оценка стоит, то меняем ее значение
        # на ноль.
        if r.mark < 80 and ecourse_mark < 80:
            if not CourseResult.objects.filter(user=r.user, course=course).first():
                course_mark = CourseResult(user=r.user,
                                           course=course,
                                           for_raiting=True,
                                           temporary=False,
                                           mark=0)
                course_mark.save()
                new_marks.append([r.user, r.mark, ecourse_mark, course_mark])
            else:
                course_mark = CourseResult.objects.filter(user=r.user, course=course).first()
                course_mark.mark = 0
                course_mark.save()
        # Хорошо сдали аттестацию, но плохо сдали ДО. Если у человека оценки по курсу нету, то ОО он не проходил.
        # Переносим оценку из ДО в курс.
        if r.mark >= 80 and ecourse_mark < 80:
            if not CourseResult.objects.filter(user=r.user, course=course).first():
                course_mark = CourseResult(user=r.user,
                                           course=course,
                                           for_raiting=True,
                                           temporary=False,
                                           mark=0)
                course_mark.save()
            else:
                course_mark = CourseResult.objects.filter(user=r.user, course=course).first()
                course_mark.mark = 0
                course_mark.save()
                new_marks.append([r.user, r.mark, ecourse_mark, course_mark])
    return new_marks
