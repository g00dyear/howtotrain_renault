from django.apps import AppConfig


class AuditoryConfig(AppConfig):
    name = 'auditory'
    verbose_name = 'Аудитория'