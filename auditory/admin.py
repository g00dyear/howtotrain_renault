from django.contrib import admin
from .models import *
from users.models import User
from etest.models import PlannedTest, MainResult
from elearning.models import PlannedCourse, Elearning_mark
from trainings.models import PlannedTraining, TrainingResult
from newcourse.models import CourseResult


class TestAuditoryAdmin(admin.ModelAdmin):

    def save_related(self, request, form, formsets, change):
        super(TestAuditoryAdmin, self).save_related(request, form, formsets, change)
        test = form.instance.ptest.test
        users = User.objects.filter(job=form.instance.auditory.all()).all()
        attestations = PlannedTest.objects.filter(test=test).all()
        for user in users:
            if not MainResult.objects.filter(user=user, ptest=attestations).first():
                form.instance.ptest.allowed_users.add(user)
        form.instance.ptest.save()


class EcourseAuditoryAdmin(admin.ModelAdmin):

    def save_related(self, request, form, formsets, change):
        super(EcourseAuditoryAdmin, self).save_related(request, form, formsets, change)
        ecourse = form.instance.ecourse.course
        users = User.objects.filter(job=form.instance.auditory.all()).all()
        attestations = PlannedCourse.objects.filter(course=ecourse).all()
        for user in users:
            if not Elearning_mark.objects.filter(user=user, pcourse=attestations).first():
                form.instance.ecourse.allowed_users.add(user)
        form.instance.ecourse.save()


class TrainingAuditoryAdmin(admin.ModelAdmin):

    def save_related(self, request, form, formsets, change):
        super(TrainingAuditoryAdmin, self).save_related(request, form, formsets, change)
        training = form.instance.training.training
        users = User.objects.filter(job=form.instance.auditory.all()).all()
        trainings = PlannedTraining.objects.filter(training=training).all()
        for user in users:
            if not TrainingResult.objects.filter(planned_training=trainings).first():
                form.instance.training.allowed_users.add(user)
        form.instance.training.save()


class CourseAuditoryAdmin(admin.ModelAdmin):

    def save_related(self, request, form, formsets, change):
        super(CourseAuditoryAdmin, self).save_related(request, form, formsets, change)
        if form.instance.course.course:
            users = User.objects.filter(job=form.instance.auditory.all()).all()
            for user in users:
                if not CourseResult.objects.filter(user=user, course=form.instance.course.course, for_raiting=True).first():
                    form.instance.course.allowed_users.add(user)
            form.instance.course.save()
        else:
            users = User.objects.filter(job=form.instance.auditory.all()).all()
            for user in users:
                form.instance.course.allowed_users.add(user)
            form.instance.course.save()


admin.site.register(TestAuditory, TestAuditoryAdmin)
admin.site.register(EcourseAuditory, EcourseAuditoryAdmin)
admin.site.register(TrainingAuditory, TrainingAuditoryAdmin)
admin.site.register(CourseAuditory, CourseAuditoryAdmin)
