from django.db import models
from users.models import User
from etest.models import PlannedTest, MainResult
from elearning.models import PlannedCourse, Elearning_mark
from trainings.models import PlannedTraining, TrainingResult
from newcourse.models import StudyCourse, CourseResult
# Create your models here.


class TestAuditory(models.Model):
    ptest = models.ForeignKey('etest.PlannedTest')
    auditory = models.ManyToManyField('users.UserJob')


class EcourseAuditory(models.Model):
    ecourse = models.ForeignKey('elearning.PlannedCourse')
    auditory = models.ManyToManyField('users.UserJob')


class TrainingAuditory(models.Model):
    training = models.ForeignKey('trainings.PlannedTraining')
    auditory = models.ManyToManyField('users.UserJob')


class CourseAuditory(models.Model):
    course = models.ForeignKey('newcourse.StudyCourse')
    auditory = models.ManyToManyField('users.UserJob')

