from django.apps import AppConfig


class CourseConfig(AppConfig):
    name = 'newcourse'
    verbose_name = 'Курсы'