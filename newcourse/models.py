from django.db import models
from users.models import User


# Create your models here.
class StudyCourse(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название курса')
    auditory = models.ManyToManyField('users.UserJob', blank=True, verbose_name='Аудитория')
    allowed_users = models.ManyToManyField('users.User', blank=True, verbose_name='Допущенные пользователи')
    course = models.ForeignKey('self', blank=True, null=True, verbose_name='Заменит курс')
    mailing = models.BooleanField(default=False, verbose_name='Разрешить рассылку')

    def __str__(self):
        return self.name

    def get_userlist(self):
        users = User.objects.filter(job=self.auditory.all()).all()
        print(users)
        for user in users:
            if user not in self.allowed_users.all():
                self.allowed_users.add(user)
        return self.allowed_users.all()

    class Meta:
        verbose_name_plural = 'Курсы'
        verbose_name = 'Курс'

    def get_date_start(self):
        if self.level1.first():
            date_start = self.level1.first().test_parts.order_by('test__date_start').first().test.date_start
        elif self.level2.first():
            date_start = self.level2.first().eparts.order_by('ecourse__date_start').first().ecourse.date_start
        elif self.level3.first():
            date_start = self.level3.first().training_parts.order_by('training__dates__date').first().training.dates.order_by('date').first().date
        else:
            date_start = None
        return date_start

    def get_date_end(self):
        if self.level3.first():
            date_end = self.level3.first().training_parts.order_by('training__dates__date').last().training.dates.order_by('date').last().date
        elif self.level2.first():
            date_end = self.level2.first().eparts.order_by('ecourse__date_end').last().ecourse.date_end
        elif self.level1.first():
            date_end = self.level1.first().test_parts.order_by('test__date_end').last().test.date_end
        else:
            date_end = None
        return date_end

    def get_tests(self):
        if self.level1.first():
            levels = self.level1.all()
            tests = []
            for level in levels:
                for part in level.test_parts.all():
                    tests.append(part.test)
            if len(tests):
                return tests
            else:
                return None

    def get_ecourses(self):
        if self.level2.first():
            levels = self.level2.all()
            ecourses = []
            for level in levels:
                for part in level.eparts.all():
                    ecourses.append(part.ecourse)
            if len(ecourses):
                return ecourses
            else:
                return None

    def get_trainings(self):
        if self.level3.first():
            levels = self.level3.all()
            trainings = []
            for level in levels:
                for part in level.training_parts.all():
                    trainings.append(part.training)
            if len(trainings):
                return trainings
            else:
                return None


class Level1(models.Model):
    study_course = models.ForeignKey(StudyCourse, blank=True, related_name='level1', verbose_name='Курсы')
    finish_mark = models.CharField(max_length=30, verbose_name='Условие завершения')
    next_step_mark = models.CharField(max_length=30, verbose_name='Условие перехода на следующий шаг')

    class Meta:
        verbose_name_plural = 'Уровни 1'
        verbose_name = 'Уровень 1'

    def __str__(self):
        return self.study_course.name


class Level1Part(models.Model):
    course_part = models.ForeignKey(Level1, related_name="test_parts", verbose_name='Часть уровня')
    test = models.OneToOneField('etest.PlannedTest', related_name='test_part', verbose_name='Тестирование')

    class Meta:
        verbose_name_plural = 'Аттестации'
        verbose_name = 'Аттестация'

    def __str__(self):
        return self.course_part.study_course.name + ' | ' + str(self.test.test)


class Level2(models.Model):
    study_course = models.ForeignKey(StudyCourse, blank=True, related_name='level2', verbose_name='Курс')
    finish_mark = models.CharField(max_length=30, verbose_name='Условие завершения')
    previous_step_mark = models.CharField(max_length=30, verbose_name='Условие возврата к предыдущему шагу')
    next_step_mark = models.CharField(max_length=30, verbose_name='Условие перехода к следующему шагу')

    class Meta:
        verbose_name_plural = 'Уровни 2'
        verbose_name = 'Уровень 2'

    def __str__(self):
        return self.study_course.name


class Level2Part(models.Model):
    course_part = models.ForeignKey(Level2, related_name='eparts', verbose_name='Часть уровня')
    ecourse = models.OneToOneField('elearning.PlannedCourse', related_name='ecourse_part', verbose_name='Дистанционное обучение')

    class Meta:
        verbose_name_plural = 'Дистанционные обучения'
        verbose_name = 'Дистанционное обучение'

    def __str__(self):
        return self.course_part.study_course.name + ' | ' + str(self.ecourse.course)


class Level3(models.Model):
    study_course = models.ForeignKey(StudyCourse, blank=True, related_name='level3', verbose_name='Курс')
    finish_mark = models.CharField(max_length=30, verbose_name='Условие завершения')
    previous_step_mark = models.CharField(max_length=30, verbose_name='Условие возврата к предыдущему шагу')
    next_step_mark = models.CharField(max_length=30, verbose_name='Условие перехода к следующему шагу')

    class Meta:
        verbose_name_plural = 'Уровни 3'
        verbose_name = 'Уровень 3'

    def __str__(self):
        return self.study_course.name


class Level3Part(models.Model):
    course_part = models.ForeignKey(Level3, related_name='training_parts', verbose_name='Часть уровня')
    training = models.OneToOneField('trainings.PlannedTraining', related_name='training_part', verbose_name='Очное обучение')

    class Meta:
        verbose_name_plural = 'Очные обучения'
        verbose_name = 'Очное обучение'

    def __str__(self):
        return self.course_part.study_course.name + ' | ' + str(self.training.training)


class CourseResult(models.Model):
    user = models.ForeignKey('users.User', db_index=True, verbose_name='Пользователь')
    course = models.ForeignKey(StudyCourse, db_index=True, verbose_name='Курс')
    mark = models.FloatField(verbose_name='Оценка')
    temporary = models.BooleanField(default=True, verbose_name='Временная?')
    for_raiting = models.BooleanField(default=True, verbose_name='Для рейтинга?')

    def save(self, *args, **kwargs):
        super(CourseResult, self).save(*args, **kwargs)  # Call the "real" save() method.
        from newcourse.reiting import renew_course_result
        renew_course_result(self.id, self.user)

    class Meta:
        verbose_name_plural = 'Результаты курсов'
        verbose_name = 'Результат курса'

    def __str__(self):
        return self.user.get_full_name() + ' | ' + self.course.name + ' | ' + str(self.mark)


class Event(models.Model):
    date = models.DateTimeField(verbose_name='Дата')
    event = models.CharField(max_length=200, verbose_name='Название события')
    course = models.ForeignKey(StudyCourse, related_name='events', verbose_name='Курс')
    finished = models.BooleanField(default=False, verbose_name='Завершен?')

    def __str__(self):
        return self.event

    class Meta:
        verbose_name_plural = 'События курсов'
        verbose_name = 'Событие курса'


class Sending(models.Model):
    user = models.ForeignKey('users.User')
    date = models.DateTimeField(auto_now=True)
    event = models.ForeignKey(Event, related_name='letters')

    def __str__(self):
        return self.user.get_full_name() + ' | ' + self.event.event + ' | ' + self.date.strftime("%d-%m")

    class Meta:
        verbose_name_plural = 'Отправленные письма'
        verbose_name = 'Отправленное письмо'
