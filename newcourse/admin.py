from django.contrib import admin
from newcourse.models import *
from nested_inline.admin import NestedModelAdmin, NestedStackedInline, NestedTabularInline
from easy_select2 import select2_modelform
# Register your models here.


class Level2PartInline(NestedTabularInline):
    model = Level2Part
    extra = 2


class Level2Inline(NestedTabularInline):
    model = Level2
    inlines = [Level2PartInline,]
    extra = 1

class Level1PartInline(NestedTabularInline):
    model = Level1Part
    extra = 2


class Level1Inline(NestedTabularInline):
    model = Level1
    inlines = [Level1PartInline,]
    extra = 1


class Level3PartInline(NestedTabularInline):
    model = Level3Part
    extra = 2


class Level3Inline(NestedTabularInline):
    model = Level3
    inlines = [Level3PartInline]
    extra = 1

StudyCourseForm = select2_modelform(StudyCourse, attrs={'width': '400px'})


class StudyCourseAdmin(NestedModelAdmin):
    form = StudyCourseForm
    inlines = [Level1Inline, Level2Inline, Level3Inline]
    filter_horizontal = ("allowed_users", "auditory",)

    def save_related(self, request, form, formsets, change):
        super(StudyCourseAdmin, self).save_related(request, form, formsets, change)
        from .reiting import course_results
        course_results(form.instance.id)
        users = list(form.instance.allowed_users.all())
        users2 = list(User.objects.filter(job=form.instance.auditory.all()).all())
        for user in users2:
            if user not in users:
                users.append(user)
        if form.instance.level1.first():
            test = form.instance.level1.first().test_parts.order_by('test__date_start').first().test
            for user in users:
                test.allowed_users.add(user)
            test.save()
        elif form.instance.level2.first():
            ecourse = form.instance.level2.first().eparts.order_by('ecourse__date_start').first().ecourse
            for user in users:
                ecourse.save()
                ecourse.allowed_users.add(user)
        elif form.instance.level3.first():
            training = form.instance.level3.first().training_parts.order_by('training__dates__date').first().training
            for user in users:
                training.save()
                training.allowed_users.add(user)


admin.site.register(StudyCourse, StudyCourseAdmin)
admin.site.register(Level1)
admin.site.register(Level2)
admin.site.register(Level3)
admin.site.register(Level1Part)
admin.site.register(Level2Part)
admin.site.register(Level3Part)
admin.site.register(CourseResult)
admin.site.register(Event)
admin.site.register(Sending)
