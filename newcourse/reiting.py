from .models import *
from users.models import User, Departament
from statistics import mean


def renew_course_result(course_result_id, user):
    result = CourseResult.objects.get(id=course_result_id)
    course = result.course
    try:
        later_result = CourseResult.objects.filter(user=user, course=course.course).last()
        later_result.for_reiting = False
        later_result.save()
    except:
        pass


def raiting_count(user_id):
    user = User.objects.get(id=user_id)
    results = CourseResult.objects.filter(for_raiting=True, user=user).all()
    total = 0
    for r in results:
        total += r.mark
    try:
        raiting = round(total/results.count(), 2)
        return raiting
    except:
        return None


def course_results(course_id):
    course = StudyCourse.objects.get(id=course_id)
    if course.auditory.first():
        users = User.objects.filter(job=course.auditory.all()).all()
        for u in users:
            if not CourseResult.objects.filter(user=u, course=course).first():
                c = CourseResult(user=u, course=course, mark=0, temporary=True)
                c.save()
    if course.allowed_users.first():
        for u in course.allowed_users.all():
            if not CourseResult.objects.filter(user=u, course=course).first():
                c = CourseResult(user=u, course=course, mark=0, temporary=True)
                c.save()


def raiting_dealer(dealer_id):
    deps = Departament.objects.all()
    dep_rating_middle = []
    marks = []
    for dep in deps:
        c_rating, m_rating = dealer_dep_rating(dealer_id, dep.id)
        dep_rating_middle.append(m_rating)
        marks.append(m_rating)
    deps_ratings = zip(deps, marks)
    dealer_rating = mean(dep_rating_middle)

    return dealer_rating, deps_ratings


def dealer_dep_rating(dealer_id, dep_id):
    dealer = User.objects.get(id=dealer_id)
    dep = Departament.objects.get(id=dep_id)
    jobs = dep.jobs.all()

    # получаем пользователей дилера из определенного отдела
    users = User.objects.filter(job=jobs, parentId=dealer, is_active=True).all()

    # получаем курсы
    results = CourseResult.objects.filter(user=users, for_raiting=True).all()
    if results:
        courses = set([r.course for r in results])
    else:
        courses = set(StudyCourse.objects.filter(allowed_users=any(users)).all())
    course_rating = {}
    course_finish_marks = []
    course_rating2 = []
    for course in courses:
        course_list = []
        try:
            c_results = mean([x.mark for x in CourseResult.objects.filter(course=course, user=users, for_raiting=True).all()])
        except:
            c_results = 0
        course_list.append(course.name)
        course_list.append(c_results)
        course_rating[course.name] = c_results
        course_finish_marks.append(course_list)
    try:
        middle_rating = mean([course_rating[x] for x in course_rating])
    except:
        middle_rating = 0
    course_rating2.append(dealer)
    course_rating2.append(course_finish_marks)
    course_rating2.append(middle_rating)
    return course_rating2, middle_rating


def deps_rating():
    deps = Departament.objects.all()
    deps_vals = {}
    dealers = User.objects.filter(groups__id=1).all()

    for dep in deps:
        dep_marks = []
        for dealer in dealers:
            x, middle_res = dealer_dep_rating(dealer.id, dep.id)
            dep_marks.append(middle_res)
        deps_vals[dep.name] = dep_marks
    for dep in deps:
        deps_vals[dep.name] = round(mean(deps_vals[dep.name]), 2)
    return deps_vals


def overall_rating():
    dep_rating = deps_rating()
    total = 0
    count = 0
    for v in dep_rating.values():
        total += v
        count += 1
    if count:
        overall = round((total/count), 2)
    else:
        overall = 0
    return overall
