from etest.models import PlannedTest, MainResult
from elearning.models import PlannedCourse, Elearning_mark
from trainings.models import PlannedTraining, TrainingResult
from mailing.models import MessageEventNew, SentMessageNew
import datetime
from mailing.mailgun import send_message
from .utils import get_elearning_message, translate_elearning_messages, get_test_message, translate_test_messages
from .utils import get_training_message, translate_training_messages
from users.models import User


def make_marks():
    """
    function gets all parts of course in the past
    """
    tests = PlannedTest.objects.past()
    ecourses = PlannedCourse.objects.past()
    trainings = PlannedTraining.objects.past()

    for test in tests:
        users = test.allowed_users.all()
        for user in users:
            if not MainResult.objects.filter(user=user, ptest=test).first():
                result = MainResult(mark=0, user=user, ptest=test)
                result.save()
    for course in ecourses:
        users = course.allowed_users.all()
        for user in users:
            if not Elearning_mark.objects.filter(user=user, pcourse=course).first():
                result = Elearning_mark(mark=0, user=user, pcourse=course)
                result.save()
    for tr in trainings:
        users = tr.get_users()
        for user in users:
            if not TrainingResult.objects.filter(user=user, planned_training=tr).first():
                result = TrainingResult(result=0, user=user, planned_training=tr, training=tr.training)
                result.save()


def elearning_events_check():
    """
    Check elearning events and send them if all conditions = True
    """
    date = datetime.datetime.now()
    events = MessageEventNew.objects.filter(elearning__isnull=False,
                                            date__lt=date).all()
    for event in events:
        users = event.elearning.allowed_users.all()
        for user in users:
            if not SentMessageNew.objects.filter(event=event,
                                                 user=user,
                                                 event_type=event.event_type).first():
                if event.event_type in ["reminder1", "reminder2"] and Elearning_mark.objects.filter(user=user, pcourse=event.elearning).first():
                    event.delete()
                else:
                    new_sent = SentMessageNew(event=event, user=user, event_type=event.event_type)
                    new_sent.save()
                    # get information to sent
                    message, subject = get_elearning_message(event)
                    correct_message, correct_subject = translate_elearning_messages(message, subject, user, event.elearning)
                    # FIXME: TODO change email to user.email
                    send_message(correct_message, correct_subject, user.email)


def test_events_check():
    """
    Check test events and send them if all conditions = True
    """
    date = datetime.datetime.now()
    events = MessageEventNew.objects.filter(test__isnull=False,
                                         date__lt=date).all()
    for event in events:
        users = event.test.allowed_users.all()
        for user in users:
            if not SentMessageNew.objects.filter(event=event,
                                              user=user,
                                              event_type=event.event_type).first():
                if event.event_type in ["reminder1", "reminder2"] and MainResult.objects.filter(user=user, ptest=event.test).first():
                    event.delete()
                else:
                    new_sent = SentMessageNew(event=event, user=user, event_type=event.event_type)
                    new_sent.save()
                    # get information to sent
                    message, subject = get_test_message(event)
                    correct_message, correct_subject = translate_test_messages(message, subject, user, event.test)
                    # FIXME: TODO change email to user.email
                    send_message(correct_message, correct_subject, user.email)


def training_events_check():
    """
    Check training events and send them if all conditions = True
    """
    date = datetime.datetime.now()
    events = MessageEventNew.objects.filter(training__isnull=False,
                                            date__lt=date).all()
    print(events)
    for event in events:
        users = event.training_date.users.all()
        for user in users:
            if not SentMessageNew.objects.filter(event=event,
                                                 user=user,
                                                 event_type=event.event_type).first():
                if event.event_type in ["reminder1", "reminder2"] and TrainingResult.objects.filter(user=user, planned_training=event.training.date_choice):
                    event.delete()
                else:
                    new_sent = SentMessageNew(event=event, user=user, event_type=event.event_type)
                    new_sent.save()
                    # get information to sent
                    message, subject = get_training_message(event)
                    correct_message, correct_subject = translate_training_messages(message,
                                                                               subject,
                                                                               user,
                                                                               event.training_date.date_choice)
                    # FIXME: TODO change email to user.email
                    send_message(correct_message, correct_subject, user.email)
