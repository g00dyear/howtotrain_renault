from mailing.message_templates import *
import datetime


def get_elearning_message(event):
    if event.event_type == 'invite':
        return elearning_invitation_text, elearning_invitation_subject

    if event.event_type == 'start':
        return elearning_start_text, elearning_start_subject

    return elearning_remember_text, elearning_remember_subject


def translate_elearning_messages(message, subject, user, elearning, result=None):
    message = message.replace('%ELEARNINGNAME%', elearning.course.name)

    if user.job:
        message = message.replace("%AUDITORY%", user.job.name)
    else:
        message = message.replace("%AUDITORY%", '-')

    message = message.replace("%datestart%", elearning.date_start.strftime("%d-%m-%Y %H:%M"))
    message = message.replace("%dateend%", elearning.date_end.strftime("%d-%m-%Y %H:%M"))
    message = message.replace("%USERNAME%", (user.first_name + ' ' + user.last_name))

    subject = subject.replace("%ELEARNINGNAME%",elearning.course.name)

    if user.job:
        subject = subject.replace("%AUDITORY%", user.job.name)
    else:
        subject = subject.replace("%AUDITORY%", '-')

    subject = subject.replace("%datestart%", elearning.date_start.strftime("%d-%m-%Y %H:%M"))
    subject = subject.replace("%dateend%", elearning.date_end.strftime("%d-%m-%Y %H:%M"))
    subject = subject.replace("%USERNAME%", (user.first_name + ' ' + user.last_name))

    if result:
        message = message.replace("%result%", str(result))
        subject = subject.replace("%result%", str(result))

    return message, subject


def get_test_message(event):
    if event.event_type == 'invite':
        return test_invitation_text, test_invitation_subject

    if event.event_type == 'start':
        return test_start_text, test_start_subject

    return test_remember_text, test_remember_subject


def translate_test_messages(message, subject, user, test, result=None):
    message = message.replace('%TESTNAME%', test.test.name)

    if user.job.name:
        message = message.replace("%AUDITORY%", user.job.name)
    else:
        message = message.replace("%AUDITORY%", '-')

    message = message.replace("%datestart%", test.date_start.strftime("%d-%m-%Y %H:%M"))
    message = message.replace("%dateend%", test.date_end.strftime("%d-%m-%Y %H:%M"))
    message = message.replace("%question_count%", str(test.question_count))
    message = message.replace("%max_time_for_question%", str(test.max_question_time))
    message = message.replace("%USERNAME%", (user.first_name + ' ' + user.last_name))

    subject = subject.replace('%TESTNAME%', test.test.name)

    if user.job.name:
        subject = subject.replace("%AUDITORY%", user.job.name)
    else:
        subject = subject.replace("%AUDITORY%", '-')

    subject = subject.replace("%datestart%", test.date_start.strftime("%d-%m-%Y %H:%M"))
    subject = subject.replace("%dateend%", test.date_end.strftime("%d-%m-%Y %H:%M"))
    subject = subject.replace("%question_count%", str(test.question_count))
    subject = subject.replace("%max_time_for_question%", str(test.max_question_time))
    subject = subject.replace("%USERNAME%", (user.first_name + ' ' + user.last_name))

    if result:
        message = message.replace("%result%", str(result))
        subject = subject.replace("%result%", str(result))

    return message, subject


def get_training_message(event):
    if event.event_type == 'invite':
        return training_invitation_text, training_invitation_subject

    if event.event_type == 'start':
        return training_start_text, training_start_subject

    return training_remember_text, training_remember_subject


def translate_training_messages(message, subject, user, training, result=None):
    message = message.replace('%TRAININGNAME%', training.training.name)
    message = message.replace("%AUDITORY%", training.auditory_list)
    message = message.replace("%datestart%", training.dates.order_by('date').first().date.strftime("%d-%m-%Y %H:%M"))
    message = message.replace("%event_duration%", '1 день')
    message = message.replace("%training_place%", training.place)
    message = message.replace("%USERNAME%", (user.first_name + ' ' + user.last_name))

    subject = subject.replace('%TRAININGNAME%', training.training.name)
    subject = subject.replace("%AUDITORY%", training.auditory_list)
    subject = subject.replace("%datestart%", training.dates.order_by('date').first().date.strftime("%d-%m-%Y %H:%M"))
    subject = subject.replace("%event_duration%", '1 день')
    subject = subject.replace("%training_place%", training.place)
    subject = subject.replace("%USERNAME%", (user.first_name + ' ' + user.last_name))

    if result:
        message = message.replace("%result%", str(result))
        subject = subject.replace("%result%", str(result))

    return message, subject
