from elearning.models import PlannedCourse
from newcourse.models import *


def correct_mark(mark, condition):
    if len(condition) > 0:
        if 'or' in condition:
            condition = condition.split('or')
            value = str(mark) + condition[0] + 'or ' + str(mark) + condition[1]
        elif 'and' in condition:
            condition = condition.split('and')
            value = str(mark) + condition[0] + 'and ' + str(mark) + condition[1]
        else:
            value = str(mark) + condition
        return value
    else:
        return None


def level1(ptest, user, mark):
    try:
        level = ptest.test_part.course_part
        course = level.study_course
        finish_mark = correct_mark(mark, level.finish_mark)
        # Изменяем временную оценку
        result = CourseResult.objects.filter(user=user, course=course).last()
        result.mark = mark
        result.temporary = True
        result.save()
        # Проверяем условие окончания курса
        if finish_mark and eval(finish_mark):
            result.temporary = False
            result.save()
        else:
            try:
                ecourse = None
                level2 = course.level2.first()
                if level2.eparts.first():
                    c_courses = PlannedCourse.objects.current()
                    for part in level2.eparts.all():
                        if part.ecourse in c_courses:
                            ecourse = part.ecourse
                if not ecourse:
                    ecourse = level2.eparts.filter(ecourse__date_start__gt=ptest.date_end).order_by('ecourse__date_start').first().ecourse
                ecourse.allowed_users.add(user)
                ecourse.save()
            except:
                try:
                    training = course.level3.first().training_parts.filter(training__dates__date__gt=ptest.date_end).order_by('training__dates__date').first().training
                    training.allowed_users.add(user)
                except:
                    if level.test_parts.filter(test__date_start__gt=ptest.date_end).order_by('test__date_start').first():
                        test = level.test_parts.filter(test__date_start__gt=ptest.date_end).order_by('test__date_start').first()
                        test.save()
                        test.allowed_users.add(user)
                    else:
                        result.temporary = False
                        result.save()
    except:
        pass


def level2(ecourse, mark, user):
    try:
        print('im in func')
        level = ecourse.ecourse_part.course_part
        print('level')
        course = level.study_course
        print('course')
        finish_mark = correct_mark(mark, level.finish_mark)
        print('finish_mark')
        next_mark = correct_mark(mark, level.next_step_mark)
        print('next_mark')
        prev_mark = correct_mark(mark, level.previous_step_mark)
        print('prev_mark')
        print(course)
        print(user)
        result = CourseResult.objects.filter(user=user, course=course).last()
        print('result obj')
        result.mark = mark
        print('result mark = ecourse mark')
        result.temporary = True
        print('result temporary = True')
        result.save()
        print('result was saved')
        if finish_mark and eval(finish_mark):
            print('im not in finish')
            result.temporary = False
            result.save()
        elif prev_mark and eval(prev_mark):
            print('im not in prev')
            # Если ДО больше одного
            try:
                # Если существует курс с датой начала позже, чем дата окончания текущего
                ecourse2 = level.eparts.filter(ecourse__date_start__gt=ecourse.date_end).order_by('ecourse__date_start').first().ecourse
                ecourse2.save()
                ecourse2.allowed_users.add(user)
            except:
                # Если такого курса не существует, то шага назад нету. Финиш курса.
                result.temporary = False
                result.save()
        elif next_mark and eval(next_mark):
            print('im in next condition')
            # Если существует тренинг после ДО, то записываемся в него
            try:
                print('im in try in next condition')
                training = course.level3.first().training_parts.filter(training__dates__date__gt=ecourse.date_end).order_by('training__dates__date').first().training
                print(training)
                training.save()
                training.allowed_users.add(user)
                print('user was added in training')
                training.save()
            # Если тренинга не существует, то пробуем найти тестирование, которое может быть окончанием курса.
            except:
                print('exception was called')
                try:
                    test = course.level1.test_parts.filter(test__date_start__gt=ecourse.date_end).order_by('test__date_start').first().test
                    test.save()
                    test.allowed_users.add(user)
                except:
                    # Если тестирования нету, то шага вперед нету, следовательно конец курса
                    result.temporary = False
                    result.save()
    except:
        pass


def level3(training, mark, user):
    try:
        level = training.training_part.course_part
        course = level.study_course
        finish_mark = correct_mark(mark, level.finish_mark)
        next_mark = correct_mark(mark, level.next_step_mark)
        prev_mark = correct_mark(mark, level.previous_step_mark)
        date = training.dates.order_by('date').last().date
        result = CourseResult.objects.filter(user=user, course=course).last()
        result.mark = mark
        result.temporary = True
        result.save()
        # Условие завершения курса
        if finish_mark and eval(finish_mark):
            print('finish_mark condition')
            result.temporary = False
            result.save()
            event = Event()
        # Условие перехода на предыдущий уровень
        elif prev_mark and eval(prev_mark):
            # пробуем найти ДО подходящее по дате
            try:
                ecourse = course.level2.first().eparts.filter(ecourse__date_start__gt=date).order_by('ecourse__date_start').first().ecourse
                ecourse.save()
                ecourse.allowed_users.add(user)
            except:
                # Если ДО нету, то пробуем найти повторное очное обучение
                try:
                    tr = level.training_parts.filter(training__dates__date__gt=date).order_by('training__dates__date').first().training
                    tr.save()
                    tr.allowed_users.add(user)
                # Если очного обучения нету, то завершаем ОО текущей оценкой
                except:
                    result.temporary = False
                    result.save()
        # Условие перехода на следующий уровень
        elif next_mark and eval(next_mark):
            try:
                # Проверяем нет ли завершающего тестирования
                test = course.level1.test_parts.filter(test__date_start__gt=date).order_by('test__date_start').first().test
                test.save()
                test.allowed_users.add(user)
            except:
                # Если тестирования нету, то завершаем курс текущей оценкой
                result.temporary = False
                result.save()
    except:
        pass
