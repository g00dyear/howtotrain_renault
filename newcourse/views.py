from django.shortcuts import render, HttpResponse
from .models import StudyCourse
from etest.models import PlannedTest, MainResult
from elearning.models import PlannedCourse, Elearning_mark
from trainings.models import PlannedTraining, TrainingResult

# Create your views here.


def course_test_export(request, test_id, course_id):
    import openpyxl
    from openpyxl.cell import get_column_letter
    course = StudyCourse.objects.get(id=course_id)
    test = PlannedTest.objects.get(id=test_id)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    if test.test_name:
        filename = test.test_name
    else:
        filename = test.test.name
    response['Content-Disposition'] = 'attachment; filename=' + filename + '.xlsx'
    wb = openpyxl.Workbook()
    ws = wb.get_active_sheet()
    ws.title = course.name

    columns = [("Название курса", 250), ("Название тестирования", 350)]

    row_num = 0
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]
    if test.test_name:
        test_name = test.test_name
    else:
        test_name = test.test.name
    row_num = 1
    row = [course.name, test_name]
    for col_num in range(len(row)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        if row[col_num] == 'None':
            c.value = ''
        else:
            c.value = row[col_num]

    columns = [("Сотрудник", 50), ("Должность", 50), ("Дилер", 50)]
    row_num = 3
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]

    for user in test.allowed_users.all():
        if not MainResult.objects.filter(user=user, ptest=test).last():
            row_num += 1
            if user.job:
                job_name = user.job.name
            else:
                job_name = '-'
            if user.parentId:
                row = [user.get_full_name(), job_name, user.parentId.short_title]
            else:
                row = [user.get_full_name(), job_name, '-']
            for col_num in range(len(row)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                if row[col_num] == 'None':
                    c.value = ''
                else:
                    c.value = row[col_num]
                c.style.alignment.wrap_text = True
    wb.save(response)
    return response


def course_ecourse_export(request, ecourse_id, course_id):
    import openpyxl
    from openpyxl.cell import get_column_letter
    course = StudyCourse.objects.get(id=course_id)
    ecourse = PlannedCourse.objects.get(id=ecourse_id)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    if ecourse.ecourse_name:
        filename = ecourse.ecourse_name
    else:
        filename = ecourse.ecourse.name
    response['Content-Disposition'] = 'attachment; filename=' + filename + '.xlsx'
    wb = openpyxl.Workbook()
    ws = wb.get_active_sheet()
    ws.title = course.name

    columns = [("Название курса", 250), ("Название дистанционного обучения", 350)]

    row_num = 0
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]
    if ecourse.course_name:
        ecourse_name = ecourse.course_name
    else:
        ecourse_name = ecourse.course.name
    row_num = 1
    row = [course.name, ecourse_name]
    for col_num in range(len(row)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        if row[col_num] == 'None':
            c.value = ''
        else:
            c.value = row[col_num]

    columns = [("Сотрудник", 50), ("Должность", 50), ("Дилер", 50)]
    row_num = 3
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]

    for user in ecourse.allowed_users.all():
        if not Elearning_mark.objects.filter(user=user, pcourse=ecourse).last():
            row_num += 1
            if user.job:
                job_name = user.job.name
            else:
                job_name = '-'
            if user.parentId:
                row = [user.get_full_name(), job_name, user.parentId.short_title]
            else:
                row = [user.get_full_name(), job_name, '-']
            for col_num in range(len(row)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                if row[col_num] == 'None':
                    c.value = ''
                else:
                    c.value = row[col_num]

                c.style.alignment.wrap_text = True
    wb.save(response)
    return response


def course_training_export(request, training_id, course_id):
    import openpyxl
    from openpyxl.cell import get_column_letter
    course = StudyCourse.objects.get(id=course_id)
    training = PlannedTraining.objects.get(id=training_id)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    if training.training_name:
        filename = training.training_name
    else:
        filename = training.training.name
    response['Content-Disposition'] = 'attachment; filename=' + filename + '.xlsx'
    wb = openpyxl.Workbook()
    ws = wb.get_active_sheet()
    ws.title = course.name

    columns = [("Название курса", 250), ("Название очного обучения", 350)]

    row_num = 0
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]
    if training.training_name:
        training_name = training.training_name
    else:
        training_name = training.course.name
    row_num = 1
    row = [course.name, training_name]
    for col_num in range(len(row)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        if row[col_num] == 'None':
            c.value = ''
        else:
            c.value = row[col_num]

    columns = [("Сотрудник", 50), ("Должность", 50), ("Дилер", 50)]
    row_num = 3
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]

    for user in training.allowed_users.all():
        if not TrainingResult.objects.filter(user=user, planned_training=training).first():
            row_num += 1
            if user.job:
                job_name = user.job.name
            else:
                job_name = '-'
            if user.parentId:
                row = [user.get_full_name(), job_name, user.parentId.short_title]
            else:
                row = [user.get_full_name(), job_name, '-']
            for col_num in range(len(row)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                if row[col_num] == 'None':
                    c.value = ''
                else:
                    c.value = row[col_num]
                c.style.alignment.wrap_text = True
    wb.save(response)
    return response