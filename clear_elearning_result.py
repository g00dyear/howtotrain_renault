from etest.models import PlannedTest, MainResult, Result, TestResult, QuestionTime
from elearning.models import PlannedCourse, Elearning_mark
from users.models import User


def clear_elearning_results(user_id, test_id, elearning_id=None):
    user = User.objects.get(id=user_id)
    test = PlannedTest.objects.get(id=test_id)
    course = PlannedCourse.objects.get(id=elearning_id)
    try:
        # delete test result
        test_result = MainResult.objects.filter(ptest=test, user=user).last()
        test_result.delete()

        # delete modules results
        modules_results = Result.objects.filter(ptest=test, user=user).all()
        for m in modules_results:
            m.delete()
        # delete questions results
        questions_results = TestResult.objects.filter(ptest=test, user=user).all()
        for q in questions_results:
            q.delete()

        # delete times
        question_times = QuestionTime.objects.filter(ptest=test, user=user).all()
        for q in question_times:
            q.delete()
    except:
        pass

    if elearning_id:
        # delete elearning mark
        elearning_result = Elearning_mark.objects.filter(pcourse=course, user=user).last()
        elearning_result.delete()

    return 'done'
