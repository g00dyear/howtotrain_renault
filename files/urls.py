from django.conf.urls import patterns, include, url
from django.contrib import admin
from files import views

urlpatterns = patterns('',
        url(r'^$', views.training_import),
        url(r'^emarks-to-course/$', views.marks_to_rating),
        url(r'^marks-to-course/$', views.marks_to_rating2),
                       )
