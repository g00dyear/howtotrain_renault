from django.db import models

# Create your models here.
class File(models.Model):
    name = models.CharField(max_length=256, verbose_name='Название файла')
    file = models.FileField(upload_to='project_files/', verbose_name='Файл')
    label = models.CharField(max_length=128, verbose_name='Метка')
    
    def __str__(self):
        return self.name + ' | ' + self.label + ' | Cсылка на файл:' + str(self.file.url)

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'