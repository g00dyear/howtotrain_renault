from django.contrib import admin
from .models import File
# Register your models here.


class FileAdmin(admin.ModelAdmin):
    list_filter = ['label', 'name']


admin.site.register(File, FileAdmin)