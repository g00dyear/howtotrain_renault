from django.shortcuts import render
from trainings.models import Training, TrainingResult, PlannedTraining, TrainingDate
from io import BytesIO
from datetime import datetime
# Create your views here.


def handle_uploaded_file(f):
    with open('some/file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def training_import(request):
    from users.models import User
    import openpyxl
    trainings = Training.objects.all()
    message = 'Выберите нужные параметры и нажмите "Синхронизировать результаты"'
    today = datetime.now()
    failed_users = []
    if request.method == 'POST':
        file = request.FILES['file']
        tr = request.POST['training']
        training = Training.objects.get(id=int(tr))
        date = request.POST['date']
        training_name = request.POST['name']
        filename = file.name
        exist_users = []
        if filename.endswith('.xlsx'):
            wb = openpyxl.load_workbook(filename=BytesIO(file.read())) #'media/' + tr_file.file.name
            ws = wb.active
            tr_row = []
            for row in ws.rows:
                tr_user = []
                for cell in row:
                    if cell.value or cell.value == 0:
                        tr_user.append(cell.value)
                tr_row.append(tr_user)
            for i in tr_row:
                if i:
                    dealer = User.objects.filter(short_title=i[2]).first()
                    last_name, first_name = i[1].split(' ')
                    if User.objects.filter(first_name=first_name, last_name=last_name, parentId=dealer).first():
                        exist_users.append([User.objects.filter(first_name=first_name,
                                                               last_name=last_name,
                                                               parentId=dealer).first(), i[3]])
                    else:
                        failed_users.append(i[1])
            if exist_users:
                new_training = PlannedTraining(training=training, training_name=training_name)
                new_training.save()
                for user in exist_users:
                    new_training.allowed_users.add(user[0])
                    result = user[1]
                    if result <= 1:
                        result *= 100
                    result = TrainingResult(user=user[0], training=training, result=result, planned_training=new_training)
                    result.save()
                training_date = TrainingDate(date=datetime.strptime(date, "%d-%m-%Y"), date_choice=new_training)
                training_date.save()
            else:
                message = 'Список пользователей пуст'

        else:
            message = 'Формат файла не "xlsx". \
            Выберите файл с расширением .xlsx и попробуйте еще раз'
    return render(request, 'files/training_import.html', {'message': message,
                                                          'trainings': trainings,
                                                          'today': today,
                                                          'failed_users': failed_users})


def marks_to_rating(request):
    from rating_script import generate_marks
    string = None
    if request.method == 'POST':
        course_id = int(request.POST['course_id'])
        ecourse_id = int(request.POST['ecourse_id'])
        string = generate_marks(ecourse_id, course_id)
    return render(request, 'files/marks_to_rating.html', {'string': string})


def marks_to_rating2(request):
    from rating_script import generate_marks_with_attestation
    newcomers = None
    if request.method == 'POST':
        course_id = int(request.POST['course_id'])
        test_id = int(request.POST['test_id'])
        ecourse_id = int(request.POST['ecourse_id'])
        newcomers = generate_marks_with_attestation(test_id, ecourse_id, course_id)
    return render(request, 'files/marks_to_rating2.html', {'newcomers': newcomers})