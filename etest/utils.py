from mailing.models import MessageEventNew
from datetime import datetime, timedelta
from mailing.message_templates import *
from mailing.mailgun import send_message

def create_test_events(test):
    """
    :param test;
    :return: Nothing;
    Creating all types of needed events: invitation, start, reminder
    """
    if not MessageEventNew.objects.filter(test=test).first():
        # Create start event
        start_event = MessageEventNew(date=test.date_start,
                                   test=test,
                                   event_type='start')
        start_event.save()

        # create invitation event
        inv_event = MessageEventNew(date=datetime.now(),
                                 test=test,
                                 event_type='invite')
        inv_event.save()

        # create reminder events
        # reminder before 1 day to end
        reminder = MessageEventNew(date=(test.date_end - timedelta(days=1)),
                                test=test,
                                event_type='reminder1')
        reminder.save()

        # halftime reminder
        halftime = ((test.date_end - test.date_start)/2).days
        reminder2 = MessageEventNew(date=(test.date_end - timedelta(days=halftime)),
                                 test=test,
                                 event_type='reminder2')
        reminder2.save()

    else:
        events = MessageEventNew.objects.filter(test=test,
                                             sent=False).all()
        for event in events:
            if event.event_type == 'start':
                if event.date != test.date_start:
                    event.date = test.date_start

            if event.event_type == 'invite':
                if event.date.day != datetime.now().day:
                    event.date = datetime.now()

            if event.event_type == 'reminder1':
                if event.date != (test.date_end - timedelta(days=1)):
                    event.date = (test.date_end - timedelta(days=1))

            if event.event_type == 'reminder2':
                halftime = ((test.date_end - test.date_start)/2).days
                if event.date != (test.date_end - timedelta(days=halftime)):
                    event.date = (test.date_end - timedelta(days=halftime))

            event.save()


def make_mail(result):
    from newcourse.utils import translate_test_messages

    if result.mark >= 80:
        message, subject = translate_test_messages(test_success_text,
                                                   test_success_subject,
                                                   result.user,
                                                   result.ptest,
                                                   result.mark)
    else:
        message, subject = translate_test_messages(test_unsuccess_text,
                                                   test_unsuccess_subject,
                                                   result.user,
                                                   result.ptest,
                                                   result.mark)
    send_message(message, subject, result.user.email)
