from django import template

register = template.Library()


@register.filter
def keyvalue(dict, key):    
    return dict[key]
    
@register.filter
def result(dict, key):    
    try:
        return dict[key]
    except:
        return ''