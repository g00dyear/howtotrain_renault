from .models import *
from random import shuffle

def get_q_list(module_id):
    questions = []
    module = Module.objects.get(id=module_id)
    mqs=[]
    for q in module.questions.all():
        mqs.append(q.id)
    shuffle(mqs)
    max_questions = module.max_questions
    if max_questions:
        x=0
        while x<max_questions:
            try:
                questions.append(mqs.pop())
                x+=1
            except:
                x+=1000000
    else:
        try:
            while True:
                questions.append(mqs.pop())
        except:
            pass
    uniq_questions = []
    for q in questions:
        if not q in uniq_questions:
            uniq_questions.append(q)
    s = ','
    s.join(uniq_questions)
    return s


