from django.shortcuts import render, redirect
from etest.models import PlannedTest, Module, TestResult, Question, Answer, Result, QuestionTime, MainResult, Test
from users.models import User
from elearning.models import PlannedCourse, Attempt, EModule, Elearning_mark
from .tools import *
from django.http import HttpResponse
from trainings.models import PlannedTraining, TrainingResult, UserlistByDate
from django.contrib.auth.decorators import login_required, permission_required
import datetime
from django.contrib.auth.models import Group


# Главная страница тестирования для пользователя
@login_required
def test_list(request):
    user = request.user
    now = datetime.datetime.now()
    tests = PlannedTest.objects.filter(date_start__lt=now, date_end__gt=now, allowed_users=user).all()
    a_tests = []
    for test in tests:
        if not MainResult.objects.filter(user=user, ptest=test).first():
            a_tests.append(test)
    return render(request, 'etests/user_mainpage.html', {'user': user, 'tests': a_tests})
    

# Правила прохождения тестирования
@login_required
def test_view(request, plannedtest_id):
    p_test = PlannedTest.objects.get(id = plannedtest_id)
    user = request.user
    return render(request, 'etests/test_rules.html', {'user': user, 'p_test': p_test})
    
    
# Список модулей тестирования для пользователя + вывод оценок по пройденным модулям
def modules(request, plannedtest_id):
    ptest = PlannedTest.objects.get(id = plannedtest_id)
    
    user = request.user
    modules = ptest.test.modules.all().order_by('sequence_number')
    marks = Result.objects.filter(ptest=ptest, user=user)
    mrks = {}
    
    if ptest.for_elearning:
        if PlannedCourse.objects.filter(start_test=ptest, allowed_users=user).last():
            pcourse = PlannedCourse.objects.filter(start_test=ptest, allowed_users=user).last()
        else:
            pcourse = PlannedCourse.objects.filter(finish_test=ptest, allowed_users=user).last()
    else:
        pcourse = None
    
    for mark in marks:
        mrks[mark.module.id] = mark.mark
    if ptest.time_limit:
        try:
            diff = time() - QuestionTime.objects.filter(ptest=ptest, user=user).first().start_time
            if diff.total_seconds() > ptest.time_limit*60:
                for module in modules:
                    if not Result.objects.filter(user=user, module=module, ptest=ptest).last():
                        m = Result(mark=-1, user=user, module=module, ptest=ptest)
                        m.save()
        except:
            pass
    """
    if not p_test.type_of_mark or p_test.type_of_mark.id == 1:
        if marks.count() == p_test.test.modules.count() and not MainResult.objects.filter(ptest=p_test, user=user).first():
            total = 0
            for mark in marks:
                total += mark.mark
            mainmark = MainResult(ptest=p_test, user=user, mark=round((total / marks.count()), 2), testend=time())
            mainmark.save()
            # обработка варианта в случае если тестирование предназначено как стартовое или финишное тестирование елернинга
            if p_test.for_elearning:
                try:
                    return redirect('/elearning/course/%s' %(p_test.pcourse_start.last().id))
                except:
                    return redirect('/elearning/course/%s' %(p_test.pcourse_finish.last().id))
            else:
                return render(request, 'etests/modules.html', {'user': user, 
                                                        'p_test': p_test, 
                                                        'modules': modules, 
                                                        'marks': mrks, 
                                                        'mainmark': mainmark.mark})
        elif marks.count() == p_test.test.modules.count() and MainResult.objects.filter(ptest=p_test, user=user).first():
            mainmark = MainResult.objects.filter(ptest=p_test, user=user).first()
            return render(request, 'etests/modules.html', {'user': user, 
                                                        'p_test': p_test, 
                                                        'modules': modules, 
                                                        'marks': mrks, 
                                                        'mainmark': mainmark.mark})
    elif p_test.type_of_mark.id == 2:
        question_marks = 0
        question_count = 0
        if marks.count() == p_test.test.modules.count() and not MainResult.objects.filter(ptest=p_test, user=user).first():
            for module in modules:
                for question in module.questions.all():
                    question_marks += get_question_mark(p_test, module, question, user)
                question_count += module.questions.count()
            mainmark = MainResult(ptest=p_test, user=user, mark=round((question_marks / question_count), 2), testend=time())
            mainmark.save()
            # обработка варианта в случае если тестирование предназначено как стартовое или финишное тестирование елернинга
            if p_test.for_elearning:
                try:
                    return redirect('/elearning/course/%s' %(p_test.pcourse_start.last().id))
                except:
                    return redirect('/elearning/course/%s' %(p_test.pcourse_finish.last().id))
            else:
                return render(request, 'etests/modules.html', {'user': user, 
                                                        'p_test': p_test, 
                                                        'modules': modules, 
                                                        'marks': mrks, 
                                                        'mainmark': mainmark.mark})
        elif marks.count() == p_test.test.modules.count() and MainResult.objects.filter(ptest=p_test, user=user).first():
            mainmark = MainResult.objects.filter(ptest=p_test, user=user).first()
       """
    
    try:
        mainmark = MainResult.objects.filter(user=user, ptest=ptest).last().mark
    except:
        mainmark = None
    
    return render(request, 'etests/modules.html', {'user': user, 
                                                    'p_test': ptest, 
                                                    'modules': modules, 
                                                    'marks': mrks, 
                                                    'mainmark': mainmark,
                                                    'pcourse': pcourse})
    
def modules_q(request, plannedtest_id):
    ptest = PlannedTest.objects.get(id=plannedtest_id)
    user = request.user
    modules = ptest.test.modules.all()
    questions = []
    
    #Прием ответов пользователя
    if request.method == 'POST':
        post = request.POST.copy() 
        question_id = int(post['question.id'])
        module_id = int(post['module'])
        m = Module.objects.get(id=module_id)
        question = Question.objects.get(id=question_id)
        answers = question.answers.all()
        qtime = QuestionTime.objects.filter(user=user, ptest=ptest, question=question, module=m).first()
        qtime.end_time = time()
        qtime.save()
        timediff = qtime.end_time - qtime.start_time
        diff = timediff.total_seconds()

        if not TestResult.objects.filter(user=user, question=question, ptest=ptest).first():
            if diff <= question.time:
                for answer in answers:
                    if str(answer.id) in request.POST.getlist('ans[]'):
                        r = TestResult(ptest = ptest, module = m, checked = True, question = question, user = user, answer = answer)
                    else:
                        r = TestResult(ptest = ptest, module = m, checked = False, question = question, user = user, answer = answer)
                    r.save()
                        
    # максимальное количество вопросов + текущий номер
    q_num = 0
    max_questions = 0
    for module in modules:
        if module.max_questions:
            max_questions += module.max_questions
        else:
            max_questions += module.questions.count()
        
        try:
            time_objects = QuestionTime.objects.filter(ptest=ptest, user = user, question=module.questions.all(), module=module).all()
            for t in time_objects:
                if t.end_time:
                    q_num += 1
        except:
            pass
        
    question_num = q_num + 1 
    try:        
        check_time = QuestionTime.objects.filter(user=user, ptest=ptest, module=modules).last()
        if check_time.start_time and not check_time.end_time:
            question = check_time.question
            timer = time()-check_time.start_time
            module = check_time.module
            return render(request, 'etests/question2.html', {'user':user, 'question':question, 'timer':timer, 'max_questions': max_questions, 'question_num': question_num, 'module': module})
    except:
        pass
    # Цикл вопросов по всем модулям тестирования      
    for module in modules:
        if get_next_question(module.id, ptest.id, user):
            question = get_next_question(module.id, ptest.id, user)
            if not QuestionTime.objects.filter(ptest=ptest, user=user, question=question, module=module).first():
                start_time = QuestionTime(start_time = time(), user=user, question=question, ptest=ptest, module=module)
                start_time.save()
                timer = question.time
                return render(request, 'etests/question2.html', {'user':user, 'question':question, 'timer':timer, 'max_questions': max_questions, 'question_num': question_num, 'module': module })
                
            else:
                q = QuestionTime.objects.filter(ptest=ptest, user=user,question=question, module=module).first()
                current = time()
                diff = current - q.start_time
                total_diff = int(diff.total_seconds())
                if total_diff < question.time + 5:
                    timer = question.time - total_diff
                else:
                    answers = question.answers.all()
                    result = {}
                    
                    for answer in answers:
                        r = TestResult(ptest = ptest, module = module, checked = False, question = question, user = user, answer = answer)
                        r.save()
                    return redirect('/tests/question/%s/' % (ptest.id))
                return render(request, 'etests/question2.html', {'user':user, 'question':question, 'timer':timer, 'max_questions': max_questions, 'question_num': question_num }) 
        
        else:
            if not Result.objects.filter(module=module, ptest=ptest, user=user).first():
                mark = get_module_mark(ptest.id, module.id, user)
                m = Result(ptest=ptest, module=module, user=user, mark=mark)
                m.save()
    return redirect('/tests/modules/%s' % ptest.id)
    
    
# Тестирование
@login_required
def testing(request, module_id, ptest_id):
    module = Module.objects.get(id = module_id)
    user = request.user
    ptest = PlannedTest.objects.get(id = ptest_id)

    if module.max_questions > 0:
        max_questions = module.max_questions
    else:
        max_questions = module.questions.count()
        
    questions = module.questions.all()
    
    
    
    #Прием ответов пользователя
    if request.method == 'POST':
        post = request.POST.copy() # из этого надо получить список вариантов ответов с результатом True/False
        question_id = int(post['question.id'])
        question = Question.objects.get(id=question_id)
        answers = question.answers.all()
        result = {}
        qtime = QuestionTime.objects.filter(user = user, ptest=ptest, question=question, module=module).first()
        qtime.end_time = time()
        qtime.save()
        timediff = qtime.end_time - qtime.start_time
        diff = timediff.total_seconds()

        if not TestResult.objects.filter(user=user, question=question, ptest=ptest).first():
            if diff <= question.time:
                for answer in answers:
                    if str(answer.id) in request.POST.getlist('ans[]'):
                        r = TestResult(ptest = ptest, module = module, checked = True, question = question, user = user, answer = answer)
                    else:
                        r = TestResult(ptest = ptest, module = module, checked = False, question = question, user = user, answer = answer)
                    r.save()
    # нумерация вопросов            
    q_num = 0
    try:
        time_objects = QuestionTime.objects.filter(ptest=ptest, user = user, question=questions, module=module).all()
        for t in time_objects:
            if t.end_time:
                q_num += 1
    except:
        pass
    question_num = q_num + 1 
    
    try:        
        check_time = QuestionTime.objects.filter(user=user, ptest=ptest, module=module).last()
        if check_time.start_time and not check_time.end_time:
            question = check_time.question
            timer = time()-check_time.start_time
            return render(request, 'etests/question.html', {'user':user, 'question':question, 'timer':timer, 'max_questions': max_questions, 'question_num': question_num })
    except:
        pass
    
    
       
    #Отправка следующего вопроса пользователю   
    if get_next_question(module_id, ptest_id, user):
        question = get_next_question(module_id, ptest_id, user)
        if not QuestionTime.objects.filter(ptest=ptest, user=user,question=question, module=module).first():
            start_time = QuestionTime(start_time = time(), user=user, question=question, ptest=ptest, module=module)
            start_time.save()
            timer = question.time
            return render(request, 'etests/question.html', {'user':user, 'question':question, 'timer':timer, 'max_questions': max_questions, 'question_num': question_num })
        else:
            q = QuestionTime.objects.filter(ptest=ptest, user=user,question=question, module=module).first()
            current = time()
            diff = current - q.start_time
            total_diff = int(diff.total_seconds())
            if total_diff < question.time + 5:
                timer = question.time - total_diff
            else:
                answers = question.answers.all()
                
                for answer in answers:
                    r = TestResult(ptest = ptest, module = module, checked = False, question = question, user = user, answer = answer)
                    r.save()
                return redirect('/tests/question/%s/%s/' % (ptest_id, module_id)) 
            return render(request, 'etests/question.html', {'user':user, 'question':question, 'timer':timer, 'max_questions': max_questions, 'question_num': question_num }) 
    #Окончание модуля
    else:
        if not Result.objects.filter(module=module, ptest=ptest, user=user).first():
            mark = get_module_mark(ptest_id, module_id, user)
            m = Result(ptest=ptest, module=module, user=user, mark=mark)
            m.save()
        return redirect('/tests/modules/%s/' % ptest_id) #на список модулей. Это конец модуля.


# Достижения пользователя
@login_required
def achievements(request):
    user = request.user
    ptests = PlannedTest.objects.filter(allowed_users=user).all()
    pcourses = PlannedCourse.objects.filter(allowed_users=user).all()
    ptrainings = PlannedTraining.objects.filter(allowed_users=user, finished=True)
    for_elearning = []
    for_training = []
    for_tests = []
    training_results = []
    elearning_results = []
    marks = []
    dates = []
    mark_for_elearning = []
    # Вывод списка тестирований
    for ptest in ptests:
        if MainResult.objects.filter(ptest=ptest, user=user):
            result = MainResult.objects.filter(user=user, ptest=ptest).last()
            if ptest.for_elearning == True:
                for_elearning.append(result)
            if ptest.for_training == True:
                for_training.append(result)
            if ptest.for_elearning == False and ptest.for_training == False:
                for_tests.append(result)
    ## Вывод списка тренингов
    trainings = UserlistByDate.objects.filter(user=user, date__date__lt=time, training=ptrainings)
    for training in trainings:
        if training.training.test_after and MainResult.objects.filter(ptest=training.training.test_after, user=user).last():
            training_results.append(training)
            dates.append(training.date.date)
            marks.append(MainResult.objects.filter(ptest=training.training.test_after, user=user).last().mark)
        elif not training.training.test_after:
            training_results.append(training)
            dates.append(training.date.date)
            marks.append('Без оценки')
    trainings_info = zip(training_results, dates, marks)
    # Вывод списка дистанционок
    for course in pcourses:
        if Elearning_mark.objects.filter(pcourse=course, user=user).last():
            r = Elearning_mark.objects.filter(pcourse=course, user=user).last()
            result = r.mark
            if int(result) >= course.min_mark_for_test:
                elearning_results.append(r)
                mark_for_elearning.append(int(r.mark))
            else:
                elearning_results.append(r)
                mark_for_elearning.append(0)
    elearning_result = zip(elearning_results, mark_for_elearning)
    return render(request, 'etests/achievements.html', {'user':user, 'for_elearning': for_elearning,
                                                    'for_training': for_training, 'for_tests': for_tests,
                                                    'trainings_info': trainings_info, 'elearning_result': elearning_result, 'dates': dates})
    

# Просмотр достижений пользователя для дистрибьютора и дилера
@login_required
def achievements_for_stuff(request, user_id):
    user = User.objects.get(id=user_id)
    current_user = request.user
    ptests = PlannedTest.objects.filter(allowed_users=user).all()
    pcourses = PlannedCourse.objects.filter(allowed_users=user).all()
    ptrainings = PlannedTraining.objects.filter(allowed_users=user, finished=True)
    for_elearning = []
    for_training = []
    for_tests = []
    training_results = []
    elearning_results = []
    marks = []
    dates = []
    # Вывод списка тестирований
    for ptest in ptests:
        if MainResult.objects.filter(ptest=ptest, user=user):
            result = MainResult.objects.filter(user=user, ptest=ptest).last()
            if ptest.for_elearning == True:
                for_elearning.append(result)
            if ptest.for_training == True:
                for_training.append(result)
            if ptest.for_elearning == False and ptest.for_training == False:
                for_tests.append(result)
    ## Вывод списка тренингов
    trainings = UserlistByDate.objects.filter(user=user, date__date__lt=time, training=ptrainings)
    for training in trainings:
        if training.training.test_after and MainResult.objects.filter(ptest=training.training.test_after, user=user).last():
            training_results.append(training)
            dates.append(training.date.date)
            marks.append(MainResult.objects.filter(ptest=training.training.test_after, user=user).last().mark)
        elif not training.training.test_after:
            training_results.append(training)
            dates.append(training.date.date)
            marks.append('Без оценки')
    trainings_info = zip(training_results, dates, marks)
    # Вывод списка дистанционок
    for course in pcourses:
        if Elearning_mark.objects.filter(pcourse=course, user=user):
            r = Elearning_mark.objects.filter(pcourse=course, user=user).last()
            result = r.mark
            if int(result) >= course.min_mark_for_test:
                elearning_results.append(r)
                
    return render(request, 'etests/achievements.html', {'user':user, 'for_elearning': for_elearning,
                                                    'for_training': for_training, 'for_tests': for_tests,
                                                    'trainings_info': trainings_info, 'elearning_results': elearning_results, 'dates': dates,
                                                    'current_user': current_user})
    


# Список тестирований для дилера
@login_required
def dealer_testlist(request):
    user = request.user
    childs = user.childs.all()
    ptests_list = PlannedTest.objects.filter(allowed_users = childs)
    ptests = list(set(ptests_list))
    return render(request, 'etests/dealer_testlist.html', {'ptests':ptests})
    
    
# Список тестирований для дистрибьютора
@login_required
def dist_testlist(request):
    user = request.user
    ptestslist = []
    subchilds = Group.objects.get(pk=3).user_set.all()
    """
    for subchild in subchilds:
        ptestslist += PlannedTest.objects.filter(allowed_users=subchild)
    ptests = list(set(ptestslist))
    """
    ptests = set(PlannedTest.objects.filter(allowed_users=subchilds))
    return render(request, 'etests/dist_testlist.html', {'ptests':ptests})
    

# Подробная информация по тестированию для дилера
@login_required
def dealer_test_info(request, ptest_id):
    user = request.user
    ptest = PlannedTest.objects.get(id = ptest_id)
    allowed_users = ptest.allowed_users.all()
    childs = user.childs.all()
    marks = []
    workers = []
    for child in childs:
        if child in allowed_users:
            if MainResult.objects.filter(ptest=ptest, user=child).last():
                marks.append(MainResult.objects.filter(ptest=ptest, user=child).last().mark)
                workers.append(child)
            else:
                marks.append('Не пройден')
                workers.append(child)
    results = zip(workers, marks)
    return render(request, 'etests/dealer_test_info.html', {'user': user, 'results': results, 'ptest': ptest})
    

    
# Подробная информация по тестированию для дистрибьютора
@login_required
def dist_test_info(request, ptest_id):
    user = request.user
    ptest = PlannedTest.objects.get(id = ptest_id)
    allowed_users = ptest.allowed_users.all()
    info = []
    subchilds = []
    dealers = []
    
    empls = Group.objects.get(pk=3).user_set.all()
    for empl in empls:
        if empl in allowed_users:
            subchilds.append(empl)
            dealers.append(empl.parentId)
            if MainResult.objects.filter(user=empl, ptest=ptest).last():
                info.append(MainResult.objects.filter(user=empl, ptest=ptest).last().mark)
            else:
                info.append(-1)
    info_users = zip(subchilds, dealers, info)
    modules = ptest.test.modules.all()
    return render(request, 'etests/dist_test_info.html', {'modules':modules, 'info_users': info_users, 'ptest': ptest})
    
# Статистика по тестированию
@login_required
def statistics(request, ptest_id):
    ptest = PlannedTest.objects.get(id=ptest_id)
    modules = ptest.test.modules.all()
    total_users = MainResult.objects.filter(ptest=ptest).count()
    marks = []
    middle_test_mark = 0
    min_test_mark = 0
    max_test_mark = 0
    for mark in MainResult.objects.filter(ptest=ptest).all():
        marks.append(mark.mark)
    if marks:
        middle_test_mark = int(sum(marks)/len(marks))
        min_test_mark = min(marks)
        max_test_mark = max(marks)
    else:
        middle_test_mark = 'Нет данных'
        min_test_mark = 'Нет данных'
        max_test_mark = 'Нет данных'
    x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    for mark in marks:
        if mark>=0 and mark<=10:
            x1 += 1
        elif mark>=11 and mark<=20:
            x2 += 1
        elif mark>=21 and mark<=30:
            x3 += 1
        elif mark>=31 and mark<=40:
            x4 += 1
        elif mark>=41 and mark<=50:
            x5 += 1
        elif mark>=51 and mark<=60:
            x6 += 1
        elif mark>=61 and mark<=70:
            x7 += 1
        elif mark>=71 and mark<=80:
            x8 += 1
        elif mark>=81 and mark<=90:
            x9 += 1
        else:
            x10 += 1
    
    middle_module_mark = []
    for module in modules:
        module_marks = []
        if Result.objects.filter(ptest=ptest, module=module).first():
            for mark in Result.objects.filter(ptest=ptest, module=module).all():
                module_marks.append(mark.mark)
            middle_module_mark.append(int(sum(module_marks)/len(module_marks)))
        else:
            middle_module_mark.append('Нет данных')
    module_marks = zip(modules, middle_module_mark)
    return render(request, 'etests/statistics.html', {'ptest':ptest, 'modules':modules, 'total_users': total_users,
                                                      'middle_test_mark':middle_test_mark,'min_test_mark': min_test_mark,
                                                      'max_test_mark': max_test_mark, 'marks': marks, 'x1': x1, 'x2': x2, 'x3': x3, 'x4': x4, 'x5': x5, 'x6': x6, 'x7': x7, 'x8': x8, 'x9': x9, 'x10': x10, 
                                                      'module_marks':module_marks})
    
# Статистика по модулю
@login_required
def module_statistics(request, ptest_id, module_id):
    ptest = PlannedTest.objects.get(id=ptest_id)
    module = Module.objects.get(id=module_id)
    module_marks = []
    middle_module_mark = 0
    for mark in Result.objects.filter(module=module, ptest=ptest).all():
        module_marks.append(mark.mark)
    if module_marks:
        middle_module_mark = int(sum(module_marks)/len(module_marks))
        min_module_mark = min(module_marks)
        max_module_mark = max(module_marks)
    else:
        middle_module_mark = 'Нет данных'
        min_module_mark = 'Нет данных'
        max_module_mark = 'Нет данных'
    question_count = module.questions.count()
    questions = module.questions.all()
    middle_time = []
    for question in questions:
        if QuestionTime.objects.filter(question=question, ptest=ptest).first():
            times = QuestionTime.objects.filter(question=question, ptest=ptest).all()
            total_time = []
            for time in times:
                difference = (time.end_time - time.start_time).total_seconds()
                total_time.append(difference)
            middle_time.append(int((sum(total_time)/len(total_time))))
        else:
            middle_time.append(0)
    
    if middle_time:
        middle_module_time = int(sum(middle_time))
    else:
        middle_module_time = 'Нет данных'
    
    #% правильных ответов
    q_persent = []
    for question in questions:
        q_persent.append(get_question_persent(ptest.id, module.id, question.id))
   
    question_times = zip(questions, q_persent, middle_time)   
    return render(request, 'etests/module_statistics.html', {'ptest':ptest, 'module':module, 'middle_module_mark': middle_module_mark, 'min_module_mark': min_module_mark, 'max_module_mark': max_module_mark,
                                                             'questions':questions, 'question_times': question_times, 'middle_module_time': middle_module_time})
    

# Статистика по вопросу
@login_required
def question_statistics(request, ptest_id, module_id, question_id):
    ptest = PlannedTest.objects.get(id=ptest_id)
    module = Module.objects.get(id=module_id)
    question = Question.objects.get(id=question_id)
    answers = question.answers.all()
    correct_answers = []
    incorrect_answers = []
    correct_answers_count = 0
    incorrect_answers_count = 0
    middle_time = 0
    correct_answer_try = 0
    
    
    total_correct = get_question_persent(ptest.id, module.id, question.id)
    
    total_answers = int(TestResult.objects.filter(question=question, ptest=ptest, module=module).count()/answers.count())
    if QuestionTime.objects.filter(question=question, ptest=ptest).first():
        times = QuestionTime.objects.filter(question=question, ptest=ptest).all()
        total_time = []
        for time in times:
            difference = (time.end_time - time.start_time).total_seconds()
            total_time.append(difference)
        middle_time = int(((sum(total_time)/len(total_time))))
    else:
        middle_time = 'Нет среднего времени'
    
    
    a_persent = []
    answers = question.answers.all()
    for answer in answers:
        a_persent.append(get_answer_persent(answer.id, ptest.id, module.id))
        
    
    answers_persents = zip(answers, a_persent)
    
    
    
    return render(request, 'etests/question_statistics.html', {'ptest':ptest, 'module':module, 'question': question, 'total_correct': total_correct,
                                                        'answers': answers, 'total_answers': total_answers, 'middle_time': middle_time, 'answers_persents':answers_persents})
    

# Тестирование для елернинга
@login_required
def testing_for_elearning(request, module_id, pcourse_id, emodule_id):
    module = Module.objects.get(id = module_id)
    user = request.user
    pcourse = PlannedCourse.objects.get(id = pcourse_id)
    emodule = EModule.objects.get(id = emodule_id)
    
    # Подсчет вопросов ( какой из скольки )
    if module.max_questions > 0:
        max_questions = module.max_questions
    else:
        max_questions = module.questions.count()
        
    questions = module.questions.all()
    q_num = 0
    try:
        time_objects = QuestionTime.objects.filter(pcourse=pcourse, user = user, question=questions).all()
        for t in time_objects:
            if t.end_time:
                q_num += 1
    except:
        pass
    question_num = q_num + 1 
    
    #Прием ответов пользователя
    if request.method == 'POST':
        post = request.POST.copy() # из этого надо получить список вариантов ответов с результатом True/False
        question_id = int(post['question.id'])
        question = Question.objects.get(id=question_id)
        answers = question.answers.all()
        result = {}
        qtime = QuestionTime.objects.filter(user = user, pcourse=pcourse, question=question).last()
        qtime.end_time = time()
        qtime.save()
        timediff = qtime.end_time - qtime.start_time
        diff = timediff.total_seconds()
        
        if diff <= (question.time + 5):
            for answer in answers:
                if str(answer.id) in request.POST.getlist('ans[]'):
                    r = TestResult(pcourse = pcourse, module = module, checked = True, question = question, user = user, answer = answer)
                else:
                    r = TestResult(pcourse = pcourse, module = module, checked = False, question = question, user = user, answer = answer)
                r.save()
           
    #Отправка следующего вопроса пользователю       
    if get_next_question_elearning(module_id, pcourse_id, user):
        question = get_next_question_elearning(module_id, pcourse_id, user)
        if not QuestionTime.objects.filter(pcourse=pcourse, user=user,question=question).first():
            start_time = QuestionTime(start_time = time(), user=user, question=question, pcourse=pcourse)
            start_time.save()
            timer = question.time
            return render(request, 'etests/question.html', {'user':user, 'question':question, 'timer':timer, 'question_num': question_num, 'max_questions': max_questions })
        else:
            q = QuestionTime.objects.filter(pcourse=pcourse, user=user,question=question).last()
            current = time()
            diff = current - q.start_time
            total_diff = int(diff.total_seconds())
            if total_diff < (question.time + 5):
                timer = question.time - total_diff
            else:
                answers = question.answers.all()
                result = {}
                
                for answer in answers:
                    r = TestResult(pcourse = pcourse, module = module, checked = False, question = question, user = user, answer = answer)
                    r.save()
                return redirect('/tests/elearning/%s/%s/%s/' % (pcourse_id, module_id, emodule_id)) 
            return render(request, 'etests/question.html', {'user':user, 'question':question, 'timer':timer }) 
    #Окончание модуля
    else:
        mark = float(get_module_mark_elearning(pcourse_id, module_id, user))
        if mark>=pcourse.min_mark_for_module:
            m = Attempt(pcourse=pcourse, module=emodule, user=user, mark=mark, is_ok=True)
            m.save()
        else:
            m = Attempt(pcourse=pcourse, module=emodule, user=user, mark=mark, is_ok=False)
            m.save()
        return redirect('/elearning/course/%s/' % pcourse_id) #на список модулей. Это конец модуля.


# Получение результатов в таблицу экселя
@login_required
def get_excel(request, ptest_id):
    import openpyxl
    from openpyxl.cell import get_column_letter
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=results.xlsx'
    wb = openpyxl.Workbook()
    ws = wb.get_active_sheet()
    ws.title = "Результаты теста"
    
    ptest = PlannedTest.objects.get(id=ptest_id)
    columns = [("ФИО", 70), ("Дилер", 40)]
    modules = ptest.test.modules.all()
    for module in modules:
        columns.append((module.name, len(module.name)))
    
    row_num = 0    
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]
        
    users = ptest.allowed_users.all()
    for user in users:
        row_num += 1
        row = [str(user.get_full_name()), str(user.parentId)]
        for module in modules:
            if Result.objects.filter(user=user, ptest=ptest, module=module).last():
                row.append(Result.objects.filter(user=user, ptest=ptest, module=module).last().mark)
            else:
                row.append('')
        if MainResult.objects.filter(user=user, ptest=ptest).last() == None:
            row.append('Не пройден')
        else:
            row.append(str(MainResult.objects.filter(user=user, ptest=ptest).last()))
        for col_num in range(len(row)):
            c = ws.cell(row=row_num + 1, column=col_num + 1)
            if row[col_num] == 'None':
                c.value = ''
            else:
                c.value = row[col_num]
            
            c.style.alignment.wrap_text = True
        
    
        
    wb.save(response)
    return response


# Получение результатов в таблицу экселя 2
@login_required
def get_excel2(request, test_id):
    import openpyxl
    from openpyxl.cell import get_column_letter
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=questions.xlsx'
    wb = openpyxl.Workbook()
    ws = wb.get_active_sheet()
    ws.title = "Вопросы"
    
    test = Test.objects.get(id=test_id)
    columns = [("ID вопроса/варианта ответа", 15), ("ID модуля", 15), ("Вопрос", 100)]
    
    row_num = 0    
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]
        
    modules = test.modules.all()
    questions = []
    for module in modules:
        for ques in module.questions.all():
            questions.append(ques)
    for q in questions:
        row_num += 1
        m_ids = ''
        for mid in q.modules.all():
            m_ids += (str(mid.id)+',')
        if m_ids.endswith(','):
            m_ids = m_ids[:-1]
        row = [q.id, m_ids, q.text]
        for col_num in range(len(row)):
            c = ws.cell(row=row_num + 1, column=col_num + 1)
            if row[col_num] == 'None':
                c.value = ''
            else:
                c.value = row[col_num]
            
            c.style.alignment.wrap_text = True
        for a in q.answers.all():
            row_num += 1
            row = [a.id, '', a.text, a.correct]
            for col_num in range(len(row)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                if row[col_num] == 'None':
                    c.value = ''
                else:
                    c.value = row[col_num]
                
                c.style.alignment.wrap_text = True
    
        
    wb.save(response)
    return response    
  
  
# Просмотр результатов тестирования по модулям для дистрибьютора
@login_required
def dist_modules_results(request, user_id, ptest_id):
    user = request.user
    subchild = User.objects.get(id=user_id)
    ptest = PlannedTest.objects.get(id=ptest_id)
    modules = ptest.test.modules.all()
    marks = []
    ptest_result = 0
    
    for module in modules:
        if Result.objects.filter(module=module, ptest=ptest, user=subchild).last():
            m = Result.objects.filter(module=module, ptest=ptest, user=subchild).last()
            marks.append(m.mark)
        else:
            marks.append(-1)
    
    if MainResult.objects.filter(ptest=ptest, user=subchild).last():
        result = MainResult.objects.filter(ptest=ptest, user=subchild).last()
        ptest_result = result.mark
    else:
        ptest_result = -1
    modules_marks = zip(modules, marks)
    return render(request, 'etests/dist_modules_results.html' , {'user': user, 'subchild': subchild, 'modules_marks': modules_marks, 'ptest_result': ptest_result, 'ptest': ptest})
    

# Просмотр результатов тестирования по вопросам для дистрибьютора
@login_required
def dist_questions_results(request, user_id, ptest_id, module_id):
    user = request.user
    subchild = User.objects.get(id=user_id)
    ptest = PlannedTest.objects.get(id=ptest_id)
    module = Module.objects.get(id=module_id)
    questions = []
    answers = []
    marks = []
    for question in module.questions.all():
        if TestResult.objects.filter(user=subchild, ptest=ptest, module=module, question=question).last():
            questions.append(question)
            results = TestResult.objects.filter(user=subchild, ptest=ptest, module=module, question=question).all()
            answers.append(results)
            marks.append(get_question_mark(ptest, module, question, subchild))
            
    questions_info = zip(questions, answers, marks)
    return render(request, 'etests/dist_questions_results.html', {'user':user, 'subchild':subchild, 'ptest': ptest, 'module': module, 'questions_info': questions_info})

 
