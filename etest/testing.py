from etest.models import *



def how_much_questions(module_id, ptest_id, user):
    module = Module.objects.get(id = module_id)
    questions = module.questions.all()
    counter = 0
    for question in questions:
        if TestResult.objects.filter(ptest = ptest, question=question, user=user).first():
            counter += 1
    return counter
    
def get_next_question(module_id, ptest_id, user):
    if module_id is not 0:
        module = Module.objects.get(id = module_id)
        questions = module.questions.all()
        ptest = PlannedTest.objects.get(id = ptest_id)
        max_q = module.max_questions
        answers = Answer.objects.filter(question=questions).all()
        middle = answers.count()/questions.count()
        results = TestResult.objects.filter(module=module, ptest=ptest, user=user).count()
        qs = module.questions.all().order_by('?')
        if max_q == 0:
            for question in questions:
                if not TestResult.objects.filter(ptest = ptest, question=question, user=user).first():
                    return question
                else:
                    pass
        else:
            if results < max_q*middle:
                for question in qs:
                    if not TestResult.objects.filter(user=user, question=question, ptest=ptest, module=module).first():
                        return question
                    else:
                        pass
        return False
    else:
        questions = []
        ptest = PlannedTest.objects.get(id = ptest_id)
        for module in ptest.test.modules():
            if module.max_questions:
                questions_count = module.max_questions - how_much_questions(module_id, ptest_id, user):
                try:
                    qs = module.questions.all().order_by('?')[0:module.max_questions]
                except:
                    qs = module.questions.all().order_by('?')
                questions.append(qs)
        
                


def get_question_mark():
    pass

def user_answer():
    pass

def get_question_num():
    pass

