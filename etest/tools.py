from etest.models import PlannedTest, Module, TestResult, Result, MainResult, Answer, Question, QuestionTime
from users.models import User
from elearning.models import PlannedCourse, Elearning_mark, ElearningFeedback, Attempt
from trainings.models import PlannedTraining, UserlistByDate, StudyLevel, Training
import pytz
import random
from django.utils import timezone


def get_next_question(module_id, ptest_id, user):
    module = Module.objects.get(id = module_id)
    questions = module.questions.all()
    ptest = PlannedTest.objects.get(id = ptest_id)
    max_q = module.max_questions
    answers = Answer.objects.filter(question=questions).all()
    middle = answers.count()/questions.count()
    results = TestResult.objects.filter(module=module, ptest=ptest, user=user).count()
    qs = module.questions.all().order_by('?')
    if max_q == 0:
        for question in questions:
            if not TestResult.objects.filter(ptest = ptest, question=question, user=user).first():
                return question
            else:
                pass
    else:
        if results < max_q*middle:
            for question in qs:
                if not TestResult.objects.filter(user=user, question=question, ptest=ptest, module=module).first():
                    return question
                else:
                    pass
    return False
        

def get_module_mark2(ptest_id, module_id, user):
    ptest = PlannedTest.objects.get(id = ptest_id)
    module = Module.objects.get(id = module_id)
    total_result = 0
    q_count = 0
    for question in module.questions.all():
        if TestResult.objects.filter(user=user, module=module, ptest=ptest).first():
            answers = TestResult.objects.filter(ptest=ptest, module=module, user=user, question=question).all()
            result = 0
            a_count = 0
            for answer in answers:
                if question.type_of_choise.id == 1:
                    if answer.answer.correct == True:
                        if answer.checked == True:
                            result+=5
                            a_count +=5
                        else:
                            result+=0
                            a_count +=5
                        continue
                if answer.checked == answer.answer.correct:
                    result += 1
                else:
                    result -= 1
                a_count +=1
            if result > 0:
                total_result += (result / a_count)
            q_count+=1
        else:
            return -1
    return (total_result / q_count) * 100


def get_question_mark(ptest, module, question, user):
    if TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).first():
        if TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).first().question.type_of_choise.id == 2:
            true_ans = 0
            for tr in TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).all():
                if tr.answer.correct == True:
                    if tr.checked == True:
                        true_ans += 1
                        return 100
                    else:
                        return 0
            if true_ans == 0:
                return 0
        else:
            result = 0
            for tr in TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).all():
                if tr.checked == tr.answer.correct:
                    result += 1
                else:
                    result -= 1
            if result>0:
                return (result / TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).count()) * 100
            else:
                return 0
    else:
        return None


def get_module_mark(ptest_id, module_id, user):
    ptest = PlannedTest.objects.get(id = ptest_id)
    module = Module.objects.get(id = module_id)
    result = 0
    q_count = 0
    c = 0
    for question in module.questions.all():
        z = get_question_mark(ptest, module, question, user)
        c += 1
        if z != None:
            result+=z
            q_count+=1
        print(c, z, result, q_count)
    if q_count == 0:
        return 0
    else:
        print(result, q_count)
        return result/q_count


def time():

    kiev = pytz.timezone('Europe/Kiev')
    time = timezone.localtime(timezone.now(), timezone=kiev)
    return time
    

#Построение таблички с пользовательскими оценками по модулям.
def get_user_mark(ptest, user):
    m_result = MainResult.objects.filter(user=user, ptest=ptest).last()
    modules = Module.objects.filter(test=ptest.test).all()
    string = '<td align="center">'+ user.get_full_name() + '</td>'
    results = Result.objects.filter(user=user, ptest=ptest).all()
    if m_result:
        m_res = str(m_result.mark)+'%'
        for result in results:
            mark = str(result.mark) + '%'
            string += '<td align="center">' + mark + '</td>'
    else:
        for x in ptest.test.modules.all():
            if Result.objects.filter(ptest=ptest, user=user, module=x):
                r = Result.objects.filter(ptest=ptest, user=user, module=x).last()
                mark = str(r.mark) + '%'
                string += '<td align="center">' + mark + '</td>'
            else:
                string += '<td align="center"></td>'
        m_res = 'Не пройден'
    string += '<td align="center">' + m_res + '</td>'
    return string
    
    
def get_next_question_elearning(module_id, pcourse_id, user):
    module = Module.objects.get(id = module_id)
    questions = module.questions.all()
    pcourse = PlannedCourse.objects.get(id = pcourse_id)
    max_q = module.max_questions
    answers = Answer.objects.filter(question=questions).all()
    middle = answers.count()/questions.count()
    results = TestResult.objects.filter(module=module, pcourse=pcourse, user=user).count()
    qs = module.questions.all().order_by('?')
    if max_q == 0:
        for question in questions:
            if not TestResult.objects.filter(pcourse = pcourse, question=question, user=user).first():
                return question
            else:
                pass
    else:
        if results < max_q*middle:
            for question in qs:
                if not TestResult.objects.filter(user=user, question=question, pcourse=pcourse, module=module).first():
                    return question
                else:
                    pass
    return False    


def get_module_mark_elearning_old(pcourse_id, module_id, user):
    pcourse = PlannedCourse.objects.get(id = pcourse_id)
    module = Module.objects.get(id = module_id)
    total_result = 0
    q_count = 0
    for question in module.questions.all():
        if TestResult.objects.filter(question=question, user=user, module=module, pcourse=pcourse).first():
            answers = TestResult.objects.filter(pcourse=pcourse, module=module, user=user, question=question).all()
            result = 0
            a_count = 0
            for answer in answers:
                if answer.checked == answer.answer.correct:
                    result += 1
                else:
                    if question.type_of_choise.id == 2:
                        result = 0
                        break
                    else:
                        result -= 1
                a_count +=1
            if result > 0:
                total_result += (result / a_count)
            q_count+=1
        else:
            pass
    return (total_result / q_count) * 100
    

def get_question_mark_elearning(pcourse, module, question, user):
    if TestResult.objects.filter(user=user, module=module, pcourse=pcourse, question=question).first():
        if TestResult.objects.filter(user=user, module=module, pcourse=pcourse, question=question).first().question.type_of_choise.id == 2:
            for tr in TestResult.objects.filter(user=user, module=module, pcourse=pcourse, question=question).all():
                if tr.answer.correct == True:
                    if tr.checked == True:
                        return 100
                    else:
                        return 0
        else:
            result = 0
            for tr in TestResult.objects.filter(user=user, module=module, pcourse=pcourse, question=question).all():
                if tr.checked == tr.answer.correct:
                    result += 1
                else:
                    result -= 1
            if result>0:
                return (result / TestResult.objects.filter(user=user, module=module, pcourse=pcourse, question=question).count()) * 100
            else:
                return 0
    else:
        return None


def get_module_mark_elearning(ptest_id, module_id, user):
    pcourse = PlannedCourse.objects.get(id = ptest_id)
    module = Module.objects.get(id = module_id)
    result = 0
    q_count = 0
    for question in module.questions.all():
        z = get_question_mark_elearning(pcourse, module, question, user)
        if z != None:
            result+=z
            q_count+=1
    if q_count == 0:
        return 0
    else:
        return result/q_count


    
def get_study_level(user):
    try:
        trainings = Training.objects.filter(auditory=user.job).all()
        mark = 0
        if StudyLevel.objects.filter(user=user).last():
            done = 0
            study_level = StudyLevel.objects.filter(user=user).last()
            timediff = time() - study_level.date
            diff = int(timediff.total_seconds()/60/60)
            if diff > 6:
                ptrainings = PlannedTraining.objects.filter(allowed_users=user, training=trainings, finished=True).all()
                for ptraining in ptrainings:
                    if ptraining.test_after and MainResult.objects.filter(ptest=ptraining.test_after, user=user).last():
                        done +=1
                    elif not ptraining.test_after and UserlistByDate.objects.filter(training=ptraining, user=user, date__date__lt=time).last():
                        done +=1
                study_level.mark = int(done/trainings.count()*100)
                study_level.date = time()
                study_level.save()
            mark = study_level.mark
            return mark
        else:
            done = 0
            ptrainings = PlannedTraining.objects.filter(allowed_users=user, training=trainings, finished=True).all()
            for ptraining in ptrainings:
                if ptraining.test_after and MainResult.objects.filter(ptest=ptraining.test_after, user=user).last():
                    done +=1
                elif not ptraining.test_after and UserlistByDate.objects.filter(training=ptraining, user=user, date__date__lt=time).last():
                    done +=1
            result = int(done/trainings.count()*100)
            study_level = StudyLevel(user=user, mark=result, date=time())
            study_level.save()
            mark = study_level.mark
            return mark
    except:
        mark = 0
        return mark
        
        
def reset(user_id):
    user = User.objects.get(id=user_id)
    qts = QuestionTime.objects.filter(user=user).all()
    results = Result.objects.filter(user=user).all()
    t_results = TestResult.objects.filter(user=user).all()
    for qt in qts:
        qt.delete()
    for res in results:
        res.delete()
    for t_result in t_results:
        t_result.delete()
    mress = MainResult.objects.filter(user=user).all()    
    for mres in mress:
        mres.delete()
    e_marks = Elearning_mark.objects.filter(user=user).all()
    efs = ElearningFeedback.objects.filter(user=user).all()
    attempts = Attempt.objects.filter(user=user).all()
    for em in e_marks:
        em.delete()
    for ef in efs:
        ef.delete()
    for at in attempts:
        at.delete()
        
def reset_test(user_id, test_id):
    user = User.objects.get(id=user_id)
    ptest = PlannedTest.objects.get(id=test_id)
    qts = QuestionTime.objects.filter(user=user, ptest=ptest).all()
    results = Result.objects.filter(user=user, ptest=ptest).all()
    t_results = TestResult.objects.filter(user=user, ptest=ptest).all()
    for qt in qts:
        qt.delete()
    for res in results:
        res.delete()
    for t_result in t_results:
        t_result.delete()
    mress = MainResult.objects.filter(user=user, ptest=ptest).all()    
    for mres in mress:
        mres.delete()
        
        
def get_question_persent(ptest_id, module_id, question_id):
    module = Module.objects.get(id=module_id)
    ptest = PlannedTest.objects.get(id=ptest_id)
    question = Question.objects.get(id=question_id)
    if ptest.allowed_users.last():
        users = ptest.allowed_users.all()
    else:
        users = []
        if MainResult.objects.filter(ptest=ptest).last():
            testresults = MainResult.objects.filter(ptest=ptest).all()
            for res in testresults:
                users.append(res.user)
    answers = question.answers.all()
    users_marks = []
    if users: 
        for user in users:
            mark = get_question_mark(ptest, module, question, user)
            if mark is not None:
                users_marks.append(mark)
    else:
        question_middle = 'Отсутствуют пользователи'
    if len(users_marks) > 0:
        question_middle = int(sum(users_marks)/len(users_marks))
    else:
        question_middle = 0
    return question_middle
        
   
    
    
def get_answer_persent(answer_id, ptest_id, module_id):
    answer = Answer.objects.get(id=answer_id)
    ptest = PlannedTest.objects.filter(id=ptest_id)
    module = Module.objects.filter(id=module_id)
    try:
        correct_tries = TestResult.objects.filter(module=module, ptest=ptest, answer=answer, checked=True).count()
        tries = TestResult.objects.filter(module=module, ptest=ptest, answer=answer).count()
        answer_persent = int((correct_tries/tries)*100)
    except:
        answer_persent = 0
    return answer_persent
    
    
def get_questions_ids(ptest_id):
    ptest = PlannedTest.objects.get(id=ptest_id)
    from random import shuffle
    ids = []
    modules = ptest.test.modules.all()
    total_questions_count = 0
    if ptest.type_of_testing.id == 1:
        for m in modules:
            if m.max_questions:
                total_questions_count += m.max_questions
            else:
                total_questions_count += m.questions.count()
    for m in modules:
        module_questions = []
        max_questions = m.max_questions
        questions = m.questions.all()
        shuffle(questions)
        for q in questions:
            if ptest.type_of_testing.id == 2 and len(module_questions) < max_questions:
                module_questions.append(q.id)
            elif ptest.type_of_testing.id == 1 and len(ids) < total_questions_count:
                ids.append(q.id)
        if ptest.type_of_testing.id == 2:
            ids.append(module_questions)
    return ids
