from django.conf.urls import patterns, include, url
from django.contrib import admin
from etest import views

urlpatterns = patterns('',
        url(r'^$', views.test_list),
        url(r'^info/(?P<plannedtest_id>\d+)/$', views.test_view),
        url(r'^modules/(?P<plannedtest_id>\d+)/$', views.modules),
        url(r'^modules_q/(?P<plannedtest_id>\d+)/$', views.modules_q),
        url(r'^question/(?P<ptest_id>\d+)/(?P<module_id>\d+)/$', views.testing),
        url(r'^testinfo/$', views.achievements),
        url(r'^testinfo/(?P<user_id>\d+)/$', views.achievements_for_stuff),
        url(r'^testlist/$', views.dealer_testlist),
        url(r'^fulltestinfo/(?P<ptest_id>\d+)/$', views.dealer_test_info),
        url(r'^test-list/$', views.dist_testlist),
        url(r'^full-testinfo/(?P<ptest_id>\d+)/$', views.dist_test_info),
        url(r'^elearning/(?P<pcourse_id>\d+)/(?P<module_id>\d+)/(?P<emodule_id>\d+)/$', views.testing_for_elearning),
        url(r'^statistics/(?P<ptest_id>\d+)/$', views.statistics),
        url(r'^statistics/(?P<ptest_id>\d+)/(?P<module_id>\d+)/$', views.module_statistics),
        url(r'^statistics/(?P<ptest_id>\d+)/(?P<module_id>\d+)/(?P<question_id>\d+)/$', views.question_statistics),
        url(r'^export/(?P<ptest_id>\d+)/$', views.get_excel),
        url(r'^export_q/(?P<test_id>\d+)/$', views.get_excel2),
        url(r'^modules_results/(?P<user_id>\d+)/(?P<ptest_id>\d+)/$', views.dist_modules_results),
        url(r'^full-testinfo/(?P<user_id>\d+)/(?P<ptest_id>\d+)/(?P<module_id>\d+)/$', views.dist_questions_results),
)
