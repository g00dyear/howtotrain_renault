from django.contrib import admin
from etest.models import *
from easy_select2 import select2_modelform
# Register your models here.

class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 3

QuestionForm = select2_modelform(Question, attrs={'width': '400px'})

class QuestionAdmin(admin.ModelAdmin):
    form = QuestionForm
    list_display = ('text', 'module', 'type_of_choise', 'tag')
    list_filter = ['modules__name', 'type_of_choise']
    search_fields = ('text', 'modules__name', 'type_of_choise__name', 'tags__name')
    inlines = [AnswerInline]
    save_as = True
    
    def module(self,obj):
        return ", ".join([m.name for m in obj.modules.all()])
    
    def tag(self, obj):
        return ", ".join([t.name for t in obj.tags.all()])
    

class PlannedTestAdmin(admin.ModelAdmin):
    filter_horizontal = ("allowed_users",)
    list_display = ('test', 'date_start', 'date_end')
    list_filter = ['test']
    
class ModuleAdmin(admin.ModelAdmin):
    list_display = ('name', 'theme', 'test',)
    list_filter = ['test__name', 'theme',]
    save_as = True
    
    def test(self, obj):
        return ", ".join([t.name for t in obj.test.all()])


admin.site.register(Test)
admin.site.register(Module, ModuleAdmin)
admin.site.register(TestResult)
admin.site.register(Result)
admin.site.register(MainResult)
admin.site.register(Question, QuestionAdmin)
admin.site.register(QuestionTime)
admin.site.register(Tag)
admin.site.register(QuestionType)
admin.site.register(Answer)
admin.site.register(TagCategory)
admin.site.register(PlannedTest, PlannedTestAdmin)
admin.site.register(CountType)
admin.site.register(TestingType)