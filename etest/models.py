from django.db import models
from users.models import User
from elearning.models import PlannedCourse
from trainings.models import PlannedTraining, TrainingDate
from django.contrib.auth.models import Group
import pytz
from django.utils import timezone
from ckeditor.fields import RichTextField
from django.db.models import Q
from .utils import create_test_events, make_mail
# Create your models here.


class Test(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название тестирования')
    
    def __str__(self):
        return self.name
        
    class Meta:
        verbose_name = 'Тестирование'
        verbose_name_plural = "Тестирования"
    
class Module(models.Model):
    sequence_number = models.IntegerField(default=1, verbose_name='Порядковый номер')
    name = models.CharField(max_length=255, verbose_name='Название модуля')
    theme = models.CharField(max_length=255, verbose_name='Тема')
    test = models.ManyToManyField(Test, related_name='modules', verbose_name='Тестирование')
    max_questions = models.IntegerField(default=0, verbose_name='Количество вопросов')
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Модуль'
        verbose_name_plural = "Модули"
  
    
class SubModule(models.Model):
    name = models.CharField(max_length=255)
    module = models.ManyToManyField(Module)

    def __str__(self):
        return self.name
  
        
class TagCategory(models.Model):
    name = models.CharField(max_length=25, verbose_name='Название')

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Категория тэгов'
        verbose_name_plural = "Категории тэгов"
        

class Tag(models.Model):
    category = models.ForeignKey(TagCategory, verbose_name='Категория тэгов')
    name = models.CharField(max_length=25, verbose_name='Название')
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Тэг'
        verbose_name_plural = "Тэги"
    
    
class QuestionType(models.Model):
    name = models.CharField(max_length=25, verbose_name='Тип вопроса')
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Тип вопроса'
        verbose_name_plural = "Типы вопросов"
    
class Question(models.Model):
    text = RichTextField(verbose_name='Текст вопроса')
    tags = models.ManyToManyField(Tag, blank=True, null=True, verbose_name='Тэги')
    type_of_choise = models.ForeignKey(QuestionType, verbose_name='Вид ответа')
    modules = models.ManyToManyField(Module, related_name='questions', verbose_name='Модули')
    time = models.IntegerField(default=60, verbose_name='Время на вопрос')
    def __str__(self):
        return self.text


    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = "Вопросы"
    
    
class Answer(models.Model):
    text = models.CharField(max_length = 255, verbose_name='Ответ')
    image = models.ImageField(upload_to= 'media/questions', blank=True, null=True,verbose_name="Изображение")
    question = models.ForeignKey(Question, related_name='answers', verbose_name='Вопрос')
    correct = models.BooleanField(default=False, verbose_name='Правильный/Не правильный')

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = "Ответы"
    

class TestManager(models.Manager):
    kiev = pytz.timezone('Europe/Kiev')
    time = timezone.localtime(timezone.now(), timezone=kiev)

    def current(self):
        return self.filter(date_start__lt=self.time,
                           date_end__gt=self.time,
                           for_elearning=False)

    def past(self):
        return self.filter(date_end__lt=self.time).all()


class CountType(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тип подсчета'
        verbose_name_plural = "Типы подсчета"
        
        
class TestingType(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тип тестирования'
        verbose_name_plural = "Типы тестирования"
                           
                           
class PlannedTest(models.Model):
    test_name = models.CharField(max_length=150, blank=True, null=True, verbose_name='Отображаемое имя')
    test = models.ForeignKey(Test, verbose_name='Тестирование')
    date_start = models.DateTimeField(verbose_name='Дата начала')
    date_end = models.DateTimeField(blank=True, verbose_name='Дата окончания')
    attempts = models.IntegerField(default=3, verbose_name='Количество попыток')
    rules = RichTextField(blank=True, null=True, verbose_name='Правила')
    allowed_users = models.ManyToManyField(User, blank=True, related_name = 'tests', limit_choices_to=Q(groups = 3) | Q(groups = 5), verbose_name='Пользователи', db_index=True)
    allowed_groups = models.ManyToManyField(Group, blank=True, related_name = 'a_tests', verbose_name='Допущенные группы')
    for_elearning = models.BooleanField(default=False, verbose_name='Для дистанционного обучения')
    for_training = models.BooleanField(default=False, verbose_name='Для тренингов')
    time_limit = models.IntegerField(null=True, blank=True)
    objects = TestManager()
    type_of_mark = models.ForeignKey(CountType, blank=True, null=True, verbose_name='Тип подсчета')
    type_of_testing = models.ForeignKey(TestingType, blank=True, null=True, verbose_name='Тип тестирования')
    mailing = models.BooleanField(default=False)

    def __str__(self):
        if self.test_name:
            return self.test_name + ' ' + self.date_start.strftime("%d-%m") + ' - ' + self.date_end.strftime("%d-%m")
        else:
            return self.test.name + ' ' + self.date_start.strftime("%d-%m") + ' - ' + self.date_end.strftime("%d-%m")
    
    class Meta:
        verbose_name = 'Запланированное тестирование'
        verbose_name_plural = "Запланированные тестирования"

    def save(self, *args, **kwargs):
        super(PlannedTest, self).save(*args, **kwargs)  # Call the "real" save() method.

        if self.mailing:
            create_test_events(self)

    @property
    def question_count(self):
        q_count = 0
        for module in self.test.modules.all():
            if module.max_questions:
                q_count += module.max_questions
            else:
                q_count += module.questions.count()
        return q_count

    @property
    def max_question_time(self):
        modules = self.test.modules.all()
        questions = []

        for m in modules:
            questions.append(m.questions.order_by('time').first().time)

        return max(questions)
    
        
class QuestionTime(models.Model):
    start_time = models.DateTimeField(verbose_name='Время начала')
    end_time = models.DateTimeField(blank=True, null=True, verbose_name='Время окончания')
    question = models.ForeignKey(Question, verbose_name='Вопрос', db_index=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', db_index=True)
    ptest = models.ForeignKey(PlannedTest, verbose_name='Запланированое тестирование', db_index=True, null=True, blank=True)
    pcourse = models.ForeignKey(PlannedCourse, db_index=True, null=True, blank=True)
    module = models.ForeignKey(Module, blank=True, null=True)

    class Meta:
        verbose_name = 'Время ответа'
        verbose_name_plural = "Время ответов"


# Результаты тестирований по вариантам ответа    
class TestResult(models.Model):
    class Meta:
        unique_together = (("ptest", "pcourse", "module", "user", "question", "answer"),)
        verbose_name = 'Результат по вариантам ответа'
        verbose_name_plural = "Результаты по вариантам ответов"
    ptest = models.ForeignKey(PlannedTest, verbose_name='Запланирование тестирование', null=True, blank=True)
    pcourse = models.ForeignKey(PlannedCourse, db_index=True, null=True, blank=True)
    module = models.ForeignKey(Module, verbose_name='Модуль')
    user = models.ForeignKey(User, verbose_name='Пользователь')
    question = models.ForeignKey(Question, verbose_name='Вопрос')
    answer = models.ForeignKey(Answer, verbose_name='Ответ')
    checked = models.BooleanField(default=False, verbose_name='Отмечен?')
    time = models.OneToOneField(QuestionTime, blank=True, null=True)
       
    
def get_question_mark(ptest, module, question, user):
    if TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).first():
        if TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).first().question.type_of_choise.id == 2:
            for tr in TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).all():
                if tr.answer.correct == True:
                    if tr.checked == True:
                        return 100
                    else:
                        return 0
        else:
            result = 0
            for tr in TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).all():
                if tr.checked == tr.answer.correct:
                    result += 1
                else:
                    result -= 1
            if result > 0:
                return (result / TestResult.objects.filter(user=user, module=module, ptest=ptest, question=question).count()) * 100
            else:
                return 0
    else:
        return None
        
        
# Результаты тестирований по модулям
class Result(models.Model):
    ptest = models.ForeignKey(PlannedTest, db_index=True, null=True, blank=True)
    module = models.ForeignKey(Module, related_name='marks', db_index=True)
    user = models.ForeignKey(User, related_name='module_marks', db_index=True)
    mark = models.IntegerField()

    class Meta:
        verbose_name = 'Результат по модулям'
        verbose_name_plural = "Результаты по модулям"

    def __str__(self):
        return str(self.mark)

    def get_results(self):
        results = Result.objects.filter(user=self.user, ptest=self.ptest).count()
        return results

    def save(self, *args, **kwargs):
        super(Result, self).save(*args, **kwargs) # Call the "real" save() method.
        if self.get_results() == self.ptest.test.modules.count() and not MainResult.objects.filter(user=self.user, ptest=self.ptest).last():
            if not self.ptest.type_of_mark or self.ptest.type_of_mark.id == 1:
                results = Result.objects.filter(user=self.user, ptest=self.ptest).all()
                total = 0
                for result in results:
                    total += result.mark
                mainmark = MainResult(ptest=self.ptest, user=self.user, mark=round((total / results.count()), 2))
                mainmark.save()
            elif self.ptest.type_of_mark.id == 2:
                question_marks = 0
                question_count = 0
                for module in self.ptest.test.modules.all():
                    for question in module.questions.all():
                        if TestResult.objects.filter(user=self.user, module=module, question=question, ptest=self.ptest).first():
                            question_marks += get_question_mark(self.ptest, module, question, self.user)
                            question_count += 1
                    
                mainmark = MainResult(ptest=self.ptest, user=self.user, mark=round((question_marks / question_count), 2))
                mainmark.save()
        else:
            pass
        

# Итоговые оценки по тестам
class MainResult(models.Model):
    ptest = models.ForeignKey(PlannedTest, db_index=True, related_name='testresults')
    user = models.ForeignKey(User, related_name='results', db_index=True)
    mark = models.IntegerField()
    testend = models.DateTimeField(auto_now=True)
    
    class Meta:
        verbose_name = 'Результат тестирования'
        verbose_name_plural = "Результаты тестирований"
        
    def __str__(self):
        return str(self.mark)

    def save(self, *args, **kwargs):
        super(MainResult, self).save(*args, **kwargs) # Call the "real" save() method.
        # Проверяем наличие принадлежности к определенному курсу.
        from newcourse.steps import level1
        level1(self.ptest, self.user, self.mark)

        if self.ptest.mailing:
            make_mail(self)
