from django.apps import AppConfig


class TestsConfig(AppConfig):
    name = 'etest'
    verbose_name = 'Тестирования'