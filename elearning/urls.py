from django.conf.urls import patterns, include, url
from django.contrib import admin
from elearning import views

urlpatterns = patterns('',
    url(r'^list/$', views.e_courses),
    url(r'^course/(?P<course_id>\d+)/$', views.course_view),
    url(r'^module/(?P<course_id>\d+)/(?P<module_id>\d+)/$', views.module_view),
    url(r'^module/(?P<course_id>\d+)/(?P<module_id>\d+)/finish$', views.module_finish),
    url(r'^userlist/(?P<course_id>\d+)/$', views.userlist),
    url(r'^user_course_info/(?P<course_id>\d+)/(?P<user_id>\d+)/$', views.elearning_users_info),
    url(r'^feedback/$', views.feedback),
)
