from django.db import models
from users.models import User
from django.contrib.auth.models import Group
from ckeditor.fields import RichTextField
import pytz
from django.utils import timezone
from django.db.models import Q
from sortedm2m.fields import SortedManyToManyField
from newcourse.models import Level2, Level2Part
from .utils import create_elearning_events, make_mail


# Create your models here.
class ECourse(models.Model):
    name = models.CharField(max_length=200, verbose_name='Название курса')
    description = models.TextField(verbose_name="Описание")
    
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Дистанционный курс'
        verbose_name_plural = "Дистанционные курсы"


class EModule(models.Model):
    sequence_number = models.IntegerField(default=1, verbose_name='Порядковый номер')
    name = models.CharField(max_length=200, verbose_name="Название модуля")
    presentation = models.FileField(upload_to='presentations/', null=True, blank=True)
    ECourse = models.ForeignKey(ECourse, related_name='modules', verbose_name="Курс")
    content = RichTextField(verbose_name="Содержимое модуля")
    description = models.TextField(verbose_name="Описание")
    finish_test_module = models.ForeignKey('etest.Module', blank=True, null=True, verbose_name="Модуль вопросов")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Модуль'
        verbose_name_plural = "Модули"


class CourseManager(models.Manager):
    kiev = pytz.timezone('Europe/Kiev')
    time = timezone.localtime(timezone.now(), timezone=kiev)

    def current(self):
        return self.filter(date_start__lt=self.time,
                           date_end__gt=self.time)

    def past(self):
        return self.filter(date_end__lt=self.time).all()


class PlannedCourse(models.Model):
    course_name = models.CharField(max_length=150, blank=True, null=True, verbose_name='Отображаемое имя')
    course = models.ForeignKey(ECourse, verbose_name="Курс")
    date_start = models.DateTimeField(verbose_name="Дата начала")
    date_end = models.DateTimeField(blank=True, verbose_name="Дата окончания")
    attempts_num = models.IntegerField(default=3, verbose_name="Количество попыток")
    min_mark_for_module = models.IntegerField(default=70, verbose_name='Минимальная оценка для модуля')
    min_mark_for_test = models.IntegerField(default=70, verbose_name='Минимальная оценка для тестирования')
    allowed_users = models.ManyToManyField(User, blank=True, related_name = 'courses', verbose_name="Разрешенные пользователи", limit_choices_to=Q(groups = 3) | Q(groups=5))
    allowed_groups = models.ManyToManyField(Group, blank=True, related_name = 'a_courses', verbose_name="Разрешенные группы")
    start_test = models.ForeignKey('etest.PlannedTest', blank=True, null=True, verbose_name="Тест в начале", limit_choices_to={'for_elearning': True}, related_name='pcourse_start')
    finish_test = models.ForeignKey('etest.PlannedTest', blank=True, null=True, verbose_name="Тест в конце", limit_choices_to={'for_elearning': True}, related_name='pcourse_finish')
    objects = CourseManager()
    for_test = models.BooleanField(verbose_name='Для целей тестирования')
    mailing = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Запланированый курс'
        verbose_name_plural = "Запланированные курсы"

    def __str__(self):
        if self.course_name:
            return self.course_name + ' ' + self.date_start.strftime("%d-%m") + ' - ' + self.date_end.strftime("%d-%m")
        else:
            return self.course.name + ' ' + self.date_start.strftime("%d-%m") + ' - ' + self.date_end.strftime("%d-%m")

    def get_name(self):
        if self.course_name:
            return self.course_name
        else:
            return self.course.name

    def save(self, *args, **kwargs):
        super(PlannedCourse, self).save(*args, **kwargs)  # Call the "real" save() method.

        if self.mailing:
            create_elearning_events(self)


class Attempt(models.Model):
    user = models.ForeignKey(User, db_index=True)
    module = models.ForeignKey(EModule, db_index=True, verbose_name='Модуль')
    pcourse = models.ForeignKey(PlannedCourse, null=True, blank=True, related_name='attempts', db_index=True, verbose_name='Курс')
    mark = models.FloatField()
    date = models.DateTimeField(auto_now=True, verbose_name='Дата')
    is_ok = models.NullBooleanField(db_index=True)

    class Meta:
        verbose_name = 'Попытка прохождения'
        verbose_name_plural = "Попытки прохождения"

    def __str__(self):
        return str(self.user) + ' ' + str(self.pcourse.course)


class Elearning_mark(models.Model):
    user = models.ForeignKey(User, db_index=True)
    pcourse = models.ForeignKey(PlannedCourse, related_name='result', db_index=True, verbose_name='Курс')
    date = models.DateTimeField(auto_now=True, verbose_name='Дата')
    mark = models.FloatField()

    class Meta:
        verbose_name = 'Результат прохождения'
        verbose_name_plural = "Результаты прохождения"

    def __str__(self):
        return str(self.user) + ' ' + str(self.pcourse.course)

    def save(self, *args, **kwargs):
        super(Elearning_mark, self).save(*args, **kwargs) # Call the "real" save() method.
        # Проверяем наличие принадлежности к определенному курсу.
        from newcourse.steps import level2
        level2(self.pcourse, self.mark, self.user)

        if self.pcourse.mailing:
            make_mail(self)



        
class ElearningFeedback(models.Model):
    pcourse = models.ForeignKey(PlannedCourse)
    date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)
    mark = models.IntegerField()
    feedback = models.TextField()
