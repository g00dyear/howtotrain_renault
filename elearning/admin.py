
from django.contrib import admin
from elearning.models import EModule, ECourse, PlannedCourse, Attempt, Elearning_mark, ElearningFeedback
from easy_select2 import select2_modelform
# Register your models here.


PlannedCourseForm = select2_modelform(PlannedCourse, attrs={'width': '400px'})
EModuleForm = select2_modelform(EModule, attrs={'width': '400px'})


class PlannedCourseAdmin(admin.ModelAdmin):
    form = PlannedCourseForm
    filter_horizontal = ("allowed_users", "allowed_groups")
    list_display = ('course', 'date_start', 'date_end', 'for_test')
    list_filter = ['course'] 


class EmoduleAdmin(admin.ModelAdmin):
    form = EModuleForm
    list_display = ('name', 'ECourse')
    list_filter = ['ECourse',]
    save_as = True
    

class Elearning_markAdmin(admin.ModelAdmin):
    list_display = ('pcourse', 'date', 'user', 'mark')
    
    
class AttemptAdmin(admin.ModelAdmin):
    list_display = ('pcourse', 'date')
    list_filter = ['pcourse', 'module']
   
admin.site.register(ECourse)
admin.site.register(ElearningFeedback)
admin.site.register(EModule, EmoduleAdmin)
admin.site.register(PlannedCourse, PlannedCourseAdmin)
admin.site.register(Attempt, AttemptAdmin)
admin.site.register(Elearning_mark, Elearning_markAdmin)