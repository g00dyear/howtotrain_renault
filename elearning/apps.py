from django.apps import AppConfig


class ETestsConfig(AppConfig):
    name = 'elearning'
    verbose_name = 'Дистанционное обучение'
    