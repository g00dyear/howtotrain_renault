from django.shortcuts import render, redirect, HttpResponse
from .models import ECourse, EModule, Attempt, PlannedCourse, Elearning_mark, ElearningFeedback
from users.models import User
from etest.models import MainResult, TestResult, QuestionTime, Module, Result
from django.contrib.auth.decorators import login_required, permission_required
from etest.tools import time
import datetime
from django.contrib.auth.models import Group
# Create your views here.


# Страница /Дистанционное обучение для пользователя, дилера и дистрибьютора
@login_required    
def e_courses(request):
    user = request.user
    if user.is_user():
        now = datetime.datetime.now()
        ecourses = PlannedCourse.objects.filter(date_start__lt=now, date_end__gt=now, allowed_users=user).all()
        withmarks = []
        courses_with_marks = []
        courses_wo_marks = []
        for course in ecourses:
            if course.finish_test:
                if MainResult.objects.filter(user=user, ptest=course.finish_test).last():
                    m = MainResult.objects.filter(user=user, ptest=course.finish_test).last()
                    if not Elearning_mark.objects.filter(user=user, pcourse=course):
                        e_mark = Elearning_mark(user=user, pcourse=course, mark=m.mark)
                        e_mark.save()
                    courses_with_marks.append(course)
                    withmarks.append(int(m.mark))
                else:
                    courses_wo_marks.append(course)
            else:
                if Elearning_mark.objects.filter(user=user, pcourse=course).last():
                    m = Elearning_mark.objects.filter(user=user, pcourse=course).last()
                    withmarks.append(int(m.mark))
                    courses_with_marks.append(course)
                else:
                    courses_wo_marks.append(course)
        cmrks = zip(courses_with_marks, withmarks)
        return render(request, 'elearning/mainpage_user.html', { 'cmrks':cmrks, 'courses': courses_wo_marks,  'user': user})
        
    elif user.is_dealer():
        childs = user.childs.all()
        ecourses = []   
        for child in childs:
            for course in child.courses.current():
                ecourses.append(course)
        ecourses = list(set(ecourses))
        return render(request, 'elearning/mainpage_dealer.html', {'courses': ecourses, 'user': user})
        
    elif user.is_dist():
        subchilds = Group.objects.get(id=3).user_set.all()
        ecourses = []
        for subchild in subchilds:
            for course in subchild.courses.all():
                if not course.for_test:
                    ecourses.append(course)
        ecourses = list(set(ecourses))
        def start(a):
            return a.date_start
        ecourses = sorted(ecourses, key=start)
        return render(request, 'elearning/mainpage_dist.html', {'courses': ecourses, 'user': user})            
  

# Просмотр содержимого курса для пользователя. Отправка фидбека в конце прохождения курса. Просмотр результатов по пройденным курсам.              
@login_required                                        
def course_view(request, course_id):
    user = request.user
    course = PlannedCourse.objects.get(id=course_id).course
    pcourse = PlannedCourse.objects.get(id=course_id)
    modules = course.modules.all().order_by('sequence_number')
    completed_modules = []
    marks = []
    end_test = pcourse.finish_test
    start_test = pcourse.start_test
    
    start_mrk = MainResult.objects.filter(user=user, ptest=start_test).last()
    counter = 0
    mark = 0
    attempts = []
    
    
    if end_test:
        if MainResult.objects.filter(user=user, ptest=pcourse.finish_test).last():
            m = MainResult.objects.filter(user=user, ptest=pcourse.finish_test).last()
            if not Elearning_mark.objects.filter(user=user, pcourse=pcourse):
                e_mark = Elearning_mark(user=user, pcourse=pcourse, mark=m.mark)
                e_mark.save()
    finish_mrk = Elearning_mark.objects.filter(user=user, pcourse=pcourse).last()       
    for module in modules:
        if Attempt.objects.filter(module = module, pcourse = pcourse, is_ok=True, user=user).first():
            a = Attempt.objects.filter(module = module, pcourse = pcourse, is_ok=True, user=user).first()
            completed_modules.append(module)
            marks.append(int(a.mark))
        else:
            marks.append(0)
        if Attempt.objects.filter(module=module, user=user, pcourse=pcourse).last():
            a = Attempt.objects.filter(module=module, user=user, pcourse=pcourse).count()
            if (pcourse.attempts_num-a) > 0:
                attempts.append(pcourse.attempts_num-a)
            else:
                attempts.append(0)
        else:
            attempts.append(pcourse.attempts_num)
    # TODO: FIXME shit happens

    if not end_test:
        if not Elearning_mark.objects.filter(user=user, pcourse=pcourse).last():
            for m in modules:
                if Attempt.objects.filter(module=m, user=user,is_ok=True).last():
                    counter+=1
                if counter == modules.count():
                    for m in modules:
                        attempt = Attempt.objects.filter(module=m, user=user).last()
                        mark += attempt.mark
                    r = int(float(mark)/float(modules.count()))
                    result = Elearning_mark(user=user, pcourse=pcourse, mark=r)
                    result.save()
    message = ''
    comment = ''
    feedback_mark = ''
    mmarks = zip(modules, marks, attempts)
    renderbutton = False
    if Elearning_mark.objects.filter(user=user, pcourse=pcourse) and not ElearningFeedback.objects.filter(user=user, pcourse=pcourse).last():
        renderform = True
        renderresult = False
    elif Elearning_mark.objects.filter(user=user, pcourse=pcourse) and ElearningFeedback.objects.filter(user=user, pcourse=pcourse).last() and request.GET.get('change') == None:
        renderform = False
        renderresult = True
        feedback = ElearningFeedback.objects.filter(user=user, pcourse=pcourse).last()
        message = 'Вы оценили данный курс на ' + str(feedback.mark) + ' баллов'
        comment = 'Ваш комментарий: ' + feedback.feedback
        time_difference = (time() - feedback.date).total_seconds()
        if time_difference < 1200:
            renderbutton = True
    elif request.GET.get('change') == '1':
        comment = ElearningFeedback.objects.filter(user=user, pcourse=pcourse).last().feedback
        feedback_mark = ElearningFeedback.objects.filter(user=user, pcourse=pcourse).last().mark
        renderform = True
        renderresult = False
    else:
        renderform = False
        renderresult = False
        
    if request.method == 'POST':
        post = request.POST.copy()
        if not ElearningFeedback.objects.filter(user=user, pcourse=pcourse).last():
            mark = int(post.getlist('radios[]')[0])
            comment = post['FeedbackText']
            result = ElearningFeedback(user=user, mark=mark, pcourse=pcourse, feedback=comment)
            result.save()
            return redirect('/elearning/course/%s' %(pcourse.id))
        else:
            feedback = ElearningFeedback.objects.filter(user=user, pcourse=pcourse).last()
            feedback.feedback = post['FeedbackText']
            feedback.mark = int(post.getlist('radios[]')[0])
            feedback.save()
            return redirect('/elearning/course/%s' %(pcourse.id))
    #TODO: Lets make a bit magic
    if Elearning_mark.objects.filter(pcourse=pcourse, user=user).first():
        try:
            course_elem = pcourse.ecourse_part
        except:
            course_elem = None
        if course_elem:
            course = course_elem.course_part.study_course
            trainings = course.get_trainings()
            if trainings:
                trs = []
                for tr in trainings:
                    if tr.dates.order_by('date').first().date >= pcourse.date_end:
                        trs.append(tr)
                if trs:
                    tr = trs[0]
                    if user not in tr.allowed_users.all():
                        tr = None
                else:
                    tr = None
            else:
                tr = None
        else:
            tr = None
    else:
        tr = None

    return render(request, 'elearning/course_view.html', {'modules':modules, 
                                                          'course': course, 
                                                          'pcourse': pcourse, 
                                                          'user':user, 
                                                          'cmodules':completed_modules, 
                                                          'mmarks': mmarks,
                                                          'end_test': end_test,
                                                          'finish_mrk': finish_mrk,
                                                          'start_mrk': start_mrk,
                                                          'start_test': start_test,
                                                          'renderform': renderform,
                                                          'message': message,
                                                          'comment': comment,
                                                          'feedback_mark': feedback_mark,
                                                          'renderresult': renderresult,
                                                          'renderbutton': renderbutton,
                                                          'tr': tr})


# Изменение фидбека
@login_required
def feedback(request, change=False):
    x = request.GET.get('change')
    return HttpResponse(x)
    
    
# Содержимое модуля
@login_required    
def module_view(request, module_id, course_id):
    user = request.user
    module = EModule.objects.get(id=module_id)
    pcourse =  PlannedCourse.objects.get(id=course_id)
    mark = 0
    
    if module.finish_test_module:
        if Attempt.objects.filter(user=user, module=module, pcourse=pcourse):
            attempts = Attempt.objects.filter(user=user, module=module, pcourse=pcourse).count()
            r = Attempt.objects.filter(user=user, module=module, pcourse=pcourse).last()
            result = r.mark
            if result <= pcourse.min_mark_for_module and (pcourse.attempts_num-attempts)>0:
                mark = 1
        else:
            mark = 1
    else:
        mark = 0
    return render(request, 'elearning/module_view.html', {'module':module, 'pcourse': pcourse, 'mark': mark,})
    

# Завершение модуля
@login_required   
def module_finish(request, module_id, course_id):
    counter = 0
    mark = 0
    user = request.user
    module = EModule.objects.get(id=module_id)
    pcourse =  PlannedCourse.objects.get(id=course_id)
    modules = pcourse.course.modules.all()
    attempts = Attempt.objects.filter(module=modules, user=user).all()
    # Для модуля нету завершающего теста
    if not module.finish_test_module:
        if not Attempt.objects.filter(user=user, module=module, pcourse=pcourse).last():
            a = Attempt(user=request.user, module=module, pcourse = pcourse, mark=100, is_ok=True)
            a.save()
        return redirect('/elearning/course/%s' % pcourse.id)
    # Переход к завершающему теста модуля
    else:
        module_test = Module.objects.get(id = module.finish_test_module.id)
        TestResult.objects.filter(user=user, pcourse=pcourse, module=module_test).delete()
        questions = module_test.questions.all()
        QuestionTime.objects.filter(user=user, pcourse=pcourse, question=questions).delete()
        if not Attempt.objects.filter(user=user, module=module, pcourse=pcourse, is_ok=True).last():
            return redirect('/tests/elearning/%s/%s/%s' % (pcourse.id, module.finish_test_module.id, module.id))
        else:
            return redirect('/elearning/course/%s' % pcourse.id)
    
    
# Список пользователей в курсе дистанционного обучения с оценками для дилера и дистрибьютора
@login_required
def userlist(request, course_id):
    user = request.user
    course = PlannedCourse.objects.get(id=course_id)
    modules = course.course.modules.all()
    
    if user.is_dealer():
        users = []
        results = []
        total_modules = modules.count()
        modules_done_all = []
        for child in user.childs.all():
            if child in course.allowed_users.all():
                users.append(child)
            modules_done = 0
            for module in modules:
                if Attempt.objects.filter(module=module, user=child, pcourse=course).last():
                    modules_done += 1
            modules_done_all.append(modules_done)
        for child in user.childs.all():
            if Elearning_mark.objects.filter(user=child, pcourse=course).last():
                m = Elearning_mark.objects.filter(user=child, pcourse=course).last()
                mark = m.mark
                results.append(mark)
            else:
                results.append('Не окончен')
        userlist = zip(users, modules_done_all, results)
        return render(request, 'elearning/dealer_elearning_userlist.html', {'user': user,
                                                                            'users': users,
                                                                            'course': course,
                                                                            'userlist':userlist,
                                                                            'total_modules':total_modules})
    if user.is_dist():
        total_modules = modules.count()
        modules_done_all = []
        results = []
        start_test_marks = []
        finish_test_marks = []
        
        childs = course.allowed_users.all()
        for child in childs:
            modules_done = 0
            #Проверяем пройденные модуля
            
            modules_done = Attempt.objects.filter(user=child, pcourse=course).count()
            modules_done_all.append(modules_done)
            #Проверяем оценку стартового тестирования
            if course.start_test:
                if MainResult.objects.filter(ptest=course.start_test, user=child):
                    mark = MainResult.objects.filter(ptest=course.start_test, user=child).last().mark
                    start_test_marks.append(mark)
                else:
                    start_test_marks.append('Нет оценки')
            else:
                start_test_marks.append('Тест не предусмотрен')
            #Проверяем оценку финишного тестирования
            if course.finish_test:
                if MainResult.objects.filter(ptest=course.finish_test, user=child):
                    mark = MainResult.objects.filter(ptest=course.finish_test, user=child).last().mark
                    finish_test_marks.append(mark)
                else:
                    finish_test_marks.append('Нет оценки')
            else:
                finish_test_marks.append('Тест не предусмотрен')
            #Проверяем общую оценку курса
            if Elearning_mark.objects.filter(user=child, pcourse=course).last():
                m = Elearning_mark.objects.filter(user=child, pcourse=course).last()
                mark = m.mark
                results.append(mark)
            else:
                results.append(0)
        """
        for child in user.childs.all():
            subchilds = child.childs.all()
            for subchild in subchilds:
                if subchild in course.allowed_users.all():
                    users.append(subchild)
                modules_done = 0
                for module in modules:
                    if Attempt.objects.filter(module=module, user=subchild, pcourse=course).last():
                        modules_done += 1
                modules_done_all.append(modules_done)
            for subchild in subchilds:
                if Elearning_mark.objects.filter(user=subchild, pcourse=course).last():
                    m = Elearning_mark.objects.filter(user=subchild, pcourse=course).last()
                    mark = m.mark
                    results.append(mark)
                else:
                    results.append(0)
            for subchild in subchilds:
                if course.start_test:
                    if MainResult.objects.filter(ptest=course.start_test, user=subchild):
                        mark = MainResult.objects.filter(ptest=course.start_test, user=subchild).last().mark
                        start_test_marks.append(mark)
                    else:
                        start_test_marks.append('Нет оценки')
                else:
                    start_test_marks.append('Тест не предусмотрен')
                    
                if course.finish_test:
                    if MainResult.objects.filter(ptest=course.finish_test, user=subchild):
                        mark = MainResult.objects.filter(ptest=course.finish_test, user=subchild).last().mark
                        finish_test_marks.append(mark)
                    else:
                        finish_test_marks.append('Нет оценки')
                else:
                    finish_test_marks.append('Тест не предусмотрен')
        """
        feedbacks = ElearningFeedback.objects.filter(pcourse=course)
        feedback_mark = 0
        feedback_texts = []
        for feedback in feedbacks:
            feedback_mark += feedback.mark
            if feedback.feedback:
                feedback_texts.append(feedback)
        if feedbacks.count() > 0:
            feedback_middle = round((feedback_mark/feedbacks.count()), 1)
        else:
            feedback_middle = 'Пользователи еще не оставили оценок'
        userlist = zip(childs, start_test_marks, modules_done_all, results, finish_test_marks)
        
        return render(request, 'elearning/dist_elearning_userlist.html', {'user': user,
                                                                          'course': course,
                                                                          'results': results,
                                                                          'userlist':userlist,
                                                                          'total_modules':total_modules,
                                                                          'feedback_texts': feedback_texts,
                                                                          'feedback_middle': feedback_middle,})
 
        
@login_required
def elearning_users_info(request, course_id, user_id):
    user = request.user
    child = User.objects.get(id=user_id)
    course = PlannedCourse.objects.get(id=course_id)
    modules = course.course.modules.all()
    results = []
    for module in modules:
        if Attempt.objects.filter(user=child, module=modules).last():
            attempt = Attempt.objects.filter(user=child, module=module).last()
            mark = attempt.mark
            results.append(mark)
        else:
            results.append(-1)
    module_marks = zip(modules, results)
    return render(request, 'elearning/elearning_user_info.html', {'user': user, 
                                                                  'child': child, 
                                                                  'course': course, 
                                                                  'module_marks': module_marks,
                                                                  'modules': modules})
                                                                  
                                                                
