from mailing.models import MessageEventNew
from datetime import datetime, timedelta
from mailing.message_templates import *
from mailing.mailgun import send_message


def create_elearning_events(elearning):
    """
    :param elearning;
    :return: Nothing;
    Creating all types of needed events: invitation, start, reminder
    """
    if not MessageEventNew.objects.filter(elearning=elearning).first():
        # Create start event
        start_event = MessageEventNew(date=elearning.date_start,
                                   elearning=elearning,
                                   event_type='start')
        start_event.save()

        # create invitation event
        inv_event = MessageEventNew(date=datetime.now(),
                                 elearning=elearning,
                                 event_type='invite')
        inv_event.save()

        # create reminder events
        # reminder before 1 day to end
        reminder = MessageEventNew(date=(elearning.date_end - timedelta(days=1)),
                                elearning=elearning,
                                event_type='reminder1')
        reminder.save()

        # halftime reminder
        halftime = ((elearning.date_end - elearning.date_start)/2).days
        reminder2 = MessageEventNew(date=(elearning.date_end - timedelta(days=halftime)),
                                 elearning=elearning,
                                 event_type='reminder2')
        reminder2.save()
    else:
        events = MessageEventNew.objects.filter(elearning=elearning,
                                             sent=False).all()
        for event in events:
            if event.event_type == 'start':
                if event.date != elearning.date_start:
                    event.date = elearning.date_start

            if event.event_type == 'invite':
                if event.date.day != datetime.now().day:
                    event.date = datetime.now()

            if event.event_type == 'reminder1':
                if event.date != (elearning.date_end - timedelta(days=1)):
                    event.date = (elearning.date_end - timedelta(days=1))

            if event.event_type == 'reminder2':
                halftime = ((elearning.date_end - elearning.date_start)/2).days
                if event.date != (elearning.date_end - timedelta(days=halftime)):
                    event.date = (elearning.date_end - timedelta(days=halftime))

            event.save()


def make_mail(result):
    from newcourse.utils import translate_elearning_messages

    if result.mark >= 80:
        message, subject = translate_elearning_messages(elearning_success_text,
                                                        elearning_success_subject,
                                                        result.user,
                                                        result.pcourse,
                                                        result.mark)
    else:
        message, subject = translate_elearning_messages(elearning_unsuccess_text,
                                                        elearning_unsuccess_subject,
                                                        result.user,
                                                        result.pcourse,
                                                        result.mark)
    send_message(message, subject, result.user.email)

