from django.conf.urls import patterns, include, url
from django.contrib import admin
from checklist import views


urlpatterns = patterns('',
    url(r'^view/(?P<dealer_id>\d+)/$', views.checklist_view),
    url(r'^report/(?P<dealer_id>\d+)/(?P<category_id>\d+)/(?P<subcategory_id>\d+)/$', views.report),
    url(r'^report/(?P<report_id>\d+)/$', views.report_view),
    url(r'^archive/$', views.archive),
    url(r'^comparison/$', views.dealer_comparison),
    url(r'^dealer_comparison/$', views.dealer_comparison),
    url(r'^info/$', views.dealer_view),
    url(r'^dealer/(?P<dealer_id>\d+)/$', views.dist_dealer_view),
    url(r'^region-info/$', views.region_info),
    url(r'^region/(?P<region_id>\d+)/$', views.dist_region_view),
    url(r'^regmanager/(?P<manager_id>\d+)/$', views.dist_regmanager_view),
    url(r'^reports/archive/$', views.dist_archive),
    url(r'^dealer-reports/$', views.dealer_report_view),
    url(r'^dealer/report/(?P<report_id>\d+)/$', views.dealer_report_view),
    url(r'^dealer/archive/$', views.dealer_archive),
    url(r'^excel/(?P<dealer_id>\d+)/$', views.excel),
)