from django.shortcuts import render, redirect, HttpResponse
from users.models import User, CustomGroup
from .models import Category, SubCategory, Report, Point, Mark
from django.contrib.auth.decorators import login_required, permission_required
from .tools import *

# Чеклист с категориями и подкатегориями
@login_required
def checklist_view(request, dealer_id):
    user = request.user
    if user.is_regmanager():
        dealer = User.objects.get(id=dealer_id)
        categories = Category.objects.all()
        subcategory_marks = {}
        category_marks = {}
        report_dates = {}
        for category in categories:
            for subcategory in category.subcategories.all():
                subcategory_marks[subcategory.name] = (get_subcategory_mark(subcategory.id, dealer.id))
                if Report.objects.filter(subcategory=subcategory, dealer=dealer, performer=user).last():
                    report = Report.objects.filter(subcategory=subcategory, dealer=dealer, performer=user).last()
                    report_dates[subcategory.name] = report.date
                else:
                    report_dates[subcategory.name] = 'Отчет отсутствует'
            category_marks[category.name] = (get_category_mark2(category.id, dealer.id))
            
        return render(request, 'checklist/checklist_view.html', {'user': user, 'dealer': dealer, 'categories': categories, 'subcategory_marks': subcategory_marks, 'category_marks': category_marks, 
                                                                 'report_dates': report_dates})
    else:
        return redirect('/login/')
    
# Заполнение сабкатегории чеклиста
@login_required
def report(request, dealer_id, category_id, subcategory_id):
    user = request.user
    if user.is_regmanager():
        dealer = User.objects.get(id=dealer_id)
        childs = dealer.childs.all()
        if CustomGroup.objects.filter(dealer = user).last() == CustomGroup.objects.filter(dealer=dealer).last():
            category = Category.objects.get(id=category_id)
            subcategory = SubCategory.objects.get(id=subcategory_id)
            points = subcategory.points.all()
            if Report.objects.filter(subcategory=subcategory, dealer=dealer).last():
                report = Report.objects.filter(subcategory=subcategory, dealer=dealer).last()
                marks = report.marks.all()
                point_marks = {}
                responsibles = {}
                comments = {}
                for mark in marks:
                    point_marks[mark.point] = mark.mark
                    if mark.responsible:
                        responsibles[mark.point] = mark.responsible
                    if mark.comment:
                        comments[mark.point] = mark.comment
                
                return render(request, 'checklist/report.html', {'user': user, 'dealer': dealer, 'category': category, 
                                       'subcategory': subcategory, 'points': points, 'childs': childs, 'point_marks': point_marks,
                                       'responsibles': responsibles, 'comments': comments})
            return render(request, 'checklist/report.html', {'user': user, 'dealer': dealer, 'category': category, 
                                   'subcategory': subcategory, 'points': points, 'childs': childs})
        else:
            return redirect('/logout/')
    else:
        return redirect('/logout/')
        

# Архив для регионального менеджера   
@login_required   
def archive(request):
    user = request.user
    group = CustomGroup.objects.filter(dealer=user).first().name
    dealers = CustomGroup.objects.filter(name=group).first().dealer.filter(groups__id=1).all()
    reports = Report.objects.filter(dealer=dealers).order_by('date')
    report_marks = []
    if reports:
        for report in reports:
            marks = report.marks.all()
            result = 0
            for mark in marks:
                result += mark.mark
            report_marks.append(int(result))
    r_marks = zip(reports, report_marks)
    return render(request, 'checklist/regmanager_archive.html', {'user': user, 'r_marks': r_marks})
    

# Подробный отчет для регионального менеджера  
@login_required
def report_view(request, report_id):
    user = request.user
    report = Report.objects.get(id=report_id)
    if user.is_dist() or report.dealer == user or report.performer == user:
        marks = report.marks.all()
        table = False
        for mark in marks:
            if mark.comment or mark.responsible:
                table = True
        return render(request, 'checklist/report_view.html', {'user': user, 'report': report, 'marks': marks, 'table': table})
    else:
        return redirect('/login/')
    

# Сравнение показателей дилеров для дистрибьютора
@login_required
def dealer_comparison(request):
    user = request.user
    if user.is_dist():
        dealers = User.objects.filter(groups__id=1).all()
        if request.method == 'POST':
            dealers_list1 = []
            post = request.POST.getlist('dealer[]')
            for x in post:
                if x != '0':
                    x = int(x)
                    dealers_list1.append(User.objects.get(id=x))
                else:
                    pass
            dealers_list = []
            for i in dealers_list1:
                if i not in dealers_list:
                    dealers_list.append(i)
                    
            
            categories = Category.objects.all().order_by('id')
            dealer_results = []
            dealer_cat_results = []
            dealer_sub_results = []
            
            for dealer in dealers_list:
                dicts = {}
                category_marks = {}
                subcategory_marks = {}
                for category in categories:
                    point_names = []
                    for subcategory in category.subcategories.all():
                        if Report.objects.filter(dealer=dealer, subcategory=subcategory).last():
                            report = Report.objects.filter(dealer=dealer, subcategory=subcategory).last()
                            for mark in report.marks.all():
                                dicts[mark.point.id] = mark.mark
                        else:
                            ps = subcategory.points.all()
                            for point in ps:
                                dicts[point.id] = -1
                        subcategory_marks[subcategory.name] = (get_subcategory_mark(subcategory.id, dealer.id))
                    
                    category_marks[category.name] = (get_category_mark2(category.id, dealer.id))
                dealer_results.append(dicts)
                dealer_cat_results.append(category_marks)
                dealer_sub_results.append(subcategory_marks)

                
                
            return render(request, 'checklist/dealer_comparison.html', {'dealers': dealers, 'user': user, 'dealers_list': dealers_list, 
                                                                            'dealer_results': dealer_results, 'categories': categories,
                                                                            'dealer_cat_results': dealer_cat_results, 'dealer_sub_results': dealer_sub_results})
        else:
            return render(request, 'checklist/dealer_comparison.html', {'dealers': dealers, 'user': user})
    else:
        return redirect('/login/')
    
    
# Общая информация о дилерах для дистрибьютора
@login_required  
def dealer_view(request):
    user = request.user
    if user.is_dist():
        dealers = User.objects.filter(groups__id=1).all()
        categories = Category.objects.all()
        dealer_category_marks = []
        dealer_report_marks = []
        chart_marks = []
        for dealer in dealers:
            category_marks = []
            for category in categories:
                category_marks.append(get_category_mark2(category.id, dealer.id))
            dealer_category_marks.append(category_marks)
            dealer_report_marks.append(get_report_mark(dealer.id))
        for category in categories:
            chart_marks.append(category_dealers_chart(category.id, user.id))
        info = zip(dealers, dealer_category_marks, dealer_report_marks)
        marks, monthes = dealers_chart(user.id)
        category_results = zip(categories, chart_marks)
        full_mark = get_full_mark()
        return render(request, 'checklist/dealer_view.html', {'user': user, 'info': info, 'categories': categories, 'full_mark': full_mark,
                                                              'monthes': monthes, 'marks': marks, 'category_results': category_results})
    else:
        return redirect('/login/')
        

            

# Информация о конкретном дилере для дистрибьютора
@login_required
def dist_dealer_view(request, dealer_id):
    user = request.user
    if user.is_dist():
        dealer = User.objects.get(id=dealer_id)
        
        categories = Category.objects.all().order_by('sequense_number')
        results = {}
        subs_results = {}
        cat_results = {}
        comments = {}
        responsible = {}
        report_res = []
        chart_marks = []
        for category in categories:
            sub_results = []
            for subcategory in category.subcategories.all():
                if Report.objects.filter(subcategory=subcategory, dealer=dealer).last():
                    report = Report.objects.filter(subcategory=subcategory, dealer=dealer).last()
                    marks = report.marks.all()
                    for mark in marks:
                        results[mark.point.id] = mark.mark
                        if mark.comment:
                            comments[mark.point.name] = mark.comment
                        if mark.responsible:
                            responsible[mark.point.name] = mark.responsible
                else:
                    for point in subcategory.points.all():
                        results[point.id] = -1
                subs_results[subcategory.name] = get_subcategory_mark(subcategory.id, dealer.id)
            cat_results[category.name] = get_category_mark2(category.id, dealer.id)
            report_res.append(get_category_mark2(category.id, dealer.id))
            chart_marks.append(category_chart(category.id, dealer.id))
        if ReportResult.objects.filter(dealer=dealer).last():
            date = ReportResult.objects.filter(dealer=dealer).last().date
        else:
            date = 0
        report_result = get_report_mark(dealer.id)
        marks, monthes = dealer_chart(dealer.id)
        if marks:
            category_lines = zip(categories, chart_marks)
        else:
            category_lines = []
        return render(request, 'checklist/dist_dealer_view.html', {'categories': categories, 'user': user, 'dealer': dealer, 'report_result': report_result, 'comments': comments, 'responsible': responsible,
                                                                   'results': results, 'subs_results': subs_results, 'cat_results': cat_results, 'marks': marks, 
                                                                   'monthes': monthes, 'category_lines': category_lines, 'date': date})
    elif user.is_regmanager():
        dealer = User.objects.get(id=dealer_id)
        if CustomGroup.objects.filter(dealer = user).last() == CustomGroup.objects.filter(dealer=dealer).last():
            categories = Category.objects.all()
            results = {}
            subs_results = {}
            cat_results = {}
            report_res = []
            for category in categories:
                sub_results = []
                for subcategory in category.subcategories.all():
                    
                    if Report.objects.filter(subcategory=subcategory, dealer=dealer).last():
                        report = Report.objects.filter(subcategory=subcategory, dealer=dealer).last()
                        marks = report.marks.all()
                        for mark in marks:
                            results[mark.point.name] = mark.mark
                    else:
                        for point in subcategory.points.all():
                            results[point.name] = -1
                    subs_results[subcategory.name] = get_subcategory_mark(subcategory.id, dealer.id)
                cat_results[category.name] = get_category_mark2(category.id, dealer.id)
                report_res.append(get_category_mark2(category.id, dealer.id))
            report_result = sum(report_res)
            return render(request, 'checklist/dist_dealer_view.html', {'categories': categories, 'user': user, 'dealer': dealer, 'report_result': report_result, 
                                                                       'results': results, 'subs_results': subs_results, 'cat_results': cat_results})
        else:
            return redirect('/login/')
   
    else:
        return redirect('/login/')
    
    
                                                               
# Общая статистика регионов
@login_required                                        
def region_info(request):
    user = request.user
    if user.is_dist():
        group_mid = []
        total_res = []
        data = []
        groups = CustomGroup.objects.filter(label='Регион').order_by('name').all()
        categories = Category.objects.all()
        if groups:
            for group in groups:
                category_middle = []
                for category in categories:
                    cat_marks = []
                    
                    for dealer in group.dealer.all():
                        mark = get_category_mark2(category.id, dealer.id)
                        if mark >= 0:
                            cat_marks.append(mark)
                    if len(cat_marks) > 0:
                        middle = round(sum(cat_marks)/(group.dealer.count()-1), 2)
                    else:
                        middle = -1
                    category_middle.append(middle)
                    
                group_mid.append(category_middle)
                total_res.append(get_region_mark(group.id))
            region_marks = zip(groups, group_mid, total_res)
        for group in groups:
            marks, monthes = regions_chart(group.id)
            data.append(marks)
        
        data1 = zip(groups, data)
        return render(request, 'checklist/region_info.html', {'user': user, 'region_marks': region_marks, 'categories': categories, 
                                                              'monthes': monthes, 'data1': data1})
        
    else:
        return redirect('/login/')
        
        
# Данные по региону
@login_required
def dist_region_view(request, region_id):
    user = request.user
    region = CustomGroup.objects.get(id=region_id)
    dealers = region.dealer.filter(groups__id=1).all()
    categories = Category.objects.all()
    dealer_category_marks = []
    dealer_report_marks = []
    if user.is_dist():
        for dealer in dealers:
            category_marks = []
            for category in categories:
                category_marks.append(get_category_mark2(category.id, dealer.id))
            dealer_category_marks.append(category_marks)
            dealer_report_marks.append(get_report_mark(dealer.id))
        info = zip(dealers, dealer_category_marks, dealer_report_marks)
        return render(request, 'checklist/dealer_view.html', {'user': user, 'info': info, 'categories': categories})
    else:
        return redirect('/login/')
        

# Данные о региональном менеджере для дистрибьютора
@login_required
def dist_regmanager_view(request, manager_id):
    user = request.user
    if user.is_dist():
        manager = User.objects.get(id=manager_id)
        region = CustomGroup.objects.filter(label='Регион', dealer=manager).last()
        r_marks = []
        if Report.objects.filter(performer=manager).last():
            reports = Report.objects.filter(performer=manager).all()
            for report in reports:
                report_marks = 0
                for mark in report.marks.all():
                    report_marks += mark.mark
                r_marks.append(int(report_marks))
            reports_marks = zip(reports, r_marks)
        else:
            reports_marks = 0
        return render(request, 'checklist/dist_regmanager_view.html', {'user': user, 'manager': manager, 'region': region, 'reports_marks': reports_marks})
    else:
        return redirect('/login/')
        
        
# Архив дистрибьютора
@login_required
def dist_archive(request):
    user = request.user
    if user.is_dist():
        reports = Report.objects.all().order_by('date')
        results = {}
        for report in reports:
            result = 0
            for mark in report.marks.all():
                result += mark.mark
            results[report.id] = int(result)
        return render(request, 'checklist/dist_archive.html', {'user': user, 'reports': reports, 'results': results})
    else:
        return redirect('/login/')
        
        
# дилер главная  
def dealer_reports(request):
    user = request.user
    if user.is_dealer():
        if Report.objects.filter(dealer=user).last():
            reports = Report.objects.filter(dealer=user).order_by('date').all()
            mrks = []
            table = False
            for report in reports:
                results = 0
                for mark in report.marks.all():
                    results += mark.mark
                    if mark.comment or mark.responsible:
                        table = True
                mrks.append(int(results))
            report_results = zip(reports, mrks)
            return render(request, 'checklist/dealer_reports.html', {'user': user, 'report_results': report_results, 'table': table})
        else:
            message = 'Отчеты отсутствуют'
            return render(request, 'checklist/dealer_reports.html', {'user': user, 'message': message})
    else:
        return redirect('/')


# Подробный отчет дилера
def dealer_report_view(request):
    user = request.user
    if user.is_dealer():
        if Report.objects.filter(dealer=user).last():
            date = Report.objects.filter(dealer=user).last().date
        else:
            date = 'Отчет отсутствует'
        categories = Category.objects.all()
        results = {}
        subs_results = {}
        cat_results = {}
        comments = {}
        responsible = {}
        report_res = []
        table = False
        for category in categories:
            sub_results = []
            for subcategory in category.subcategories.all():
                if Report.objects.filter(subcategory=subcategory, dealer=user).last():
                    report = Report.objects.filter(subcategory=subcategory, dealer=user).last()
                    marks = report.marks.all()
                    for mark in marks:
                        results[mark.point.name] = mark.mark
                        if mark.comment:
                            comments[mark.point.name] = mark.comment
                            table = True
                        if mark.responsible:
                            responsible[mark.point.name] = mark.responsible
                            table = True
                else:
                    for point in subcategory.points.all():
                        results[point.name] = -1
                subs_results[subcategory.name] = get_subcategory_mark(subcategory.id, user.id)
            cat_results[category.name] = get_category_mark2(category.id, user.id)
            report_res.append(get_category_mark2(category.id, user.id))
        report_result = get_report_mark(user.id)
        return render(request, 'checklist/dealer_reports.html', {'categories': categories, 'user': user, 'report_result': report_result, 'date': date, 'table': table,
                                                                 'results': results, 'subs_results': subs_results, 'cat_results': cat_results, 'comments': comments, 'responsible': responsible})
    else:
        return redirect('/')
        
        
# Архив дилера
def dealer_archive(request):
    user = request.user
    if user.is_dealer():
        reports = Report.objects.filter(dealer=user)
        if len(reports) > 0:
            results = {}
            for report in reports:
                result = 0
                for mark in report.marks.all():
                    result += mark.mark
                results[report.id] = int(result)
            return render(request, 'checklist/dealer_archive.html', {'user': user, 'reports': reports, 'results': results})
        else:
            return render(request, 'checklist/dealer_archive.html', {'user': user})
    else:
        return redirect('/login/')
        
        
def excel(request, dealer_id):
    import openpyxl
    from openpyxl.cell import get_column_letter
    from openpyxl.styles import colors, Font, Color, Fill
    dealer = User.objects.get(id=dealer_id)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=per4.xlsx'
    wb = openpyxl.Workbook()
    ws = wb.get_active_sheet()
    ws.title = "Отчет"
    categories = Category.objects.all()
    
    
    columns = [("", 15), (dealer.short_title, 100), ("SUM", 15), ("%", 15), ("Дата/Подтверждение", 50), ("Комментарии", 50), ("Ответственный", 50)]
    
    row_num = 0    
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]
    
    dealer = User.objects.get(id=dealer_id)
    mark = get_report_mark(dealer.id)
    if mark < 0:
        mark = 0
    
    columns = [("", 15), ("Общая оценка", 100), ("", 15), (mark, 15), ("", 50), ("", 50), ("", 50)]   
    row_num = 1
    for col_num in range(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        
        c.value = columns[col_num][0]
        c.style.font.bold = True
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]
        
    for category in categories:
        row_num += 1
        result = get_category_mark2(category.id, dealer.id)
        if result == -1:
            result = 0
        count_marks = 0    
        for sub in category.subcategories.all():
            if Report.objects.filter(subcategory=sub, dealer=dealer).last():
                report = Report.objects.filter(subcategory=sub, dealer=dealer).last()
                count_marks += report.marks.count()
            
        row = [category.sequense_number, category.name, count_marks, result, '', '', '']
        for col_num in range(len(row)):
            c = ws.cell(row=row_num + 1, column=col_num + 1)
            if row[col_num] == 'None':
                c.value = ''
            else:
                c.value = row[col_num]
            c.style.alignment.wrap_text = True
        for subcategory in category.subcategories.all():
            row_num += 1
            count_marks = 0
            s_result = get_subcategory_mark(subcategory.id, dealer.id)
            if s_result == -1:
                s_result = ''
            number = str(category.sequense_number) + '.' + str(subcategory.sequense_number)
            if Report.objects.filter(subcategory=subcategory, dealer=dealer).last():
                report = Report.objects.filter(subcategory=subcategory, dealer=dealer).last()
                count_marks += report.marks.count()
            row = [number, subcategory.name, count_marks, s_result, '', '', '']
            for col_num in range(len(row)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                c.font
                if row[col_num] == 'None':
                    c.value = ''
                else:
                    c.value = row[col_num]
                c.style.alignment.wrap_text = True
            for point in subcategory.points.all():
                row_num += 1
                if Report.objects.filter(dealer=dealer, subcategory=subcategory).last():
                    report = Report.objects.filter(dealer=dealer, subcategory=subcategory).last()
                    date = report.date
                    mark = report.marks.filter(point=point, report=report).last()
                    p_result = mark.mark
                    if mark.comment:
                        comment = str(mark.comment)
                    else:
                        comment = ''
                    if mark.responsible:
                        resp = mark.responsible.get_full_name()
                    else:
                        resp = ''
                else:
                    p_result = ''
                    date = ''
                    comment = ''
                    resp = ''
                p_number = str(category.sequense_number) + '.' + str(subcategory.sequense_number) + '.' + str(point.sequense_number)
                row = [p_number, point.name, '', p_result, date, comment, resp]
                for col_num in range(len(row)):
                    c = ws.cell(row=row_num + 1, column=col_num + 1)
                    if row[col_num] == 'None':
                        c.value = ''
                    else:
                        c.value = row[col_num]
                    c.style.alignment.wrap_text = True
        
    wb.save(response)
    return response