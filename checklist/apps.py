from django.apps import AppConfig


class ChecklistConfig(AppConfig):
    name = 'checklist'
    verbose_name = 'PER4'