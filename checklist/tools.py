from users.models import User
from .models import *
from users.models import CustomGroup
#from datetime import datetime, timedelta
import datetime
import calendar

def get_category_mark(category_id, dealer_id):
    dealer = User.objects.get(id=dealer_id)
    category = Category.objects.get(id=category_id)
    subcategories = category.subcategories.all()
    points = 0
    for subcategory in subcategories:
        points+=subcategory.points.count()
    result = 0
    data = ''
    if Report.objects.filter(dealer=dealer, subcategory=category.subcategories.all()):
        for subcategory in subcategories:
            if Report.objects.filter(dealer=dealer, subcategory=subcategory).last():
                marks = Report.objects.filter(dealer=dealer, subcategory=subcategory).last().marks.all()
                for mark in marks:
                    result += mark.mark
        date = str(Report.objects.filter(dealer=dealer, subcategory=category.subcategories.all()).last().date)
        if result == 0:
            category_result = 0
        else:
            category_result = round((result/points)*100, 2)
    else:
        category_result = -1
        date = '-'
    return [category_result, date]


def get_category_mark2(category_id, dealer_id):
    dealer = User.objects.get(id=dealer_id)
    category = Category.objects.get(id=category_id)
    subcategories = category.subcategories.all()
    points = 0
    for subcategory in subcategories:
        points+=subcategory.points.count()
    result = 0
    if Report.objects.filter(dealer=dealer, subcategory=subcategories):
        for subcategory in subcategories:
            if Report.objects.filter(dealer=dealer, subcategory=subcategory).last():
                marks = Report.objects.filter(dealer=dealer, subcategory=subcategory).last().marks.all()
                for mark in marks:
                    result += mark.mark
        if result == 0:
            category_result = 0
        else:
            category_result = round((result/points)*100, 2)
    else:
        category_result = -1
    return category_result
    
    
def get_subcategory_mark(subcategory_id, dealer_id):
    dealer = User.objects.get(id=dealer_id)
    subcategory = SubCategory.objects.get(id=subcategory_id)
    result = 0
    m_res = 0
    if Report.objects.filter(dealer=dealer, subcategory=subcategory).last():
        report = Report.objects.filter(dealer=dealer, subcategory=subcategory).last()
        marks = report.marks.all()
        for mark in marks:
            m_res += mark.mark
        if m_res > 0:
            result = round((m_res/marks.count()*100), 2)
        elif m_res == 0:
            result = 0
    else:
        result = -1
    return result
    
        
        
def get_report_mark2(dealer_id):
    dealer = User.objects.get(id=dealer_id)
    categories = Category.objects.all()
    marks = []
    for category in categories:
        mark = get_category_mark2(category.id, dealer.id)
        #mark = CategoryResult.objects.filter(dealer=dealer, category=category).last()
        if mark:
            marks.append(mark)
        else:
            marks.append(-1)
    if sum(marks) >= 0:    
        result = round((sum(marks)/categories.count()), 2)
    elif sum(marks) > -(categories.count()) and sum(marks) < 0:
        result = 0
    else:
        result = -1
    return result
    
    
def get_report_mark(dealer_id):
    dealer = User.objects.get(id=dealer_id)
    categories = Category.objects.all()
    marks = []
    for cat in categories:
        try:
            marks.append(CategoryResult.objects.filter(dealer=dealer, category=cat).last().mark)
        except:
            pass
        
    if marks:
        result = round((sum(marks)/categories.count()), 2)
    else:
        result = -1
    return result
    
    
    
def get_region_mark(group_id):
    group = CustomGroup.objects.get(id=group_id)
    dealers = group.dealer.all()
    categories = Category.objects.all()
    category_results = []
    result = []
    
    for dealer in dealers:
        mark = get_report_mark(dealer.id)
        if mark >= 0:
            result.append(mark)
    try:
        region_mark = round((sum(result)/len(result)),2)
    except:
        region_mark = -1
        
    return region_mark
                    

def get_full_mark():
    dealers = User.objects.filter(groups__id=1).all()
    result = []
    
    for dealer in dealers:
        mark = get_report_mark(dealer.id)
        
        if mark >= 0:
            result.append(mark)
    try:
        full_mark = round((sum(result)/len(result)),2)
    except:
        full_mark = -1
    
    return full_mark
    
                    
def dealer_chart(dealer_id):
    now = datetime.date.today()
    month = now.month + 1
    year = now.year
    marks = []
    monthes = []
    for x in range(0, 12):
        month = month-1
        
        if month <= 0:
            month = 12
            year=year-1
            
            
        if month == 1:
            m = "Январь"
        elif month == 2:
            m = "Февраль"
        elif month == 3:
            m = 'Март'
        elif month == 4:
            m = 'Апрель'
        elif month == 5:
            m = 'Май'
        elif month == 6:
            m = 'Июнь'
        elif month == 7:
            m = 'Июль'
        elif month == 8:
            m = 'Август'
        elif month == 9:
            m = 'Сентябрь'
        elif month == 10:
            m = 'Октябрь'
        elif month == 11:
            m = 'Ноябрь'
        else:
            m = 'Декабрь'
        y = year
        date = m + ', ' + str(y)
        report = ReportResult.objects.filter(dealer__id=dealer_id, date__lt=datetime.date(year, month, calendar.monthrange(year, month)[1])).last()
        
        
        try:
            marks.append(report.mark)
           
            monthes.append(date)
        except:
            pass
    monthes.reverse()
    marks.reverse()
    return marks, monthes
    
    

def dealers_chart(dist_id):
    dist = User.objects.get(id=dist_id)
    dealers = dist.childs.all()
    now = datetime.date.today()
    last_date = datetime.date(now.year-1, now.month, (calendar.monthrange(now.year-1, now.month)[0] + 1))
    month = now.month + 1
    year = now.year
    monthes = []
    marks = []
    for x in range(0, 12):
        month = month-1
        
        if month <= 0:
            month = 12
            year=year-1
            
        if month == 1:
            m = "Январь"
        elif month == 2:
            m = "Февраль"
        elif month == 3:
            m = 'Март'
        elif month == 4:
            m = 'Апрель'
        elif month == 5:
            m = 'Май'
        elif month == 6:
            m = 'Июнь'
        elif month == 7:
            m = 'Июль'
        elif month == 8:
            m = 'Август'
        elif month == 9:
            m = 'Сентябрь'
        elif month == 10:
            m = 'Октябрь'
        elif month == 11:
            m = 'Ноябрь'
        else:
            m = 'Декабрь'  
            
        y = year
        date = m + ', ' + str(y)
        reports = []
        for dealer in dealers:
            report = ReportResult.objects.filter(dealer=dealer, date__lt=datetime.date(year, month, calendar.monthrange(year, month)[1])).last()
            if report:
                reports.append(report)
        
        if len(reports):
            monthes.append(date)
            mark = 0
            for rep in reports:
                mark+=rep.mark
            mark = round((mark/dealers.count()),2)
            marks.append(mark)
    marks.reverse()
    monthes.reverse()
    return marks, monthes
                
            
def regions_chart(region_id):
    region = CustomGroup.objects.get(id=region_id)
    dealers = region.dealer.filter(groups__id=1).all()
    now = datetime.date.today()
    month = now.month + 1
    year = now.year
    monthes = []
    marks = []
    
    for x in range(0, 12):
        month = month-1
        
        if month <= 0:
            month = 12
            year=year-1
        if month == 1:
            m = "Январь"
        elif month == 2:
            m = "Февраль"
        elif month == 3:
            m = 'Март'
        elif month == 4:
            m = 'Апрель'
        elif month == 5:
            m = 'Май'
        elif month == 6:
            m = 'Июнь'
        elif month == 7:
            m = 'Июль'
        elif month == 8:
            m = 'Август'
        elif month == 9:
            m = 'Сентябрь'
        elif month == 10:
            m = 'Октябрь'
        elif month == 11:
            m = 'Ноябрь'
        else:
            m = 'Декабрь'  
            
        y = year
        date = m + ', ' + str(y)
        reports = []
        for dealer in dealers:
            report = ReportResult.objects.filter(dealer=dealer, date__lt=datetime.date(year, month, calendar.monthrange(year, month)[1])).last()
            if report:
                reports.append(report)
        
        if len(reports):
            monthes.append(date)
            mark = 0
            for rep in reports:
                mark+=rep.mark
            mark = round(mark/dealers.count(), 2)
            marks.append(mark)
        else:
            monthes.append(date)
            marks.append('')
    marks.reverse()
    monthes.reverse()
    return marks, monthes
    
    
def category_chart(category_id, dealer_id):
    category = Category.objects.get(id=category_id)
    now = datetime.date.today()
    month = now.month + 1
    year = now.year
    marks = []
    for x in range(0, 12):
        month = month-1
        
        if month <= 0:
            month = 12
            year=year-1
            
        report = CategoryResult.objects.filter(category=category, dealer__id=dealer_id, date__lt=datetime.date(year, month, calendar.monthrange(year, month)[1])).last()
        try:
            marks.append(report.mark)
        except:
            pass
    marks.reverse()
    return marks


def category_dealers_chart(category_id, dist_id):
    category = Category.objects.get(id=category_id)
    dist = User.objects.get(id=dist_id)
    dealers = dist.childs.all()
    now = datetime.date.today()
    month = now.month + 1
    year = now.year
    marks = []
    for x in range(0, 12):
        month = month-1
        if month <= 0:
            month = 12
            year=year-1
        reports = []
        for dealer in dealers:
            report = CategoryResult.objects.filter(category=category, dealer=dealer, date__lt=datetime.date(year, month, calendar.monthrange(year, month)[1])).last()
            if report:
                reports.append(report)
        if len(reports):
            mark = 0
            for rep in reports:
                mark+=rep.mark
            mark = round((mark/dealers.count()),2)
            marks.append(mark)
    marks.reverse()
    return marks
    
    
def category_reg_chart(region_id, category_id):
    region = CustomGroup.objects.get(id=region_id)
    category = Category.objects.get(id=category_id)
    dealers = region.dealer.filter(groups__id=1).all()
    now = datetime.date.today()
    month = now.month + 1
    year = now.year
    marks = []
    for x in range(0, 12):
        month = month-1
        if month <= 0:
            month = 12
            year=year-1
        reports = []
        for dealer in dealers:
            report = CategoryResult.objects.filter(category=category, dealer=dealer, date__lt=datetime.date(year, month, calendar.monthrange(year, month)[1])).last()
            if report:
                reports.append(report)
        if len(reports):
            mark = 0
            for rep in reports:
                mark+=rep.mark
            mark = round((mark/dealers.count()),2)
            marks.append(mark)
        else:
            marks.append('')
    marks.reverse()
    return marks