from django.db import models
from ckeditor.fields import RichTextField


class Category(models.Model):
    name = models.CharField(max_length=50, verbose_name='Название категории')
    sequense_number = models.IntegerField(verbose_name='Порядковый номер', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        

class SubCategory(models.Model):
    name = models.CharField(max_length=50, verbose_name='Название подкатегории')
    category = models.ForeignKey(Category, related_name='subcategories', verbose_name='Категория')
    sequense_number = models.IntegerField(verbose_name='Порядковый номер', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'


class Point(models.Model):
    name = models.CharField(max_length=500, verbose_name='Название пункта')
    subcategory = models.ForeignKey(SubCategory, related_name='points', verbose_name='Подкатегория')
    weight = models.FloatField(default=1, verbose_name='Вес пункта')
    sequense_number = models.IntegerField(verbose_name='Порядковый номер', blank=True, null=True)
    comment = RichTextField(verbose_name='Комментарий', blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Пункт'
        verbose_name_plural = 'Пункты'
        
        
class Report(models.Model):
    subcategory = models.ForeignKey(SubCategory, related_name='reports', verbose_name='Подкатегория')
    dealer = models.ForeignKey('users.User', related_name='reports', verbose_name='Дилер')
    date = models.DateField(auto_now=True)
    performer = models.ForeignKey('users.User', verbose_name='Исполнитель')

    def __str__(self):
        return 'Отчет по ' + self.subcategory.name

    class Meta:
        verbose_name = 'Отчет'
        verbose_name_plural = 'Отчеты'


class Mark(models.Model):
    point = models.ForeignKey(Point, verbose_name='Пункт')
    report = models.ForeignKey(Report, related_name='marks', verbose_name='Отчет')
    checked = models.BooleanField(verbose_name='Отмечен?')
    mark = models.FloatField(verbose_name='Оценка')
    comment = models.TextField(null=True, blank=True, verbose_name='Комментарий')
    responsible = models.ForeignKey('users.User', verbose_name='Ответственный', blank=True, null=True)

    def __str__(self):
        return self.point.name + ' ' + self.point.subcategory.name

    class Meta:
        verbose_name = 'Оценка'
        verbose_name_plural = 'Оценки'

    
class ReportResult(models.Model):
    dealer = models.ForeignKey('users.User', verbose_name='Дилер')
    date = models.DateField()
    mark = models.FloatField()

    class Meta:
        verbose_name = 'Результат по отчету'
        verbose_name_plural = 'Результаты по отчетам'
    

class CategoryResult(models.Model):
    category = models.ForeignKey(Category, verbose_name='Категория')
    dealer = models.ForeignKey('users.User', verbose_name='Дилер')
    date = models.DateField()
    mark = models.FloatField()

    class Meta:
        verbose_name = 'Результат по категории'
        verbose_name_plural = 'Результаты по категорям'
