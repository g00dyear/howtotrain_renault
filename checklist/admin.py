from django.contrib import admin
from .models import *


class CategoryInline(admin.TabularInline):
    model = SubCategory


class CategoryView(admin.ModelAdmin):
    inlines = [CategoryInline]


class PointInline(admin.StackedInline):
    model = Point

class SubCategoryView(admin.ModelAdmin):
    list_display = ('name', 'category')
    list_filter = ('category',)
    inlines = [PointInline]
    
    
admin.site.register(Category, CategoryView)
admin.site.register(SubCategory, SubCategoryView)
admin.site.register(Point)