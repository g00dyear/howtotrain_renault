from django.db import models
from django.core.mail import send_mail, EmailMultiAlternatives
from users.models import User
from ckeditor.fields import RichTextField
from .message_templates import *
# Create your models here.


class MailFrom(models.Model):
    mail_from = models.CharField(max_length=200)

    def __str__(self):
        return self.mail_from


class MailCamp(models.Model):
    users = models.ManyToManyField(User, null=True, blank=True)
    emails_to = models.TextField(null=True, blank=True)
    mail_from = models.ForeignKey(MailFrom)
    subject = models.CharField(max_length=200)
    text = RichTextField()
    send = models.BooleanField(default=True)
    def save(self, *args, **kwargs):
        if self.send:
            self.send=False
            mails = self.emails_to.split(',')
            for mail in mails:
                msg = EmailMultiAlternatives(self.subject, self.text, self.mail_from.mail_from, [mail.strip()])
                msg.attach_alternative(self.text, "text/html")
                msg.send()
        super(MailCamp, self).save(*args, **kwargs) # Call the "real" save() method.
        

class MailCamp2(models.Model):
    users = models.ManyToManyField(User, null=True, blank=True)
    emails_to = models.TextField(null=True, blank=True)
    mail_from = models.ForeignKey(MailFrom)
    subject = models.CharField(max_length=200)
    text = RichTextField()
    send = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if self.send:
            self.send=False
            mails = self.emails_to.split(',')
            for mail in mails:
                msg = EmailMultiAlternatives(self.subject, self.text, self.mail_from.mail_from, [mail.strip()])
                msg.attach_alternative(self.text, "text/html")
                msg.send()
        super(MailCamp2, self).save(*args, **kwargs)  # Call the "real" save() method.
        

class Messages(models.Model):
    # Invitation
    elearning_inv_subject = models.TextField(default=elearning_invitation_subject)
    elearning_inv_text = RichTextField(default=elearning_invitation_text)
    test_inv_subject = models.TextField(default=test_invitation_subject)
    test_inv_text = RichTextField(default=test_invitation_text)
    training_inv_subject = models.TextField(default=training_invitation_subject)
    training_inv_text = RichTextField(default=training_invitation_text)

    # Start
    elearning_st_subject = models.TextField(default=elearning_start_subject)
    elearning_st_text = RichTextField(default=elearning_start_text)
    test_st_subject = models.TextField(default=test_start_subject)
    test_st_text = RichTextField(default=test_start_text)
    training_st_subject = models.TextField(default=training_start_subject)
    training_st_text = RichTextField(default=training_start_text)

    # Finish
    elearning_fin_subject = models.TextField(default=elearning_finish_subject)
    elearning_fin_text = RichTextField(default=elearning_finish_text)
    test_fin_subject = models.TextField(default=test_finish_subject)
    test_fin_text = RichTextField(default=test_finish_text)
    training_fin_subject = models.TextField(default=training_finish_subject)
    training_fin_text = RichTextField(default=training_finish_text)

    # Success passing
    elearning_suc_subject = models.TextField(default=elearning_success_subject)
    elearning_suc_text = RichTextField(default=elearning_success_text)
    test_suc_subject = models.TextField(default=test_success_subject)
    test_suc_text = RichTextField(default=test_success_text)
    training_suc_subject = models.TextField(default=training_success_subject)
    training_suc_text = RichTextField(default=training_success_text)

    # Unsuccess passing
    elearning_unsuc_subject = models.TextField(default=elearning_unsuccess_subject)
    elearning_unsuc_text = RichTextField(default=elearning_unsuccess_text)
    test_unsuc_subject = models.TextField(default=test_unsuccess_subject)
    test_unsuc_text = RichTextField(default=test_unsuccess_text)
    training_unsuc_subject = models.TextField(default=training_unsuccess_subject)
    training_unsuc_text = RichTextField(default=training_unsuccess_text)

    # Reminders
    elearning_rem_subject = models.TextField(default=elearning_remember_subject)
    elearning_rem_text = RichTextField(default=elearning_remember_text)
    test_rem_subject = models.TextField(default=test_remember_subject)
    test_rem_text = RichTextField(default=test_remember_text)
    training_rem_subject = models.TextField(default=training_remember_subject)
    training_rem_text = RichTextField(default=training_remember_text)


class MessageEvent(models.Model):
    date = models.DateTimeField()
    sent = models.BooleanField(default=False)
    test = models.ForeignKey('etest.PlannedTest', null=True, blank=True)
    elearning = models.ForeignKey('elearning.PlannedCourse', null=True, blank=True)
    training = models.ForeignKey('trainings.TrainingDate', null=True, blank=True)
    course = models.ForeignKey('newcourse.StudyCourse', null=True, blank=True)
    event_type = models.CharField(max_length=100)

    def get_elem(self):
        if self.elearning:
            return self.elearning

        if self.training:
            return self.training

        if self.course:
            return self.course

        return self.test

    @classmethod
    def not_sent(cls):
        return MessageEvent.objects.filter(sent=False).all()


class MessageEventNew(models.Model):
    date = models.DateTimeField()
    sent = models.BooleanField(default=False)
    test = models.ForeignKey('etest.PlannedTest', null=True, blank=True)
    elearning = models.ForeignKey('elearning.PlannedCourse', null=True, blank=True)
    training_date = models.ForeignKey('trainings.TrainingDate', null=True, blank=True)
    course = models.ForeignKey('newcourse.StudyCourse', null=True, blank=True)
    event_type = models.CharField(max_length=100)

    def get_elem(self):
        if self.elearning:
            return self.elearning

        if self.training:
            return self.training

        if self.course:
            return self.course

        return self.test

    @classmethod
    def not_sent(cls):
        return MessageEvent.objects.filter(sent=False).all()


class SentMessage(models.Model):
    event = models.ForeignKey(MessageEvent)
    user = models.ForeignKey('users.User')
    event_type = models.CharField(max_length=100)


class SentMessageNew(models.Model):
    event = models.ForeignKey(MessageEventNew)
    user = models.ForeignKey('users.User')
    event_type = models.CharField(max_length=100)
