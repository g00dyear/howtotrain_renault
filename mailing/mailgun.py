import requests
import html2text


def send_simple_message(text, subject, mail):
    return requests.post(
        "https://api.mailgun.net/v3/kpi-ua.com/messages",
        auth=("api", "key-decd69da86514e92762fd6d547038049"),
        data={"from": "Renault <renault-support@kpi-ua.com>",
              "to": ["team@kpi-ua.com"],
              "subject": subject,
              "h:Reply-To": mail,
              "text": text})


def send_message(text, subject, mail):
    return requests.post(
        "https://api.mailgun.net/v3/kpi-ua.com/messages",
        auth=("api", "key-decd69da86514e92762fd6d547038049"),
        data={"from": "Renault <renault@kpi-ua.com>",
              "to": mail,
              "subject": subject,
              "text": html2text.html2text(text),
              "html": text})


def send_simple_message2(text, subject, user):
    return requests.post(
        "https://api.mailgun.net/v3/kpi-ua.com/messages",
        auth=("api", "key-decd69da86514e92762fd6d547038049"),
        data={"from": "Hyundai <hyundai@kpi-ua.com>",
              "to": [user],
              "subject": subject,
              "html": text,
              "text": html2text.html2text(text)})


def send_simple_message3(text, subject, user):
    return requests.post(
        "https://api.mailgun.net/v3/kpi-ua.com/messages",
        auth=("api", "key-decd69da86514e92762fd6d547038049"),
        data={"from": "Renault <team@kpi-ua.com>",
              "to": [user],
              "subject": subject,
              "html": text,
              "text": html2text.html2text(text)})

text = """<div>Здравствуйте!</div>
<div>
<div dir="ltr">
<div>&nbsp;</div>
<p>Это письмо-приглашение для прохождения&nbsp;<strong>Онлайн тестирования механиков Hyundai.</strong></p>
<p>Пройти тест можно с любого компьютера, в том числе и дома.</p>
<div>&nbsp;</div>
<div>Для прохождения теста, перейдите, пожалуйста, по ссылке:</div>
<div>&nbsp;</div>
<p><a href="/kpi-ua.com">kpi-ua.com</a>&nbsp;&nbsp; &nbsp;введите полученный код (см. ниже) и следуйте дальнейшим инструкциям.</p>
<div>&nbsp;</div>
<p>ВНИМАНИЕ: полученный код даёт право на 1 попытку прохождения тестирования!</p>
<div>&nbsp;</div>
<p>Ваш код для прохождения теста:</p>
<div>&nbsp;</div>
<div>&nbsp;{invitationCodes}</div>
<div>&nbsp;</div>
<p>Обратите внимание: тест состоит из 30 вопросов</p>
<div>В тесте предусмотрен моновыбор ответов: правильным может быть только 1 вариант ответа.</div>
<div>
<p>На прохождение теста вам дано 60 минут. Счетчик времени находится в верхней правой части экрана.</p>
<div>Даты проведения тестирования - 25-29.04.2016&nbsp;</div>
</div>
<p>В случае возникновения каких-либо технических сложностей с прохождением тестирования, пишите по адресу&nbsp;<span>hyundai</span><span>@</span><span><a href="/">kpi-ua.com</a></span></p>
</div>
</div>"""

subject = 'Тестирование механиков Hyundai'

def send_old_mails():
    codes = ["DA7E197C82DAFA116F5FD1B9E654681CF21A28F","DA7E1973A5F8682479B6190513FF99214A02179","DA7E19741C26854BEF2D62F01DB85C3D2152A98","DA7E19753F36449CA2FA525BDE2009ED4D3F329","DA7E19789B8E000BEA46666D123ABF6A4234D0A","DA7E197FBB7C9C5348068D863F0DD7FA8386457","DA7E197E5875E9A957975D7FD8F1B5A39A7BC08","DA7E197E1DA7D568F550D58144F9564750D075D","DA7E197AE39F9F57A0A6E1507F60218CA093ADD","DA7E197258C6A553FC00F7A9858DB61B7799735","DA7E1978BF4E8FA0DD8DBC165D2694357176CB5","DA7E197910917FA66C48D82E1B29579AC1C93D4","DA7E19712E28E136485E783EE700061667BF3F7","DA7E197EFB0CF1503A7A057BC1A51948DF81E6D","DA7E1977538E6E06507D08B9C2FB726866A97A7","DA7E197187EF6C2FB017893075E79B609A97AB5","DA7E1975FBC7737CD75A440E11DD878B4F45505","DA7E197CB52A1CBA72D9208AC7F76A1A6CE1A62","DA7E197EF09047761C480056E0AF876EA8F6997","DA7E19726ABAEE37FA4CAE8EEA68335839DBD5F","DA7E1971A57CDBE48C51CA05CA4ACE89ED4EC86","DA7E1971C9566E96A0F737CB56D9DD1809A6DA3","DA7E19740DE33CB635CB8D23B83FE9B53B5E408","DA7E197B0802A6B8413BEDAAC578170A3846ABF","DA7E1975DFC80F86BBC27EE4A4A9F918BC7DEA6","DA7E197D34B9C8285B65D7C653AC0AFEFD7B1A6","DA7E19722B8D4159ED5B175E4042DF82BE4D5F3","DA7E197DD07CED8B9D6A2A51C6B9A9FB6FB391C","DA7E1970CCA808C02812B7AA52C6E1704B9C77B","DA7E197040E8D128590539EBF2BEC258FC0B277","DA7E1975BC58664D5DBCA866655B00F31301A14","DA7E1978DA57BAABD682B8A35DCDE8E61A0657B","DA7E1972C39CB2116094F9960147E4D8A475435","DA7E1975E8B4A4794CC37F05075BB4A2E9C615D","DA7E197DB1BC9C1AE9C1F85BC51704C6C23C62A","DA7E197CBAEB1AC4EE4D8D966485005AA22FEEA","DA7E19798618C8F4441A1021A996D120243D62C","DA7E197565F31D07511D78EB4CBDC95378A0272","DA7E19763210DD4F3214FAFE5C225769415CD4D","DA7E197B6980B8EEA398A79ED2A05F30D608ABD","DA7E1978A58E6AAB142F4B94EC886CE8CB275C2","DA7E1978603A35807DC1E1EEE01FFE312C23A08","DA7E197EDDCA8620BC64F8589CB9B473115B3BC","DA7E197DA5863DC55FFE705A6DE7DEB086C47BC","DA7E197CB8705E26A790108C2E97FE17ADE1243","DA7E1970C1B7021025B74DF00AC50BA80DAADD4","DA7E1971CD013B2CF010A1825CA041EC0D586E0","DA7E19754F47F9719427DE8530B0D3C29F13176","DA7E1972C7592D6AC0FAA7A1E33C394A76BD5DF","DA7E1970B95F2CD04866C8526D6D4B1F1F94F6C","DA7E197A19C97FFA2CBC19E7D4962D2BF4EDACB","DA7E19749E83D7F8952D3DFB4AAB31F42E14BD3","DA7E1970A395E39ADEED2EDFE19917D4A4507F5","DA7E1970B0038160539C98ECF72E0411EE29F05","DA7E197FB6B6B10527932FA7D5F0EB02798001D","DA7E19714BD6B1DF0F9B0675AA7B0B8A9D83BB4","DA7E1973176F97AF116B9B1BF54EF2ABCC2D1FE","DA7E197DDF68ECAC1D7ACD250C98DC3155AE97F","DA7E197E864A0D369A6BD917F8A5F4C36ED92E0","DA7E1975D95720A1B5A19E833CE1F8A3543D6C2","DA7E197C42490E20ED27A71FC2E82F2846D7F47","DA7E19739CB0804CA582675AB074155CC995BE9","DA7E197D55EAC4EAF5AC2AE85860417E0BAA892","DA7E1978C7F06578F095A8C033AA866E8E02388","DA7E19704EDFD6CA7C78AB426F3465FBEC56877","DA7E197A483FA9023DD9A82F6673427D7FCE0EE","DA7E19717925A49F5A3F6A13ACC2B92A48ACCA5","DA7E197411BC6E963B58E61F420C81429411C32","DA7E197B47587647EFDB8B1598760A29C3B1F10","DA7E19758B5EDD511F55941E1694028E5688344"]

    users = ["hyundai@mail.zp.ua","z_masha83@mail.ru","kapital-avto-service@ladaukraine.net","autosoyuz_zt@ukr.net","autosoyuz_zt@ukr.net","lesha0131@mail.ru","berchunn@mail.ru","leonid.gurov.00@mail.ru","babaybarbitura@gmail.com","mukola.babiy@bogdanauto.com.ua"," v.makabula@aelita.ua","dmitriy.reznichenko@bogdanauto.com.ua","oleh.minieiev@bogdanauto.com.ua","artur.khyzhniak@bogdanauto.com.ua","slazd@mail.ru","dnavtc@gmail.com"," komimaks@yandex.ru ","a.serebrennikov@hyundai.pl.ua","a.serebrennikov@hyundai.pl.ua","a.serebrennikov@hyundai.pl.ua","a.serebrennikov@hyundai.pl.ua","a.serebrennikov@hyundai.pl.ua","a.serebrennikov@hyundai.pl.ua","a.serebrennikov@hyundai.pl.ua","ihor.hural@bogdanauto.com.ua","ihor.hural@bogdanauto.com.ua","ihor.hural@bogdanauto.com.ua","igor.kulik@bogdanauto.com.ua","igor.kulik@bogdanauto.com.ua","igor.kulik@bogdanauto.com.ua","vladimir.butniak@bogdanauto.com.ua","vladimir.butniak@bogdanauto.com.ua","kramarovskaya@autoplaneta.com.ua","kramarovskaya@autoplaneta.com.ua","kramarovskaya@autoplaneta.com.ua","kramarovskaya@autoplaneta.com.ua","pavelbaluk83@gmail.com","foresterigor@ukr.net","ehorklimavichus@gmail.com","ihirschukin245@gmail.com","garant@buhavto.vin.ua","garant@buhavto.vin.ua","garant@buhavto.vin.ua","BVP1955@i.ua","past18@ukr.net","SVM1951@i.ua","tuseyko@rambler.ru","ro-many@mail.ru","Krasnogor@i.ua","yulkis@ukr.net","xnechitailox@yandex.ru","TVL1958@i.ua","belichanek@ukr.net","KOP71@i.ua","vovamelnik50@ukr.net","kravchukv@yandex.ru","7gg7@ukr.net","alekseenkaaleksey@gmail.com","vladvn@rambler.ru","furmanura@ukr.net"]

    test_users = ['svshostak@gmail.com', 'svshostak+h@gmail.com']

    for user in users:
        code = codes.pop()
        newtext = text.replace('{invitationCodes}', code)
        send_simple_message2(newtext, subject, user)
        print(user, code)


