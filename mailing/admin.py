from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(MailCamp)
admin.site.register(MailFrom)
admin.site.register(MessageEventNew)
admin.site.register(SentMessageNew)
