elearning_invitation_subject = 'Запрошення на дистанційне навчання: %ELEARNINGNAME%, %AUDITORY%. %datestart% - %dateend%.'

elearning_invitation_text = """
<p>Шановний %USERNAME%,</p>

<p>Вас запрошено взяти участь в дистанційному навчанні.</p>

<ul>
	<li>
		<p>Назва навчання: %ELEARNINGNAME%</p>
	</li>
	<li>
		<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
		<p>Початок відкриття доступу: %datestart%</p>
	</li>
	<li>
		<p>Завершення доступу: %dateend%</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>Для доступу до онлайн курсу скористайтесь наступною URL-адресою: <a href="http://renault.kpi-ua.com/">http://renault.kpi-ua.com</a> .</p>

<p>&nbsp;</p>

<p>Бажаємо успішного навчання.</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

test_invitation_subject = 'Запрошення на онлайн тестування: %TESTNAME%. %AUDITORY%. %datestart% - %dateend%.'

test_invitation_text = """
<p>Шановний %USERNAME%,</p>

<p>Вас запрошено взяти участь в онлайн тестуванні.</p>

<ul>
	<li>
		<p>Назва тестування: %TESTNAME%</p>
	</li>
	<li>
		<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
		<p>Початок відкриття доступу: %datestart%</p>
	</li>
	<li>
		<p>Завершення доступу: %dateend%</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>Для доступу до онлайн тестування скористайтесь наступною URL-адресою: <a href="http://renault.kpi-ua.com/">http://renault.kpi-ua.com</a> .</p>

<p>&nbsp;</p>

<p>Кількість питань: %question_count%</p>

<p>Максимальний час відведений для відповіді на кожне питання %max_time_for_question%.</p>

<p>Мінімально допустимий бал: 70</p>

<p>&nbsp;</p>

<p>Бажаємо успішного тестування.</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>

"""

training_invitation_subject = 'Запрошення на очний курс: %TRAININGNAME%. %AUDITORY%. %datestart%. %event_duration%. %training_place%.'

training_invitation_text = """
<p>Шановний %USERNAME%,</p>

<p>Вас запрошено взяти участь в очному навчанні.</p>

<ul>
	<li>
	<p>Назва навчання: %TRAININGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок навчання: %datestart%</p>
	</li>
	<li>
	<p>Тривалість навчання: %event_duration%</p>
	</li>
	<li>
	<p>Місце проведення очного навчання: %training_place%.</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>Чекаємо Вас на нашому очному навчанні.</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

test_start_subject = 'Запрошення пройти дистанційну атестацію. %AUDITORY%. %datestart% - %dateend% .'

test_start_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про проведення дистанційної атестації персоналу.</p>

<ul>
	<li>
		<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
		<p>Початок відкриття доступу: %datestart%</p>
	</li>
	<li>
		<p>Завершення доступу: %dateend%</p>
	</li>
</ul>

<p>Для доступу до дистанційної атестації скористайтесь наступною URL-адресою:</p>

<p><a href="http://renault.kpi-ua.com/">http://renault.kpi-ua.com</a> .</p>

<p>&nbsp;</p>

<p>Бажаємо успішної атестації.</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>

"""

test_finish_subject = 'Завершення дистанційної атестації. %AUDITORY%. %datestart% - %dateend% .'

test_finish_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про завершення дистанційної атестації персоналу.</p>

<ul>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок відкриття доступу: %datestart%</p>
	</li>
	<li>
	<p>Завершення доступу: %dateend%</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

elearning_start_subject = 'Початок дистанційного навчання: %ELEARNINGNAME%, %AUDITORY%. %datestart% - %dateend%. '

elearning_start_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про початок дистанційного навчання.</p>

<ul>
	<li>
	<p>Назва навчання: %ELEARNINGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок відкриття доступу: %datestart%</p>
	</li>
	<li>
	<p>Завершення доступу: %dateend%</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>Для доступу до онлайн курсу скористайтесь наступною URL-адресою: <a href="http://renault.kpi-ua.com/">http://renault.kpi-ua.com</a> .</p>

<p>&nbsp;</p>

<p>Бажаємо успішного навчання.</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

elearning_finish_subject = 'Завершення дистанційного навчання: %ELEARNINGNAME%, %AUDITORY%. %datestart% - %dateend%.'

elearning_finish_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про завершення дистанційного навчання.</p>

<ul>
	<li>
	<p>Назва навчання: %ELEARNINGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок відкриття доступу: %datestart%</p>
	</li>
	<li>
	<p>Завершення доступу: %dateend%</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

training_start_subject = 'Очний курс: %TRAININGNAME%, %AUDITORY%. Початок навчання: %datestart%. %event_duration%. %training_place%.'

training_start_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про проведення очного навчання.</p>

<ul>
	<li>
	<p>Назва навчання: %TRAININGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок навчання: %datestart%</p>
	</li>
	<li>
	<p>Тривалість навчання: %event_duration%</p>
	</li>
	<li>
	<p>Місце проведення очного навчання: %training_place%.</p>
	</li>
</ul>

<p>Якщо цей очний курс є продовженням дистанційного навчання, кожен учасник записується в групу очного навчання самостійно, скориставшись опцією по завершенню онлайн курсу.</p>

<p>Якщо ж очний курс не передбачає попереднього дистанційного навчання, то для участі Вас в цьому очному курсі Ваше керівництво має надіслати заявку за встановленою формою.</p>

<p>&nbsp;</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

training_finish_subject = 'Завершення очного курсу: %TRAININGNAME%, %AUDITORY%. Початок навчання: %datestart%. %event_duration%. %training_place%.'

training_finish_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про завершення очного навчання.</p>

<ul>
	<li>
	<p>Назва навчання: %TRAININGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок навчання: %datestart%</p>
	</li>
	<li>
	<p>Тривалість навчання: %event_duration%</p>
	</li>
	<li>
	<p>Місце проведення очного навчання: %training_place%.</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

test_success_subject = 'Результат атестації. %AUDITORY%. %datestart% - %dateend% .'

test_success_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про те, що Ви успішно пройшли атестацію.</p>

<ul>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Дати проведення атестації: %datestart% - %dateend%</p>
	</li>
	<li>
	<p>Мінімальний бал для успішної атестації: 70</p>
	</li>
	<li>
	<p>Кількість набраних балів: %result%</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>Вітаємо!</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

test_unsuccess_subject = 'Результат атестації. %AUDITORY%. %datestart% - %dateend% .'

test_unsuccess_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про те, що, на превеликий жаль, Ви не пройшли атестацію.</p>

<ul>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Дати проведення атестації: %datestart% - %dateend%</p>
	</li>
	<li>
	<p>Мінімальний бал для успішної атестації: 70</p>
	</li>
	<li>
	<p>Кількість набраних балів: %result%</p>
	</li>
</ul>
"""

elearning_success_subject = 'Результат дистанційного навчання. %ELEARNINGNAME%, %AUDITORY%. %datestart% - %dateend% .'

elearning_success_text = """
<p>Шановний %USERNAME%</p>

<p>Повідомляємо Вам про те, що Ви успішно пройшли дистанційне навчання.</p>

<ul>
	<li>
	<p>Назва навчання: %ELEARNINGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок відкриття доступу: %datestart%</p>
	</li>
	<li>
	<p>Завершення доступу: %datestart%</p>
	</li>
	<li>
	<p>Мінімальний бал для успішного завершення навчання: 70</p>
	</li>
	<li>
	<p>Кількість набраних балів: %result%</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>Вітаємо!</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>

"""

elearning_unsuccess_subject = 'Результат дистанційного навчання: %ELEARNINGNAME%, %AUDITORY%. %datestart% - %dateend% .'

elearning_unsuccess_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про те, що, на превеликий жаль, Ви не успішно завершили дистанційне навчання.</p>

<ul>
	<li>
	<p>Назва навчання: %ELEARNINGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Дати проведення атестації: %datestart% - %dateend%.</p>
	</li>
	<li>
	<p>Мінімальний бал для успішного завершення навчання: 70</p>
	</li>
	<li>
	<p>Кількість набраних балів: %result%</p>
	</li>
</ul>

<p>Пропонуємо пройти це навчання ще раз, щойно буде оголошено про відкриття чергового доступу до цього онлайн курсу. Слідкуйте, будь ласка, за нашими повідомленнями.</p>

<p>&nbsp;</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

training_success_subject = 'Результат очного навчання: %TRAININGNAME%, %AUDITORY%. Початок навчання: %datestart%. %event_duration%. %training_place%.'

training_success_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про те, що Ви успішно пройшли очне навчання.</p>

<ul>
	<li>
	<p>Назва навчання: %TRAININGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок навчання: %datestart%</p>
	</li>
	<li>
	<p>Тривалість навчання: %event_duration%</p>
	</li>
	<li>
	<p>Місце проведення навчання: %training_place%</p>
	</li>
	<li>
	<p>Мінімальний бал для успішного завершення навчання: 70</p>
	</li>
	<li>
	<p>Кількість набраних балів: %result%</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>Вітаємо!</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

training_unsuccess_subject = 'Результат очного навчання. %TRAININGNAME%, %AUDITORY%. Початок навчання: %datestart%. %event_duration%. %training_place%.'

training_unsuccess_text = """
<p>Шановний %USERNAME%,</p>

<p>Повідомляємо Вам про те, що, на превеликий жаль, Ви не успішно завершили очне навчання.</p>

<ul>
	<li>
	<p>Назва навчання: %TRAININGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок навчання: %datestart%</p>
	</li>
	<li>
	<p>Тривалість навчання: %event_duration%</p>
	</li>
	<li>
	<p>Місце проведення навчання: %training_place%</p>
	</li>
	<li>
	<p>Мінімальний бал для успішного завершення навчання: 70</p>
	</li>
	<li>
	<p>Кількість набраних балів: %result%</p>
	</li>
</ul>

<p>Пропонуємо пройти це навчання ще раз, щойно буде оголошено про набір до чергової групи з цього курсу. Слідкуйте, будь ласка, за нашими повідомленнями.</p>

<p>&nbsp;</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

test_remember_subject = 'Нагадування про необхідність взяти участь у дистанційній атестації. %AUDITORY%. %datestart% - %dateend%.'

test_remember_text = """
<p>Шановний %USERNAME%,</p>

<p>Нагадуємо Вам про необхідність пройти атестацію.</p>

<ul>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок відкриття доступу: %datestart%</p>
	</li>
	<li>
	<p>Завершення доступу: %dateend%</p>
	</li>
</ul>

<p>Для доступу до дистанційної атестації скористайтесь наступною URL-адресою:</p>

<p><a href="http://renault.kpi-ua.com/">http://renault.kpi-ua.com</a> .</p>

<p>&nbsp;</p>

<p>Бажаємо успішного проходження атестації.</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

elearning_remember_subject = 'Нагадування про необхідність взяти участь у дистанційному навчанні: %ELEARNINGNAME%, %AUDITORY%. %datestart% - %dateend%.'
elearning_remember_text = """
<p>Шановний %USERNAME%,</p>

<p>Нагадуємо Вам про необхідність пройти дистанційне навчання.</p>

<ul>
	<li>
	<p>Назва навчання: %ELEARNINGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок відкриття доступу: %datestart%</p>
	</li>
	<li>
	<p>Завершення доступу: %dateend%</p>
	</li>
</ul>

<p>Для доступу до онлайн курсу скористайтесь наступною URL-адресою:</p>

<p><a href="http://renault.kpi-ua.com/">http://renault.kpi-ua.com</a> .</p>

<p>&nbsp;</p>

<p>Бажаємо успішного навчання.</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

training_remember_subject = 'Нагадування про необхідність подати заявку на очний курс: %TRAININGNAME%, %AUDITORY%. Початок навчання: %datestart%. %event_duration%. %training_place%.'

training_remember_text = """
<p>Шановний %USERNAME%,</p>

<p>Нагадуємо Вам про необхідність взяти участь в очному навчанні.</p>

<ul>
	<li>
	<p>Назва навчання: %TRAININGNAME%</p>
	</li>
	<li>
	<p>Категорія персоналу: %AUDITORY%</p>
	</li>
	<li>
	<p>Початок навчання: %datestart%</p>
	</li>
	<li>
	<p>Тривалість навчання: %event_duration%</p>
	</li>
	<li>
	<p>Місце проведення очного навчання: %training_place%.</p>
	</li>
</ul>

<p>Просимо Вас записатись на наше очне навчання.</p>

<p>&nbsp;</p>

<p>З повагою,</p>

<p><strong>Навчально-трен</strong><strong>інговий центр Рено Україна.</strong></p>
"""

course_invitation_subject = ''
course_invitation_text = """
"""

course_finish_subject = ''
course_finish_text = """
"""