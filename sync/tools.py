def correct_symbols(s):
    intab = "асхеук"
    outtab = "acxeyk"
    trantab = s.maketrans(intab, outtab)
    s = s.translate(trantab)
    s = s.replace(" ", "")
    return s


def correct_mail(s):
    s = s.replace(" ", "")
    s = s.replace("\t", "")
    s = s.replace("\n", "")
    return s
