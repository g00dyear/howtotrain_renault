from users.models import User, UserJob
from django.contrib.auth.models import Group
from django.http import HttpResponse
from etest.models import Result, MainResult
from etest.tools import get_module_mark
from checklist.models import *
import csv


def sync(request):
    txt = open('media/sync/DMD_STD_UA.txt')
    for row in txt:
        #Дилер
        if row[13:15] == '01':
            full_name = row[87:127].strip()
            parent = User.objects.get(pk=4)
            short_name = row[127:137].strip()
            phone = row[465:479].strip()
            if not User.objects.filter(email=short_name+'@renault.com').last():
                email=short_name+'@renault.com'
            elif User.objects.filter(email=short_name+'@renault.com').count() == 1:
                email=short_name+'2@renault.com'
            else:
                continue
            u = User(username = 'Dealer', title = full_name, short_title = short_name, email=email, phone=phone)
            u.set_password(short_name)
            u.save()
            u.groups.add(1)
            u.save()
    return HttpResponse('Ok')
        
        
def sync_users(request):
    from sync.tools import correct_symbols
    csv_file = open('media/sync/test.csv')
    #rows = csv.reader(csv_file)
    count = 0
    errors = 0
    s_row = []
    while errors < 200:
        try:
            row = next(csv_file)
            row = str(row)
            s_row = row.split(';')
            first_name = correct_symbols(s_row[2])
            last_name = correct_symbols(s_row[1])
            parent = User.objects.filter(short_title=s_row[22]).first()
            if s_row[10] != '':
                email = s_row[10].lower()
            else:
                email = s_row[0].lower() + '@example.com'
            count+=1
            #Должность
            if UserJob.objects.filter(name = correct_symbols(s_row[11])).last():
                job = UserJob.objects.filter(name = correct_symbols(s_row[11])).last()
            else:
                job = UserJob(name=correct_symbols(s_row[11]))
                job.save()
            userid = s_row[0]
            if not User.objects.filter(userid=userid).first():
                u = User(username='employee', parentId=parent, first_name=first_name, last_name=last_name, email=email, job=job, userid=userid)
                u.save()
                u.groups.add(3)
                u.set_password('renault1898')
                u.save()
            else:
                u = User.objects.filter(userid=userid).last()
                u.job = job
                u.email = email
                u.groups.add(3)
                u.save()
        except:
            errors+=1
            continue
    return HttpResponse(str(count)+' '+str(errors))
    
    
    
    
    
def repair(request):
    
    x = 0
    csv_file = open('media/sync/import.csv')
    
    while x < 500:
        
        row = next(csv_file)
        row = str(row)
        s_row = row.split(';')
        category_name = s_row[0]
        subcategory_name = s_row[1]
        point_name = s_row[2]
        if not Category.objects.filter(name=category_name):
            category = Category(name=category_name)
            category.save()
        else:
            category = Category.objects.filter(name=category_name).last()
        if not SubCategory.objects.filter(name=subcategory_name):
            subcategory = SubCategory(name=subcategory_name, category=category)
            subcategory.save()
        else:
            subcategory = SubCategory.objects.filter(name=subcategory_name).last()
                
        point = Point(name=point_name, subcategory=subcategory)
        point.save()
        x+=1
    return HttpResponse('ok')
            
            
def short(request):
    dealers = User.objects.filter(groups__id=1).all()
    for dealer in dealers:
        if not dealer.short_title or dealer.short_title == '':
            dealer.short_title = dealer.title[:3]
            dealer.save()
    return HttpResponse('profit')
    
    
def new_dealers_sync(request):
    csv_file = open('media/sync/dealers.csv')
    row = next(csv_file)
    errors = 0
    new_dealers = []
    needed = 0
    while errors < 100:
        try:
            row = next(csv_file)
            row = row.split(';')
            short_title = row[2]
            if User.objects.filter(short_title=short_title).last():
                dealer = User.objects.filter(short_title=short_title).last()
                dealer.title = row[3]
                dealer.userid = row[1]
                dealer.save()
                new_dealers.append(dealer)
            else:
                needed += 1
        except:
            errors += 1
            continue
        
    # включаем МАР в АЛХ
    try:
        mar = User.objects.filter(short_title='MAR').first()
        alx = User.objects.filter(short_title='ALX').first()
        childs = mar.childs.all()
        for child in childs:
            child.parentId = alx
            child.save()
    except:
        pass
    
    # Чистим список существующих дилеров
    dealers = User.objects.filter(groups__id=1).all()
    for dealer in dealers:
        if not dealer in new_dealers:
            if dealer.childs.first():
                dealer.is_active = False
                dealer.save()
            else:
                dealer.delete()
        else:
            dealer.is_active = True
            dealer.save()
    return HttpResponse(new_dealers)
    
    
    
    
            