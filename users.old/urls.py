from django.conf.urls import patterns, include, url
from django.contrib import admin
from users import views



urlpatterns = patterns('',
     url(r'^$', views.index),
     url(r'^distributor/$', views.index2),
     url(r'^dealer/$', views.index3),
     url(r'^login/$', views.login),
     url(r'^logout/$', views.logout),
     url(r'^dealerlist/$', views.dealerList),
     url(r'^distlist/$', views.distList),
     url(r'^distulist/$', views.distUList),
     url(r'^user/(?P<user_id>\d+)/$', views.userView),
     url(r'^duser/(?P<user_id>\d+)/$', views.distUserView),
     url(r'^dealer/info/(?P<user_id>\d+)/$',  views.dealerinfo),
)
