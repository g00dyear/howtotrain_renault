from django.contrib import admin
from .models import User, City, Region, UserJob
from .forms import AdminUserAddForm, AdminUserChangeForm
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _

# Register your models here.
    

class RegionInline(admin.TabularInline):
    model = City
    
    
class RegionView(admin.ModelAdmin):
    inlines = [RegionInline]
    

class CityView(admin.ModelAdmin):
    list_display = ('name', 'region')
    list_filter = ('region',)
    
    
class JobInline(admin.TabularInline):
    model = UserJob
    
    
class JobView(admin.ModelAdmin):
    list_display = ('name',)
        
        
class UserAdmin(BaseUserAdmin):
    form = AdminUserChangeForm
    add_form = AdminUserAddForm
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),
        (_('Personal info'), {'fields': ('last_name', 'first_name', 'fathers_name', 'title', 'phone', 'job', 'city', 'parentId',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2')}),
        )
    


admin.site.register(City,CityView)
admin.site.register(UserJob, JobView)
admin.site.register(Region, RegionView)
admin.site.register(User, UserAdmin)
