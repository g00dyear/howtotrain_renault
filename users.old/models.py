from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.utils.translation import ugettext_lazy as _
# Create your models here.

class Region(models.Model):
    name = models.CharField(verbose_name='Регион', max_length=30)
    def __str__(self):
        return self.name
    
    
class City(models.Model):
    name = models.CharField(verbose_name='Город', max_length=30)
    region = models.ForeignKey(Region)
    def __str__(self):
        return self.name
    
    
class UserJob(models.Model):
    name = models.CharField(verbose_name='Должность', max_length=255)
    def __str__(self):
        return self.name
    

AbstractUser._meta.get_field('email')._unique = True
AbstractUser._meta.get_field('email').blank = False
AbstractUser._meta.get_field('username')._unique = False
AbstractUser._meta.get_field('username').default = 'noname'


class User(AbstractUser):
    class Meta:
        permissions = (
            ('UPerm', "can join user's urls"),
            ('DLPerm', "can join dealer's urls"),
            ('DSPerm', "can join distributor's urls"),
        )
        
    fathers_name = models.CharField(blank=True, max_length=30, verbose_name='Отчество')
    city = models.OneToOneField(City, blank=True, null=True, verbose_name='Город')
    phone = models.CharField(verbose_name='Телефон', max_length=30, blank=True)
    job = models.ForeignKey(UserJob, blank=True, null=True, verbose_name='Должность')
    parentId = models.ForeignKey('self', blank=True, null=True, related_name='childs')
    title = models.CharField(verbose_name='Название(дилер, дистрибьютор)', max_length=255, blank=True, null=True)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']
    
    def get_full_name(self):
        if self.title:
            return self.title
        else:
            return '%s %s %s' % (self.last_name, self.first_name, self.fathers_name,)
           
    def is_dealer(self):
        if Group.objects.get(id = 2) in self.groups.all():
            return True
        else:
            return False
    
    def is_dist(self):
        if Group.objects.get(id = 1) in self.groups.all():
            return True
        else:
            return False
            
    def is_user(self):
        if Group.objects.get(id = 3) in self.groups.all():
            return True
        else:
            return False
    
    
    
    