from django.shortcuts import render, render_to_response, redirect
from django.contrib import auth
from django.contrib.auth.models import Group
from .models import User
from django.contrib.auth.decorators import login_required, permission_required
# Create your views here.

def login(request):
    if request.method == 'POST':
        email = request.POST['email']
        if not User.objects.filter(email=email).first():
            message = 'Не правильный email!'
            return render(request, 'login.html', {'message': message})
        password = request.POST['password']
        user = auth.authenticate(email=email, password=password)
        if user:
            auth.login(request, user)
            if user.is_dealer():
                return redirect('/dealer')
            if user.is_dist():
                return redirect('/distributor')
            else:
                return redirect('/')
        else:
            message = 'Не правильно введен пароль'
            return render(request, 'login.html', {'message': message})
    else:
        return render(request, 'login.html')
    

#user permission
@login_required
@permission_required('users.UPerm')
def index(request):
    user = request.user
    return render(request, 'user.html', {'user': user})
    

#dist permission
@login_required
@permission_required('users.DSPerm')
def index2(request):
    user = request.user
    return render(request, 'distributor.html', {'user': user})
    

@login_required
@permission_required('users.DSPerm')
def distList(request):
    user = request.user
    return render(request, 'dist_list.html', {'user': user})
    
    
@login_required
@permission_required('users.DSPerm')
def distUList(request):
    user = request.user
    return render(request, 'DistUserList.html', {'user': user})
    
    
@permission_required('users.DSPerm')
@login_required
def distUserView(request, user_id):
    user = User.objects.get(id=user_id)
    dist = request.user
    if user.parentId.parentId == dist:
        return render(request, 'userlist.html', {'user':user})
    else:
        return redirect('/logout')
        

@permission_required('users.DSPerm')
@login_required
def dealerinfo(request, user_id):
    user = User.objects.get(id=user_id)
    dist = request.user
    if user.parentId == dist:
        return render(request, 'dealerinfo.html', {'user':user})
    else:
        return redirect('/logout')
        
        
#dealer permission       
@login_required
@permission_required('users.DLPerm')
def index3(request):
    user = request.user
    return render(request, 'dealer.html', {'user': user})


@login_required
@permission_required('users.DLPerm')
def dealerList(request):
    user = request.user
    return render(request, 'dealer_list.html', {'user': user})
    
    
@permission_required('users.DLPerm')
@login_required
def userView(request, user_id):
    user = User.objects.get(id=user_id)
    dealer = request.user
    if not user in dealer.childs.all():
        return redirect('/logout')
    else:
        return render(request, 'userlist.html', {'user': user})
        
        
@login_required
def logout(request):
    auth.logout(request)
    return redirect('/login/')
    


        

