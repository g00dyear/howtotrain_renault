$(function () {
$(window).resize(function () {
    adaptiveHeight();
});

function adaptiveHeight () {
    var $content = $('#content'),
    $windowHeight = $(window).height(),
    contentHeight = $windowHeight - $('#header').outerHeight(true) - $('#footer').outerHeight(true);
    
    if(!($content.outerHeight(true) >= contentHeight)) {
 
        if($content.outerHeight() >= $content.outerHeight(true)) {
            contentHeight += $content.outerHeight() - $content.outerHeight(true);
        }
    
    $content.height(contentHeight);
    }
};

adaptiveHeight();
});
