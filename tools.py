from etest.models import PlannedTest, Module, TestResult, Result, MainResult, Answer, Question, QuestionTime
from users.models import User
from elearning.models import PlannedCourse, Elearning_mark, ElearningFeedback, Attempt
from trainings.models import PlannedTraining, UserlistByDate, StudyLevel, Training
import pytz
from django.utils import timezone


def time():
    kiev = pytz.timezone('Europe/Kiev')
    time = timezone.localtime(timezone.now(), timezone=kiev)
    return time
    
    
def get_study_level(user):
    try:
        trainings = Training.objects.filter(auditory=user.job).all()
        mark = 0
        if StudyLevel.objects.filter(user=user).last():
            done = 0
            study_level = StudyLevel.objects.filter(user=user).last()
            timediff = time() - study_level.date
            diff = int(timediff.total_seconds()/60/60)
            if diff > 6:
                ptrainings = PlannedTraining.objects.filter(allowed_users=user, training=trainings, finished=True).all()
                for ptraining in ptrainings:
                    if ptraining.test_after and MainResult.objects.filter(ptest=ptraining.test_after, user=user).last():
                        done +=1
                    elif not ptraining.test_after and UserlistByDate.objects.filter(training=ptraining, user=user, date__date__lt=time).last():
                        done +=1
                study_level.mark = int(done/trainings.count()*100)
                study_level.date = time()
                study_level.save()
            mark = study_level.mark
            return mark
        else:
            done = 0
            ptrainings = PlannedTraining.objects.filter(allowed_users=user, training=trainings, finished=True).all()
            for ptraining in ptrainings:
                if ptraining.test_after and MainResult.objects.filter(ptest=ptraining.test_after, user=user).last():
                    done +=1
                elif not ptraining.test_after and UserlistByDate.objects.filter(training=ptraining, user=user, date__date__lt=time).last():
                    done +=1
            result = int(done/trainings.count()*100)
            study_level = StudyLevel(user=user, mark=result, date=time())
            study_level.save()
            mark = study_level.mark
            return mark
    except:
        mark = 0
        return mark